.. _glossary:

========
Glossary
========

.. glossary::

   ADC
      An *atmospheric dispersion corrector* generally consists of a pair of prisms that are used in the optical system of a telescope or instrument to compensate for the dispersive effect of Earth's atmosphere. No ADC is available for GMOS. 

   ADU
      For CCD amplifiers, one *analog-to-digital unit* is one count, corresponding to a quantity of detected photons given by the detector gain setting. 

   Baseline Calibrations
      *Baseline calibration* exposures are those that are obtained by Gemini Staff throughout an observing semester to support the science programs that have been initiated. They include closed-dome (bias, dark, flat-fields) and on-sky (standard stars) observations. See `GMOS Baseline Calibrations <https://www.gemini.edu/node/10625?q=node/10444>`_ for details. 

   BPM
      A static *Bad Pixel Mask* can be defined for each focal plane, which consists of an array of short integers that encode pathologies, such as bad columns, that apply to each sensor in the :term:`FPA` (see `BPM Flag Encoding <https://noirlab.edu/science/programs/csdc/usngo/gmos-cookbook/Processing/Supplement.html#gmos-bpm-flag-encoding>`_). The BPM is inserted into the :term:`MEF` files as the initial DQ extensions, one for each sensor. 

   CCD
      A *charge-coupled device* is an optical-band sensor type that is used extensively in astronomy because of their high quantum efficiency from the near-UV to the near-IR, excellent linearity, and high dynamic range. The GMOS :term:`FPA` consists of 3 abutted CCDs. 

   dayCal
      A variety of calibration exposures are obtained during the day (or twilight) to support observing programs, and to monitor the health and performance of GMOS. 

   Dither
      A *dither* pattern of exposures in a sequence, with small relative spatial offsets, is intended to provide full coverage of a contiguous region of sky in spite of gaps between adjacent detectors in the FPA. 

   FITS
      The *Flexible Image Transport System* format is an international standard in astronomy for storing images, tables, and related metadata in disk files. Multiple images and tables may be stored in :term:`MEF` files. See the `IAU FITS Standard <http://fits.gsfc.nasa.gov/fits_standard.html>`_ for details. 

   FoV
      The *field of view*, or spatial extent of sky the from the optical system of the telescope+instrument that actually falls on the detector. 

   FPA
      A *focal plane array* is the arrangement of multiple sensors in the focal plane of a camera. 

   GCAL
      The `Gemini Facility Calibration Unit <https://www.gemini.edu/observing/telescopes-and-sites/telescopes#GCAL>`_ provides continuum and emission light sources for flat-field and wavelength calibration. 

   HDU
      A :term:`FITS` file consists of a primary *header-data unit* and zero or more extension HDUs. The primary HDU (PHDU) contains a header, but may or may not contain an image. An extension HDU contains a header and any valid FITS extension type, including a binary image or a table. 

   IFU
      A *integral field unit* is a component of a spectrograph that intercepts a portion of the telescope :term:`FoV` with an array of lenslets that feed fiber optics, which are then re-mapped onto one or more virtual slits that pass light to the dispersing optics. The spectrum created for each fiber appears on the :term:`FPA`, and once extracted after data processing, can be used to reconstruct a *data cube* of spatial position and wavelength. 

   Keyword
      In the astronomy data domain, *keyword* is most closely associated with a named metadatum stored in a :term:`FITS` header, which is assigned a particular scalar or text value. 

   MasterCal
      A *Master Calibration* file, which may be an image that captures a particular instrumental signature, or a table consisting of a calibration or reference information. *MasterCals* are often built by combining calibration exposures in a particular way, or by recording coefficients of a function that characterizes, e.g., the dispersion solution. They may also consist of a catalog of reference information, such as astrometric or photometric standards. 

   MDF
   Mask Definition File
      The *mask definition file* is a :term:`FITS` binary table that gives the attributes of all apertures for the mask in use during the observation. 

   MEF
      *Multi-extension FITS* format files contain a primary HDU, and one or more extensions HDUs, each of which contains a header and data (HDU) such as a table or binary image. Raw exposures normally contain one image extension for each amplifier used for read-out.

   MOS
      *Multi-object spectroscopy* is the capability of obtaining multiple spectra of different targets (or different regions within an extended object) in the same exposure. This may be achieved by rotating a facility longslit or using a custom slitmask with multiple apertures corresponding to the targets of interest in the :term:`FoV`. 

   N&S
   Nod-and-Shuffle
      It is possible to improve the sky-subtraction in spectra obtained in the red using *nod-and-shuffle* data acquisition. In this configuration, the telescope position is spatially dithered between sequential exposures, while the charge on the sensors is shuffled to align with the new pointing. In this way the sky is sampled with the same pixels used to observe the science target. See the Gemini `Nod & Shuffle description <https://www.gemini.edu/instrumentation/gmos/capability#NodShuffle>`_ for details. 

   OIWFS
      The *On-Instrument Wavefront Sensor* provides guiding and tip-tilt correction information to the telescope control system. It is mounted on a probe that patrols an area that can extend into the imaging FoV. 

   Overscan
      Refers to the portion of the amplifier read-out of the serial register after all science pixels have been accumulated. The overscan is often appended to the science pixels in the assembled amplifier image as a separate region. This region is useful to science processing software for estimating the stability of the DC offset in the read-out electronics. 

   PHDU
      Primary header-data unit of a :term:`FITS` file, i.e., extension [0] in an :term:`MEF` file. A simple FITS file contains a header and (usually) an image array. The PHDUs in :term:`MEF` files generally *do not* include an image array. 

   RoI
   Region of Interest
      Refers to the portion of the CCD that is read out after an exposure. The *RoI* includes the full detector for imaging mode, and either the full frame or a 1024 (unbinned) pixel subset along the spatial direction for longslit or MOS spectroscopic modes depending upon the spatial extent of the target. Note that the *RoI* for science exposures must be matched to the calibration exposures (bias, flat, etc.). 

   Slit-mask
      A *slit-mask* may be inserted at the focal-plane entrance to GMOS to block light from all except selected targets or regions within an extended source from passing through the instrument. Small apertures are cut into the mask that correspond spatially with targets of interest (see `GMOS Mask Preparation <https://www.gemini.edu/instrumentation/gmos/observation-preparation#Mask>`_). The apertures are usually square (for alignment on field stars) or rectangular (i.e., *slitlets*), with the long axis sized to capture the target and nearby background, without overlapping spatially with other slitlets as projected on the :term:`FPA`. 

   WCS
   World Coordinate System
      A mapping from image pixel coordinates to physical coordinates. 
      For direct images the mapping is to the equatorial (RA, Dec) system; for extracted spectra the mapping is to the dispersion axis, usually in Angstroms, and position along the slit. 
