.. _gmos-overview:

===================
Instrument Overview
===================

To reduce data from the Gemini Multi-Object Spectrographs effectively, and to understand its limitations, is helpful to know the basics of the GMOS design and its operating modes. 
These instruments are very well documented on the Gemini Observatory website (see `GMOS <https://www.gemini.edu/instrumentation/gmos>`_) and in the literature (see [ID]_ and [PI]_). 
This chapter contains a *very* brief summary of the instrument; links to more in-depth material appear throughout the discussion. 

The GMOS imaging spectrograph can be configured in the following ways: 

   * **Imaging** of a :math:`5.5\times5.5` arcmin :term:`FoV`
   * **Long-slit spectroscopy** with moderate resolution and a variety of slit widths 
   * Simultaneous **multi-object spectroscopy** of targets in the imaging FoV with custom slitlets
   * **Integral Field Unit spectroscopy** with one or two apertures

The instrument can be operated in *standard* or *Nod-and-Shuffle* (:term:`N&S`) modes. 
The excellent `N&S mode data reduction tutorial <https://noirlab.edu/science/sites/default/files/media/archives/presentations/scipresentation0484-en.pdf>`_ by Kathy Roth may be very helpful. 

.. _gmos-focalplane:

GMOS Focal Plane
----------------
Images from GMOS can be obtained through a filter or in white light. 
The imaging field of view (:term:`FoV`) illustrated below (adapted from [ID]_ and [PI]_) is the intersection of the 7 arcmin diameter fold mirror FoV, the mask FoV, and the focal plane detector array. 
The spatial extent is 5.5 arcmin square, with truncated corners and gaps between the sensors. The On-Instrument Wavefront Sensor (:term:`OIWFS`), mounted on a moveable arm that is about 20 arcsec wide, may vignette the FoV. 

.. figure:: _static/GMOS_FoV.png
   :figwidth: 90 %

   Schematic of the GMOS focal plane. The imaging FoV is the intersection of the fold mirror FoV (*light grey circle*), the extent of the mask (*light brown square*) and the detector geometry (*clear rectangles*); detector amplifier regions are delimited by dashed lines. The patrol area of the OIWFS is also shown (*light red rectangle*). When the :term:`IFU`  mask is deployed, the *object* and *sky* apertures (*purple rectangles*) near the center of the FoV are separated by 1.0 arcmin. In spectroscopic mode, the facility longslit (*green*) is aligned vertically at the center of the detector format; the IFU fields are mapped to the R or B slit positions, which are separated by about 3000 pixels. Wavelength decreases left to right along the image *x*-axis.   
   Click image to enlarge. 

Long-slit or multi-object spectroscopy requires obtaining an acquisition exposure with a the slit mask inserted, but with no disperser to ensure that light from all targets in the field passes through the intended slitlets. 
These *acq* exposures are not useful for data reduction. 
The disperser places the spectra from the slit(s) onto the detector format for subsequent exposures.  

Detectors
^^^^^^^^^
As shown in Fig. 1 above, the focal plane is populated with three adjacent :term:`CCD` sensors. 
The CCD dimensions have changed slightly as the devices were upgraded over time, but schematically they are very similar, as shown below. 
The pixel arrays in the :term:`FITS` image extensions are ordered left-to-right, except for 6-amp mode where the order is: 2,1,4,3,6,5. 
Note that single-amp read-outs were used for GMOS-N with the EEV arrays. See `Gemini Data Formats <https://www.gemini.edu/instrumentation/gmos/data-reduction#DataFormat>`_ and the table below for details. 

.. figure:: _static/GMOS_detector.png
   :figwidth: 90 %

   Schematic of the left-most detector of the CCD arrays that have been used in GMOS. Amplifier regions are indicated (*clear rectangles*) and labelled by the extension number in the raw FITS file. Thirty-two pixel-wide serial over-scan (*tan*) regions are appended to the pixel array, as are 48 rows of parallel pre-scan (*light blue*) for the Hamamatsu detectors. The amplifier coordinate origin (*teal arrows*) is indicated for a few of the amps, but the amp pixel arrays are re-mapped to the detector coordinate system (*black arrows*) prior to writing the raw FITS file. 

.. csv-table:: **Detector Properties**
   :header: "Instrument", "Detector/nAmps", "W x H (pix)", "Pixel Size (arcs/um)", "Install Date", QE
   :widths: 12, 18, 14, 14, 12, 8

   GMOS-N, `EEV <https://www.gemini.edu/instrumentation/gmos/components#GNeev>`__/1, 2048 x 4608, 0.0728/13.5, Original, `plot <http://www.gemini.edu/sciops/instruments/gmos/gmos_n_ccd_red.gif>`__
   , `e2v_DD <https://www.gemini.edu/instrumentation/gmos/components#GNe2vDD>`_/2, 2048 x 4608, 0.0728/13.5, 2011-Oct, `plot <http://www.gemini.edu/sciops/instruments/gmos/CCD_options.pdf>`__
   , `Hamamatsu <https://www.gemini.edu/instrumentation/gmos/components#GNHam>`_/4, 2048 x 4176, 0.0809/15.0, 2017-Mar, `plot <http://www.gemini.edu/sciops/instruments/gmos/QE_GMOSN_Hamatsu_e2vdd.png>`__
   GMOS-S, `EEV <https://www.gemini.edu/instrumentation/gmos/components#GSeev>`__/1 (Old), 2048 x 4608, 0.073/13.5, Original, `plot <http://www.gemini.edu/sciops/instruments/gmos/gmos_s_ccd_blue.gif>`__
   , `EEV <https://www.gemini.edu/instrumentation/gmos/components#GSeev>`__/1 (New), 2048 x 4608, 0.073/13.5, 2006-Sep, `plot <http://www.gemini.edu/sciops/instruments/gmos/gmos_s_ccd_blue.gif>`__
   , `Hamamatsu <https://www.gemini.edu/instrumentation/gmos/components#GSHam>`__/4, 2048 x 4176, 0.080/15.0, 2014-Jun, `plot <http://www.gemini.edu/sciops/instruments/gmos/QE_Ham_plot2.pdf>`__

All the CCDs may be read out with one of a variety of binning factors: :math:`1\times` [1|2|4], :math:`2\times` [1|2|4], and :math:`4\times` [1|2|4]. 
However, binning greater than :math:`1\times1` is seldom used for IFU spectroscopy since adjacent spectra from the fibers would overlap. 
Each amplifier read-out includes 32 pixels of serial overscan, regardless of binning. 
The newer Hamamatsu arrays in addition include 48 rows of parallel pre-scan, although these rows are not used by the overscan correction software. 

Imaging
-------
Imaging may be obtained over the :math:`5.5\times5.5` arcmin FoV through a variety of filters, with possible vignetting from the :term:`OIWFS`. 
Note that the image quality in broad-band filters will be degraded by differential atmospheric dispersion, as GMOS lacks an :term:`ADC`; see `the impact of atmospheric refraction on GMOS data <http://www.gemini.edu/observing/resources/optical-resources#ADR>`_ for details. 

Filters
^^^^^^^
Images may be obtained with a variety of filters, including the *ugriz* SDSS bands, Z and Y bands in the IR, and various narrow bands. 
There are also four long-pass filters used for order blocking in spectroscopic mode. 
See `GMOS Filter Description <https://www.gemini.edu/instrumentation/gmos/components#Filters>`_ for details, including: 

   * Transmission curves for `GMOS-N filters <http://www.gemini.edu/sciops/instruments/gmos/filters/filters_2009.gif>`_ 
   * Transmission curves for `GMOS-S filters <http://www.gemini.edu/sciops/instruments/gmos/filters/GMOSS_filters_2009.gif>`_ 

In addition, observers have from time to time been allowed to supply their own filters. 

Spectroscopy
------------
Masks
^^^^^
There are several facility 5.5 arcmin longslit masks available for GMOS, with identifiers given by the FITS keyword ``MASKNAME``. 
For longslits the value of this keyword takes the form ``[MM]X.Xarcsec`` where ``MM`` is either blank or indicates Nod-and-Shuffle (:term:`N&S`) mode, and ``X.X`` is the width of the slit in arcseconds. 
The widths are one of: [``0.5|0.75|1.0|1.5|2.0|5.0``]. 
The N+S masks are one of: [``NS0.75|NS1.0``]. 
Use of the IFU mask is indicated by the string ``IFU``. 

Custom masks have routinely been fabricated for PIs to obtain simultaneous spectra of up to several hundred targets within the imaging FoV (although <50 is more typical). 
See `Multi-Object Spectroscopy <https://www.gemini.edu/instrumentation/gmos/capability#MOS>`_ for details of how the masks are constructed. 

.. _gmos-gratings:

Gratings
^^^^^^^^
There are currently six gratings offered for each instrument, with attributes detailed on the Gemini/GMOS `description of the gratings <https://www.gemini.edu/instrumentation/gmos/components#Gratings>`_.  Their properties are summarized below. 
Note that the R150 grating is commonly used with the IFU in two-slit mode; broad-band filters keep spectra from different slits from overlapping. 

Spectral Resolution
:::::::::::::::::::
The resolution of GMOS depends upon a number of factors, including the optics, the dispersers, and the size of the entrance aperture. 
The ``0.5arcsec`` facility longslit provides excellent resolution, as shown below. The IFU, with an effective slit of 0.31 arcsec, achieves even better resolution. 

.. figure:: _static/Gratings.png

   Resolutions for the GMOS dispersers (see labels) with a 0.5 arcsec slit, which is oversampled by a factor of ``S`` with :math:`1\times1` binning. Colored bands show the wavelength coverage in first order for a single setting at the blaze wavelength. 
   Click image to enlarge. 

Grating Efficiency
::::::::::::::::::
The gratings can be configured for almost any central wavelength, but the optimal choice of grating for a given science program depends upon its efficiency and resolving power, shown below. 
Note that the R150 grating is most commonly used for IFU programs. 

.. figure:: _static/GratEff.png

   The efficiency of the various GMOS gratings in first order (see labels) as measured in the lab, and the average QE of the old e2v (*dashed curve*) and the new Hamamatsu (*black dotted curve*) detectors. The total system efficiency is in addition affected by the atmospheric transmission, and by the reflectivity of the `silver mirror coatings <http://www.gemini.edu/node/11532>`_ in the optical system which are optimized for the red spectral region. 
   Click image to enlarge. 

.. _gmos-cuar-lamp:

Arc Lamp
^^^^^^^^
Wavelength comparison arcs are obtained with the Cu-Ar lamp; the spectrum is shown below. 
This atlas was built from higher resolution long-slit configurations, and is a composite of three settings with grating B1200 and three with R831, all with the 0.5 arcsec slit. 
A new high-resolution :download:`line-list <_static/cuarHiRes.txt>` includes nearly 460 lines, and may yield more robust wavelength solutions for the highest dispersion plus narrow slit configurations. 
With the B1200 grating it should be possible to identify 100 or more lines, while for R831 grating in the near-IR only a few dozen lines are identifiable. 

.. figure:: _static/CuAr_HiRes.png

   Cu-Ar spectrum at full scale (*blue*) with portions magnified (*purple*) and offset vertically for clarity. Identifiable lines are marked (*red ticks*) along the wavelength axis, and some of the brighter or more isolated lines are labelled, which should suffice to bootstrap a wavelength solution. Second-order lines in the near IR (*lower right*), labelled in green, can constrain the dispersion solution in this sparse part of the arc lamp spectrum. 
   Click image to enlarge. 

The data reduction software can write an approximate WCS solution into the headers of dispersed exposures, consisting of the central wavelength and first-order dispersion terms at the center of the spectrum. 
A full WCS solution, including (small) non-linear terms, requires running `autoidentify <https://iraf.net/irafhelp.php?val=autoidentify>`_ (which is called from ``gmos.gswavelength``). 
A zero-point correction to the wavelength solution is possible using night sky emission lines in the red. 

IFU Spectroscopy
^^^^^^^^^^^^^^^^
The integral field unit dissects two small spatial fields (*Object* and *Background*) with lenslet arrays which feed fiber optic cables. 
These fiber optics, which are grouped into rectangular blocks at the telescope focal plane, are re-packaged to linear arrays. 
The linear arrays for each block are arranged along one of two slits (*red* or *blue*) in a special slit mask, as shown below for GMOS-N. 
Light from these closely packed fibers is dispersed along the FPA. 
Light from the background field can be masked or, in two-slit mode, the dispersed light from the spectral traces on the red slit are kept from overlapping with those of the blue slit using passband limiting filters.  

.. figure:: _static/IFU_slitMap.png
   :width: 90 %

   The IFU fields are composed of (hexagonal faced) lenslet arrays which are mapped to the slit mask via fiber optics. The *Object field* consists of 1000 fibers in a :math:`25\times40` array, and the *Background Field* consists of 500 filbers in a :math:`25\times20` array. 
   Half of the object and the background fields are mapped to the red slit (*right*) and half to the blue slit (*left*). The fields are composed of blocks (labelled :math:`1-20` and :math:`A-J`) of two rows of 25 fibers each, which are re-mapped to linear arrays as shown in the insets for the *A* background block and the *20* object block. This diagram illustrates the GMOS-N configuration; some blocks are masked for GMOS-S in :term:`N&S` mode. 
   Click image to enlarge. 


Instrument Foibles
------------------
Issues with GMOS instrument performance have arisen from time to time. See the `Status and Availability <https://www.gemini.edu/news/instrument-announcements/gmos>`_ page for an inverse chronological list of issues that have affected data attributes or data quality. Some events are particularly noteworthy: 

* 2004 May: Gemini-S primary mirror initial coating with protected silver
* 2004 Nov.: Gemini-N primary mirror initial coating with protected silver
* 2011 Nov.: New e2v deep depletion CCDs were installed on GMOS-N
* 2014 Jun.: New Hamamatsu CCDs were installed on GMOS-S
* 2015 Aug.: New Hamamatsu CCD video boards upgraded on GMOS-S

Other issues may be listed here, as they become known: 

**UT date of exposure start**
   The start of an exposure seems not to be known with good accuracy for most GMOS archived data. There are multiple header keywords that capture a start time: ``UT``, ``UTSTART``, and ``TIME-OBS`` except that **these times often disagree**. It is thought that the ``UT`` keyword value is most accurate, with an uncertainty of at least 1 s.  

   This problem was discovered in 2012, and a fix has been implemented at Gemini South for the new Hamamatsu CCDs. Again, the ``UT`` keyword contains the value that is most accurate. 
