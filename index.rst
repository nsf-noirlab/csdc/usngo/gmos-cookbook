.. GMOS Cookbook documentation master file, created by
   sphinx-quickstart on Mon 26 Oct 2015.
   You can adapt this file completely to your liking, but it should at least 
   contain the root `toctree` directive.

.. figure:: /_static/GMOS_M8.*
   :scale: 100%
GMOS Image of M8 (Lagoon Nebula). Photo credit: Gemini/J. Arias & R. Barba.

.. Important::
    **Version 2.0 of the GMOS Cookbook** (release date: March 1, 2022)

    The GMOS Cookbook has undergone extensive revision and updating. The **PyRAF** tutorials (:ref:`Imaging<pyraf-proc-img>`, :ref:`Long-Slit<pyraf-proc-ls>`, :ref:`Nod & Shuffle<pyraf-proc-lsns>`, :ref:`Multi-Object Spectra<pyraf-proc-mos>`, and :ref:`IFU<pyraf-proc-ifu>`) are compliant with the software described in the :ref:`getting-started` section. The **IRAF** tutorials (:ref:`Imaging<iraf-proc-img>`, :ref:`Long-Slit<iraf-proc-ls>`, :ref:`Multi-Object Spectra<iraf-proc-mos>`, and :ref:`IFU<iraf-proc-ifu>`) are also expected to work with the same software. Please report any issues to usngo@noirlab.edu.

============================
GMOS Data Reduction Cookbook
============================

The `Gemini Observatory <http://www.gemini.edu>`_ provides a variety of facility-class instruments for use by the professional astronomy community, including the twin Gemini Multi-Object Spectrographs (`GMOS <https://www.gemini.edu/instrumentation/gmos>`_). 
These imaging spectrographs offer useful sensitivity in the optical from the near-UV to the near-IR, at high spatial resolution and low to moderate spectral resolution. 
GMOS-North was the first instrument to be delivered, in 2001 July, and GMOS-South followed in 2002 December; data from the commissioning periods to the present are offered in the `Gemini Observatory Archive <https://www.gemini.edu/sciops/data-and-results/gemini-observatory-archive>`_. 

Raw data for these instruments are archived and made available for public use, but no calibrated, science-ready products are produced. 
This *Cookbook* is intended as a guide to data reduction and calibration for PIs and Archive users of data from either of the GMOS instruments. 
The descriptions, recipes, and scripts offered in this *Cookbook* will provide the necessary information for deriving scientifically viable (but not necessarily optimal) spectra and images from GMOS. 
However, users should be aware that the ultimate utility of the data for any specific scientific goal depends strongly upon a number of external factors, including: 

* the weather conditions at the time of the observations (see the `Gemini QA process <http://www.gemini.edu/observing/phase-iii/after-data-are-taken#DataQA>`_),
* the performance of the instrument, 
* the observing procedures used to obtain the data,
* the scientific objectives of the original observing program.

Contents
^^^^^^^^

The GMOS instruments are very well documented on the `Gemini/GMOS website <https://www.gemini.edu/instrumentation/gmos>`_ and in published papers (see :ref:`literature-ref`). 
But it is not necessary to understand every detail of the instrument design and operation to reduce your data; links to relevant portions of the instrument literature will appear throughout this manual. 

.. toctree::
   :maxdepth: 2

   GMOS_overview

Data reduction begins with installing and configuring the data reduction software environment, searching for and retrieving data, and previewing and organizing the data for processing. 

.. toctree::
   :maxdepth: 2

   GettingStarted

Details of processing science data are presented in a set of tutorials, which reduce data obtained directly from the Archive for each GMOS operating mode. 

.. toctree::
   :maxdepth: 3

   Processing/index.rst

Some of the techniques or resources used in the course of data reduction, such as wavelength calibration, spectrum extraction, or calibration artifacts, are described in more detail. 

.. toctree::
   :maxdepth: 3

   Processing/Supplement

The following chapter provides many links to on-line resources, software tools, and literature references that may be very helpful for GMOS data reduction. 

.. toctree::
   :maxdepth: 2

   Resources

Finally, a glossary of technical terms and a description of how this manual was prepared appear in the final two chapters. 

.. toctree::
   :maxdepth: 2

   Glossary
   About

.. only:: comment

   Index and Search
   ^^^^^^^^^^^^^^^^

   * :ref:`genindex`
   * :ref:`search`
