#! /usr/bin/env python
#    2016-Jun-10  shaw@noao.edu

import sys
import copy
from pyraf import iraf
from pyraf.iraf import gemini, gemtools, gmos, onedspec
import fileSelect as fs

def gmos_mos_proc():
    '''
    GMOS Data Reduction Cookbook companion script to the chapter:
        "Reduction of Multi-Object Spectra with PyRAF"

    PyRAF script to:
    Process MOS exposures for M81 field 1, in program GN-2011B-C-3.

    The names for the relevant header keywords and their expected values are
    described in the DRC chapter entitled "Supplementary Material"

    Perform the following starting in the parent work directory:
        cd /path/to/work_directory

    Place the fileSelect.py module in your work directory. Now execute this
    script from the unix prompt:
        python gmos_mos_proc.py
    '''

    print ("### Begin Processing GMOS/MOS Spectra ###")
    print (' ')
    print ("---> You must have the MDF files:")
    print ("--->    GN2011BC003-01.fits and GN2011BC003-02.fits")
    print ("---> in your work directory. ")
    print (' ')
    print ("=== Creating MasterCals ===")

    # This whole example depends upon first having built an sqlite3 database of metadata:
    #    cd ./raw
    #    python obslog.py obsLog.sqlite3
    dbFile='./raw/obsLog.sqlite3'

    # From the work_directory:
    # Create the query dictionary of essential parameter=value pairs.
    # Select bias exposures within ~2 months of the target observations:
    qdf = {'use_me':1,
           'Instrument':'GMOS-N','CcdBin':'2 2','RoI':'Full',
           'Disperser':'B600+_%','CentWave':520.0,'AperMask':'GN2011BC003-01',
           'Object':'M81-field1',
           'DateObs':'2012-01-16:2012-02-12'
          }
    # Need another copy for the CenterSpec RoI:
    qdc = copy.deepcopy(qdf)
    qdc.update({'RoI':'CentSp','AperMask':'1.0arcsec','Object':'G191B2B'})

    print (" --Creating Bias MasterCal--")

    # Use primarily the default task parameters.
    gemtools.gemextn.unlearn()    # Disarm a bug in gbias
    gmos.gbias.unlearn()
    biasFlags = {
        'logfile':'biasLog.txt','rawpath':'./raw/','fl_vardq':'yes',
        'verbose':'no'
    }

    # The following SQL generates the list of full-frame files to process.
    SQL = fs.createQuery('bias', qdf)
    biasFull = fs.fileListQuery(dbFile, SQL, qdf)
    
    # The str.join() funciton is needed to transform a python list into a
    # string of comman-separated files that IRAF can understand.
    if len(biasFull) > 1:
        gmos.gbias(','.join(str(x) for x in biasFull), 'MCbiasFull', **biasFlags)
    
    # Again, for the CenterSpec files
    biasCenSp = fs.fileListQuery(dbFile, fs.createQuery('bias', qdc), qdc)
    if len(biasCenSp) > 1:
        gmos.gbias(','.join(str(x) for x in biasCenSp), 'MCbiasCenSp', **biasFlags)
    
    # Clean up
    iraf.imdel('gN2012*.fits')

    print (" -- Creating GCAL Spectral Flat-Field MasterCals --")
    # Set the task parameters.
    gmos.gireduce.unlearn()
    gmos.gsflat.unlearn()
    flatFlags = {
        'fl_over':'yes','fl_trim':'yes','fl_bias':'yes','fl_dark':'no',
        'fl_fixpix':'no','fl_oversize':'no','fl_vardq':'yes','fl_fulldq':'yes',
        'rawpath':'./raw','fl_inter':'no','fl_detec':'yes',
        'function':'spline3','order':'8',
        'logfile':'gsflatLog.txt','verbose':'no'
        }    
    # Normalize the spectral flats per CCD.
    # Response curve fitting should be done interactively.
    print ("  - CenterSpec GCAL-flat normalization, non-interactive -")
    qdc['DateObs'] = '*'
    cwc = {'B6-420':420.0, 'B6-520':520.0, 'B6-620':620.0, 'R4-620':620.0, 'R4-740':740.0}
    for tag,w in cwc.iteritems():
        qdc['Disperser'] = tag[0:2] + '00+_%'
        qdc['CentWave'] = w
        flatName = 'MCflatCenSp_' + tag
        flatCenSp = fs.fileListQuery(dbFile, fs.createQuery('gcalFlat', qdc), qdc)
        gmos.gsflat (','.join(str(x) for x in flatCenSp), flatName, bias='MCbiasCenSp',
                **flatFlags)

    print ("  -Full Flat (GCAL & Twi) normalization, non-interactive-")
    qdf['DateObs'] = '*'
    cwf = {'B6-520':520.0, 'R4-740':740.0}
    flatFlags.update({'fl_keep':'yes','fl_usegrad':'yes','fl_detec':'no',
                     'fl_seprows':'no','order':53})
    flatType = ['gcalFlat', 'twiSpecFlat']
    for ft in flatType:
        for tag,w in cwf.iteritems():
            qdf['Disperser'] = tag[0:2] + '00+_%'
            qdf['CentWave'] = w
            flatName = 'MC' + ft + '-M01_' + tag
            combName = 'MC' + ft + 'Comb-M01_' + tag
            flatFull = fs.fileListQuery(dbFile, fs.createQuery(ft, qdf), qdf)
            gmos.gsflat (','.join(str(x) for x in flatFull), flatName, 
                         bias='MCbiasFull', combflat=combName, **flatFlags)

    print ("=== Processing Science Files ===")
    print (" -- Performing Basic Processing --")

    # Use primarily the default task parameters.
    gmos.gsreduce.unlearn()
    sciFlags = {
        'fl_over':'yes','fl_trim':'yes','fl_bias':'yes','fl_gscrrej':'no',
        'fl_dark':'no','fl_flat':'yes','fl_gmosaic':'yes','fl_fixpix':'no',
        'fl_gsappwave':'yes','fl_oversize':'no',
        'fl_vardq':'yes','fl_fulldq':'yes','rawpath':'./raw',
        'fl_inter':'no','logfile':'gsreduceLog.txt','verbose':'no'
    }
    arcFlags = copy.deepcopy(sciFlags)
    arcFlags.update({'fl_flat':'no','fl_vardq':'no','fl_fulldq':'no'})
    stdFlags = copy.deepcopy(sciFlags)
    stdFlags.update({'fl_fixpix':'yes','fl_vardq':'no','fl_fulldq':'no'})

    print ("  - Longslit Std-star and Arc exposures -")
    for tag,w in cwc.iteritems():
        qdc['Disperser'] = tag[0:2] + '00+_%'
        qdc['CentWave'] = w
        flatName = 'MCflatCenSp_' + tag
        arcCenSp = fs.fileListQuery(dbFile, fs.createQuery('arc', qdc), qdc)
        gmos.gsreduce (','.join(str(x) for x in arcCenSp), bias='MCbiasCenSp',
                  **arcFlags)
        stdCenSp = fs.fileListQuery(dbFile, fs.createQuery('std', qdc), qdc)
        gmos.gsreduce (','.join(str(x) for x in stdCenSp), bias='MCbiasCenSp',
                  flatim=flatName, **stdFlags)
                  
    print ("  - MOS Science and Arc exposures -")
    for tag,w in cwf.iteritems():
        qdf['Disperser'] = tag[0:2] + '00+_%'
        qdf['CentWave'] = w
        flatName = 'MCgcalFlat-M01_' + tag
        gradName = 'MCgcalFlatComb-M01_' + tag
        
        arcFull = fs.fileListQuery(dbFile, fs.createQuery('arcP', qdf), qdf)
        gmos.gsreduce (','.join(str(x) for x in arcFull), bias='MCbiasFull',
                  gradimage=gradName, **arcFlags)
                  
        sciFull = fs.fileListQuery(dbFile, fs.createQuery('sciSpec', qdf), qdf)
        gmos.gsreduce (','.join(str(x) for x in sciFull), bias='MCbiasFull',
                  flatim=flatName, gradimage=gradName, **sciFlags)

    # Clean up
    iraf.imdel('gN2012*.fits')

    print ("=== Finished Basic Calibration Processing ===")
    print ("\n")
    print ("=== Begining multi-frame cosmic ray rejection ===")

    # Set the task parameters.
    gemtools.gemcombine.unlearn()
    sciCombFlags = {
        'combine':'average','reject':'ccdclip',
        'fl_vardq':'yes','fl_dqprop':'yes',
        'logfile':'gemcombineLog.txt.txt','verbose':'no'
    }
    # Combine the science exposures with outlier rejection for each grating.
    prefix = 'gs'
    for tag,w in cwf.iteritems():
        qdf['Disperser'] = tag[0:2] + '00+_%'
        qdf['CentWave'] = w
        outFile = qdf['Object'] + tag
        sciFull = fs.fileListQuery(dbFile, fs.createQuery('sciSpec', qdf), qdf)
        gemtools.gemcombine (','.join(prefix+str(x) for x in sciFull), outFile,
                             **sciCombFlags)

    print ("=== Begining wavelength calibration ===")
    print (" -- Deriving wavelength calibration --")
    gmos.gswavelength.unlearn()
    waveFlags = {
        'coordlist':'gmos$data/CuAr_GMOS.dat','fwidth':6,'nsum':50,
        'function':'chebyshev','order':5,
        'fl_inter':'no','logfile':'gswaveLog.txt','verbose':'no'
        }
    # Begin with longslit Arcs.
    # The fit to the dispersion relation should be performed interactively;
    # here we will us a previously determined result.
    # There are many arcs to choose from: we only need one for each setting.
    for seq in ['246','247','248','251','252']:
        inFile = prefix + 'N20120124S0' + seq
        gmos.gswavelength(inFile, **waveFlags)

    # Now for MOS Arcs
    # Should achive RMS<0.20
    waveFlags.update({'order':7,'nsum':20,'step':2})
    for tag,w in cwf.iteritems():
        qdf['Disperser'] = tag[0:2] + '00+_%'
        qdf['CentWave'] = w
        outFile = qdf['Object'] + '-M01_' + tag
        gmos.gswavelength (','.join(prefix+str(x) for x in arcFull),
                           **waveFlags)

    print (" -- Applying wavelength calibration --")
    gmos.gstransform.unlearn()
    transFlags = {
        'fl_vardq':'yes','interptype':'linear','fl_flux':'yes',
        'logfile':'gstransformLog.txt','verbose':'no'
    }                             
    # Construct a filename mapping.
    # (sciFileID, arcFileID, stdName): 'Config mneumonic'
    transMap = {
                (120,247,'G191B2B'):'B6-420',
                (117,246,'G191B2B'):'B6-520',
                (121,248,'G191B2B'):'B6-620',
                (128,251,'G191B2B'):'R4-620', 
                (123,252,'G191B2B'):'R4-740',
                (229,247,'HZ44'):'B6-420', 
                (226,246,'HZ44'):'B6-520', 
                (230,248,'HZ44'):'B6-620'
                }
    for tag,id in transMap.iteritems():
        inFile = 'gsN20120124S0' + id[0]
        wavFile = 'gsN20120124S0' + id[1]
        outFile = 't' + id[2] + '_' + tag
        gmos.gstransform (inFile, outimages=outFile, wavtraname=wavFile, **transFlags)
               
    # Science MOS exposures
    transFlags.update({'fl_vardq':'yes'})
    gmos.gstransform ('M81-field1-M01_B6-520', wavtraname='gsN20120124S0169', **transFlags)
    gmos.gstransform ('M81-field1-M01_R4-740', wavtraname='gsN20120124S0187', **transFlags)
    # Clean up
    iraf.imdel(gsN2012*.fits')
                          
    print ("=== Begining Flux Calibration ===")
    print (" -- Perform sky subtraction --")
                             
    # This will require summing the spectra along columns, e.g.:
    #   pcols ('tG191B2B_B6_520.fits[SCI]'), 1100, 2040, wy1=40, wy2=320)
    # The sky regions should be selected with care, using e.g. prows/pcols.
    gmos.gsskysub.unlearn()
    skyFlags = {
        'fl_oversize':'no','long_sample'='50:150,350:450','fl_vardq':'no',
        'logfile':'gsskysubLog.txt','verbose':'no'
    }
    gmos.gsskysub ('tG191B2B_*', **skyFlags)
    gmos.gsskysub ('tHZ44*', **skyFlags)

    print (" -- Extract Std spectrum --")

    # Extract the std spectruma using a large aperture.
    # It's important to trace the spectra interactively.
    gmos.gsextract.unlearn()
    extrFlags = {
        'apwidth':3.,'fl_inter':'no','find':'yes',
        'trace':'yes','tfunction':'spline3','tnsum':20,'tstep':50,
        'weights':'none','background':'none',
        'fl_vardq':'no','verbose':'no','logfile':'gsextractLog.txt'
    }
    ordc = {'B6-420':7, 'B6-520':8, 'B6-620':8, 'R4-620':19, 'R4-740':19}
    for tag,o in ordc.iteritems():
        inFile = 'stG191B2B' + tag
        gmos.gsextract (inFile, torder=o, **extrFlags)

    # Special cases for HZ44:
    # RMS is 7.e-3:
    gmos.gsextract ('stHZ44_B6-520', torder=6, **extrFlags)
    # RMS = 0.039, if the bluest 7 points are removed:
    gmos.gsextract ('stHZ44_B6-420', torder=14, refimages='stHZ44_B6-520',
                    **extrFlags)
    # RMS is 7.1e-3:
    gmos.gsextract ('stHZ44_B6-620', torder=13, refimages='stHZ44_B6-520', 
                    **extrFlags)
                             
    print (" -- Derive Flux calibration --")
    # Derive the sensitivity function.
    # Make a local copy the monochromatic mags file from ESO for G191B2B, HZ44. 
    # Make a local copy of the custom Mauna Kea atmospheric exteinction function
    gmos.gsstandard.unlearn()
    sensFlags = {
        'fl_inter':'yes','starname':'g191b2b','caldir':'./',
        'observatory':'Gemini-North','extinction':'./mk_extinct.txt',
        'function':'spline3','order':7,'verbose':'no','logfile':'gsstdLog.txt'
    }
    # Note that the S/N is very poor shortward of ~3400 Ang.
    gmos.gsstandard ('estG191B2B_B6*', sfile='std_B6', sfunction='sens_B6', **sensFlags)
    gmos.gsstandard ('estG191B2B_R4*', sfile='std_R4', sfunction='sens_R4', **sensFlags)

    print ("=== Begining Science MOS Calibration ===")
    print (" -- Applying MOS wavelength calibration --")
    gmos.gstransform.fl_vardq = 'yes'
    gmos.gstransform ('M81-field1-M01_B6-520', wavtraname='gsN20120124S0169')
    gmos.gstransform ('M81-field1-M01_R4-740', wavtraname='gsN20120124S0187')
               
    print (" -- Extracting MOS spectra --")
    ordf = {'B6-520':8, 'R4-740':19}
    extrFlags.update({'apwidth':2.5,'mos_bsample':1.0,'torder':1,'tnsum':50,
                      'tfunction':'spline3','background':'median',
                      'fl_vardq':'no','fl_inter':'yes'})
    gmos.gsextract ('tM81-field1-M01_B6-520', **extrFlags)
    gmos.gsextract ('tM81-field1-M01_R4-740', **extrFlags)

    ## Apply the sensitivity function.
    gmos.gscalibrate.unlearn()
    calibFlags = {
        'extinction':'./mk_extinct.txt','fl_ext':'yes','fl_scale':'no', 
        'fl_vardq':'no','logfile':'gscalibrateLog.txt'
        }
    gmos.gscalibrate ('etM81-field1-M01_B6-520', sfunction='sens_B6', **calibFlags)
    gmos.gscalibrate ('etM81-field1-M01_R4-740', sfunction='sens_R4', **calibFlags)

    print ("=== Finished Calibration Processing ===")


if __name__ == "__main__":
    gmos_mos_proc()
