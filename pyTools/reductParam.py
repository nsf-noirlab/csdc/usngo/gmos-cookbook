#!/usr/bin/env python
# shaw@noao.edu  2016-Apr-28

import fileSelect as fs

class ReductParam(object):
    '''Container for data reduction parameters.
    '''

    def __init__(self, expType, expParams, dbFile, biasName=None, traceName=None, 
                 gapsName=None, flatName=None, wvtranName=None):
        '''Construct a data reduction container for a set of exposures.
        '''

        self.type = str(expType)
        self.qd = expParams
        self.dbFile = str(dbFile)
        self.MCbias = str(biasName)
        self.MCtrace = str(traceName)
        self.MCgaps = str(gapsName)
        self.MCresponse = str(flatName)
        self.MCwav = str(wvtranName)

        qd = self.qd
        if qd is not None and self.dbFile is not None:
            self.fileList = fs.fileListQuery(self.dbFile, fs.createQuery(self.type, qd), qd)
        else:
            self.fileList = []

    def rpToDict(self):
        ''' Return processing parameters in a dict.
        '''
        
        return {'bias': self.MCbias, 'reference': self.MCtrace,
                'response': self.MCresponse, 'wavtraname': self.MCwav}

