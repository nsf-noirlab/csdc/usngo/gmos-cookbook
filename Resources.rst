.. _resources:

================
Useful Resources
================

Resources for your data reduction needs are collected here for easy reference. 
Information about the GMOS instrument design, operation, and observing may be found on the `GMOS Home Page <https://noirlab.edu/science/programs/csdc/usngo/instruments/gmos>`_. 

Archive Portal
--------------
Users of Gemini should retrieve their data from the `Gemini Observatory Archive <https://archive.gemini.edu/searchform>`_. 
No account is necessary to retrieve data, except for recent data covered by a proprietary period during which only the PI or designated Co-Is of the observing program have access. 
Calibration data are not proprietary. 

.. _GMOS-help:

GMOS Help
---------
For help with GMOS data reduction you may find the `Data Reduction Support page <https://www.gemini.edu/observing/phase-iii>`_ useful. 
To address problems with the Gemini software, direct your inquiry as follows: 

* `Astroconda <http://astroconda.readthedocs.io/en/latest/>`_ installation and PyRAF: help@stsci.edu
* `Gemini Helpdesk <https://www.gemini.edu/observing/helpdesk>`_ for problems with:

  * IRAF tools, including the **gemini** packages
  * Gemini Observatory Archive

* Gemini/IRAF `FAQ list <https://www.gemini.edu/observing/phase-iii/data-reduction-support#FAQ>`_
* US NGO GMOS `FAQ list <https://noirlab.edu/science/programs/csdc/usngo/instruments/gmos/faq>`_
* List of `known problems <https://www.gemini.edu/observing/phase-iii/data-reduction-support#Problems>`_ with GMOS software or the data. 
* `Gemini Data Reduction User Forum <http://drforum.gemini.edu/start-here/>`_

.. _online-resources:

On-Line Resources
-----------------
A number of good tutorials for reducing data have been developed over the years, some of which contain links to demonstration data. 
This *Cookbook* draws upon many of them. 
Here are several:

- Power-point presentations from the `Gemini Data Workshop <https://noirlab.edu/science/events/websites/gdw>`_ in Tucson (2010 July; all in PDF): 

  - `GMOS general data reduction tutorial <https://noirlab.edu/science/sites/default/files/media/archives/presentations/scipresentation0482-en.pdf>`_ (Rodrigo Carrasco) 
  - `GMOS IFU data reduction tutorial <https://noirlab.edu/science/sites/default/files/media/archives/presentations/scipresentation0485-en.pdf>`_ (Richard McDermid)
  - `Introduction to GMOS Nod-and-Shuffle observing <https://noirlab.edu/science/sites/default/files/media/archives/presentations/scipresentation0483-en.pdf>`_ (Katy Roth)
  - `GMOS Nod-and-Shuffle data reduction tutorial <https://noirlab.edu/science/sites/default/files/media/archives/presentations/scipresentation0484-en.pdf>`_ (Katy Roth)

* Presentations from the South American Gemini Data Reduction Workshop (2011 October)

  * `Reducing GMOS Spectroscopic Data <https://noirlab.edu/science/sites/default/files/media/archives/presentations/scipresentation0041-en.pdf>`_ (Ricardo Schiavon)
  * `GMOS Data Reduction <https://noirlab.edu/science/sites/default/files/media/archives/presentations/scipresentation0042-en.pdf>`_ (Rodrigo Carrasco)

* `The PyRAF Tutorial <http://stsdas.stsci.edu/pyraf/doc.old/pyraf_tutorial/>`_ by Rick White and Perry Greenfield provides a good primer for using **python** to invoke IRAF tasks, while offering the rich programming environment that the language provides. 

* `On-line IRAF help files <http://stsdas.stsci.edu/gethelp/HelpSys.html>`_ provided by STScI.

Standards and Catalogs
----------------------
The following catalogs and other reference material may be of use during your data reduction and analysis: 

* Cu-Ar `reference spectrum <https://drive.google.com/file/d/1jcAbHwZE7BfGjR696U_LOdyvg8QB5zhg/view?usp=sharing>`_ (atlas, in FITS format) and `ASCII line list <https://drive.google.com/file/d/12PfuywAn3465iaqD-O8y_2ywXEXOGO3j/view?usp=sharing>`_.

* Spectrophotometric standard star compendia

  * from `Gemini <https://www.gemini.edu/instrumentation/gmos/calibrations#SpectStand>`_ 
  * from `ESO <http://www.eso.org/sci/observing/tools/standards/spectra.html>`_
  * from `STScI <https://www.stsci.edu/hst/instrumentation/reference-data-for-calibration-and-tools/astronomical-catalogs/calspec>`_

* `DSS <http://archive.eso.org/dss/dss>`_: ESO Online Digitized Sky Survey

* `USNO <http://www.usno.navy.mil/USNO/astrometry/optical-IR-prod/icas>`_ Image and Catalog Archive Server

.. _software-tools:

Software Tools
--------------
IRAF Tools
^^^^^^^^^^
`IRAF <https://iraf-community.github.io/>`_ (version 2.17) has a variety of packages for the reduction and calibration of images and longslit spectra. For help with IRAF software, post a message to `iraf.net <http://iraf.net>`_

.. csv-table:: **IRAF Data Processing References**
   :header: "Resource", "Description"
   :widths: 15, 60

   `ximtool <https://github.com/iraf-community/x11iraf>`_, IRAF image display tool (not under active development). May be used instead of `ds9 <https://sites.google.com/cfa.harvard.edu/saoimageds9/home>`_. 
   `WCS Tutorial <https://iraf.net/irafdocs/astrometry/astrom.html>`_, *Creating a Mosaic World Coordinate System* 2000 by F. Valdes (Tucson: NOAO/IRAF). Although the material is intended for images from the NOAO MOSAIC Cameras it is quite general and can be applied to most images. **Note:** many of the links on this page are stale.
    :download:`Slit Spectral Reductions <_static/IRAF_LSreduce.pdf>`, *User's Guide to Reducing Slit Spectra with IRAF* [MVB]_; ca. 1992.
    `LS Spectral Extraction <http://physics.uwyo.edu/~chip/Classes/ASTR5150/doslitgif.html>`_, Tutorial for extracting longslit spectra using the `doslit` task; ca. 1994. 


Python Tools
^^^^^^^^^^^^
The **python** packages used within this *Cookbook* are either included with **AstroConda** (or **Ureka** for older installations), or can be installed easily. Two additional modules were built to support this *Cookbook*:

.. csv-table:: **Python Processing Tools**
   :header: "File", "Description"
   :widths: 15, 60

   ``obslog.py``, Unix command-line tool to create a log of observations based on FITS file metadata. Output is an sqlite database and optionally an ASCII representation. 
   ``fileSelect.py``, Provides a method to retrieve a list of files from the observing log (i.e. the SQLite3 database created with **obslog.py**) based on exposure metadata. Can be executed from the unix command line to produce a list of files. 

These tools have dependencies on some common **python** packages. 
A few particularly useful **python** packages are listed in the table below: 

.. csv-table:: **Python Package Dependencies**
   :header: "File", "Description"
   :widths: 15, 60

   `numpy <http://www.numpy.org>`_, Numerical operations on arrays
   `astropy <http://www.astropy.org>`_, General astronomical utilities including FITS i/o
   `matplotlib <http://matplotlib.org/>`_, 2-D python plotting library. 
   `scipy <http://scipy.org>`_, General scientific and mathematical utilities
   `sqlite <https://www.sqlite.org>`_, Database creation and access tools 

These packages are included by default in the `Anaconda distribution of python <https://store.continuum.io/cshop/anaconda/>`_, which is highly recommended. 

Third-Party Software
^^^^^^^^^^^^^^^^^^^^
Various third-party software tools may be useful for the data reduction process, depending upon the scientific goals. 
While most astronomers will have many or most of these tools, they are listed here for convenience. 

.. csv-table:: **Third-Party Tools**
   :header: "Tool", "Description"
   :widths: 15, 60

   `Aladin <http://aladin.u-strasbg.fr>`_, Interactive sky atlas; may be used to refine WCS solution in images. The desktop tool can access many on-line astronomical catalogs as well as local (private) catalogs.
   `ds9 <https://sites.google.com/cfa.harvard.edu/saoimageds9/home>`_, SAOImage DS9 image display tool. May be used in place of IRAF ``ximtool``.
   `SQLite Browser <http://sqlitebrowser.org>`_, Browser for SQLite3 database files.

Acknowledgements
----------------
Scientific publications that make use of data obtained from Gemini facilities should include the appropriate acknowledgement described on the Gemini Observatory section of the `NOIRLab Scientific Acknowledgements <https://noirlab.edu/science/about/scientific-acknowledgments#gemini>`_ page.  
You should also cite one or both of the GMOS instrument description papers by [PI]_ and [ID]_, depending upon the observing configuration used. 

Citations to this *Cookbook* should read: 

   US National Gemini Office 2022, *GMOS Data Reduction Cookbook* (Version 2.0; Tucson: NSF's National Optical-Infrared Astronomy Research Laboratory), available online at:

   `<https://noirlab.edu/science/programs/csdc/usngo/gmos-cookbook/>`_

Previous citation (deprecated):

   Shaw, Richard A. 2016, *GMOS Data Reduction Cookbook* (Version 1.2; Tucson: National Optical Astronomy Observatory), available online at: 

   `<https://noirlab.edu/science/programs/csdc/usngo/gmos-cookbook/>`_


Use of IRAF software should include the following footnote: 

   *IRAF was distributed by the National Optical Astronomy Observatory, which was managed by the Association of Universities for Research in Astronomy (AURA) under a cooperative agreement with the National Science Foundation.*

Use of PyRAF software should also include the following footnote: 

   *PyRAF is a product of the Space Telescope Science Institute, which is operated by AURA for NASA.*

.. _literature-ref:

Literature References
---------------------
.. [ID] Allington-Smith, J., Murray, G., Content, R., Dodsworth, G., Davies, R., Miller, B. W., Jorgensen, I.; Hook, I.; Crampton, D., & Murowinski, R. 2002, `Integral Field Spectroscopy with the Gemini Multiobject Spectrograph. I. Design, Construction, and Testing`, `PASP, 114, 892 <http://ui.adsabs.harvard.edu/abs/2002PASP..114..892A>`_ 

.. [BC] Buton, et al. 2013, *Atmospheric extinction properties above Mauna Kea from the Nearby SuperNova Factory spectrophotometric data set*, `A&A 549, 8 <http://ui.adsabs.harvard.edu/abs/2013A%26A...549A...8B>`_

.. [DST] Davies, et al. 2015, *The "Green Bean" galaxy SDSS J224024.1-092748: unravelling the emission signature of a quasar ionization echo*, `MNRAS, 449, 1731 <http://ui.adsabs.harvard.edu/abs/2015MNRAS.449.1731D>`_

.. [PI] Hook, I.M., Jorgensen, I., Allington-Smith, J.R., Davies, R.L., Metcalfe, N., Murowinski, R.G., & Crampton, D. 2004, `The Gemini-North Multiobject Spectrograph: Performance in Imaging, Long-slit, and Multi-Object Spectroscopic Modes`, `PASP, 116, 425 <http://ui.adsabs.harvard.edu/abs/2004PASP..116..425H>`_

.. [CP] Jorgensen, I. 2009, *Calibration of Photometry from the Gemini Multi-Object Spectrograph on Gemini North*, `PASA, 26, 17 <http://ui.adsabs.harvard.edu/abs/2009PASA...26...17J>`_

.. [K14] Krabbe, et al. 2014, *Interaction Effects on Galaxy Pairs with Gemini/GMOS- I: Electron Density*, `MNRAS, 437, 1155 <http://ui.adsabs.harvard.edu/abs/2014MNRAS.437.1155K>`_

.. [M05] Matheson, T. et al. 2005, *Spectroscopy of High-Redshift Supernovae from the ESSENCE Project: The First 2 Years*, `AJ, 129, 2352 <http://ui.adsabs.harvard.edu/abs/2005AJ....129.2352M>`_

.. [MVB] Massey, P., Valdes, F., & Barnes, J. 1992, *User's Guide to Reducing Slit Spectra with IRAF*, (Tucson: NOAO/IRAF)

.. [SM14] Stanghellini et al. 2014, *The Radial Metallicity Gradient and the History of Elemental Enrichment in M81 Through Emission-line Probes*, `A&A, 567, 88 <http://ui.adsabs.harvard.edu/abs/2014A%26A...567A..88S>`_

.. [VD] van Dokkum, P. G. 2001, *Cosmic-Ray Rejection by Laplacian Edge Detection*, `PASP, 113, 1420 <http://ui.adsabs.harvard.edu/abs/2001PASP..113.1420V>`_

.. [NS] Abraham, R. G., et al. 2004, *The Gemini Deep Deep Survey. I. Introduction to the Survey, Catalogs, and Composite Spectra*, `AJ, 127, 2455 <http://ui.adsabs.harvard.edu/abs/2004AJ....127.2455A>`_

.. [HS] Hamuy, M., Suntzeff, N.B., Heathcote, S.R., Walker, A.R., Gigoux, P., & Phillips, M.M. 1994, *Southern Spectrophotometric Standards. II.*, `PASP, 106, 566 <http://ui.adsabs.harvard.edu/abs/1994PASP..106..566H>`_

.. [MG] Massey, P. & Gronwall, C. 1990, *The Kitt Peak Spectrophotometric Standards: Extension to 1 micron*, `ApJ, 358, 344 <http://ui.adsabs.harvard.edu/abs/1990ApJ...358..344M>`_

.. [MS] Massey, P. & Strobel, K. 1988, *Spectrophotometric Standards*, `ApJ, 328, 315 <http://ui.adsabs.harvard.edu/abs/1988ApJ...328..315M>`_

.. [FITS] Pence, W.D., Chiappetti, L, Page, C.G., Shaw, R.A., & Stobie, E. 2010, *Definition of the Flexible Image Transport System (FITS), Version 3.0*, `A&A, 524, 42 <http://ui.adsabs.harvard.edu/abs/2010A%26A...524A..42P>`_

.. [AP] Robitaille, T. P., et al. 2013, `Astropy: A Community Python Package for Astronomy <http://ui.adsabs.harvard.edu/abs/2013A%26A...558A..33A>`_ A&A, 558, 33
