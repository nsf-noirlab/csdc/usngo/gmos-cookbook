# Make a BPM for each CCD that includes the area used for spectroscopy.
# Values in the BPM different from 0 are treated as bad.
#
# This applies to GMOS-S, EEV, 1x1, Full

#fxdel rgS20120829S0067.fits 4
gemarith rgS20120829S0067.fits * 0 bpm_GSEEV_1x1_full outtype="ushort"
hedit bpm_GSEEV_1x1_full[sci,1] EXTNAME "DQ" upd+ ver-
hedit bpm_GSEEV_1x1_full[sci,2] EXTNAME "DQ" upd+ ver-
hedit bpm_GSEEV_1x1_full[sci,3] EXTNAME "DQ" upd+ ver-

imreplace bpm_GSEEV_1x1_full[dq,1][1909:1912,306:822] 1
imreplace bpm_GSEEV_1x1_full[dq,2][1909:1913,1:4608] 1
imreplace bpm_GSEEV_1x1_full[dq,2][1498:1501,3896:4608] 1
imreplace bpm_GSEEV_1x1_full[dq,2][408:412,1126:4608] 1
imreplace bpm_GSEEV_1x1_full[dq,2][782,2352:2820] 1
imreplace bpm_GSEEV_1x1_full[dq,2][1090,2062:2308] 1
imreplace bpm_GSEEV_1x1_full[dq,3][1415:1420,2622:4608] 1
imreplace bpm_GSEEV_1x1_full[dq,3][1116:1120,4530:4608] 1

# The longer science (& to a lesser extent std) data have an extra bad region
# with a slower tail-off, so make BPMs with that feature included; also some
# columns that are slightly dark in long exposures:
imreplace bpm_GSEEV_1x1_full[dq,2][1914:1925,305:823] 1

# Some faintly bad columns:
imreplace bpm_GSEEV_1x1_full[dq,1][947,50:550] 1
imreplace bpm_GSEEV_1x1_full[dq,2][646,340:570] 1
imreplace bpm_GSEEV_1x1_full[dq,2][782:783,*] 1
imreplace bpm_GSEEV_1x1_full[dq,2][1090,*] 1
imreplace bpm_GSEEV_1x1_full[dq,2][1223,2800:2900] 1
imreplace bpm_GSEEV_1x1_full[dq,2][1502,3896:4608] 1
imreplace bpm_GSEEV_1x1_full[dq,3][574,3430:3500] 1
imreplace bpm_GSEEV_1x1_full[dq,3][137,300:825] 1
imreplace bpm_GSEEV_1x1_full[dq,3][137,300:825] 1

# Dark glow feature:
imreplace bpm_GSEEV_1x1_full[dq,3][1810:1855,4115:4150] 1

# Some extra pixels in the longer std, as in the sci:
imreplace bpm_GSEEV_1x1_full[dq,2][1914:1925,305:823] 1
