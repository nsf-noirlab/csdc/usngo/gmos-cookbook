# GMOS Data Reduction Cookbook companion script to the chapter:
#   "Reduction of Multi-Object Spectra with IRAF" 
#
# IRAF CL command script to: 
# Process MOS exposures for M81 field 1, in program GN-2011B-C-3.
#    2016-Mar-06  shaw@noao.edu
#
# The names for the relevant header keywords and their expected values are described 
# in the DRC chapter entitled "Supplementary Material"
#
# Perform the following starting in the parent work directory:
#cd /path/to/work_directory

print ("### Begin Processing GMOS/MOS Spectra ###")
print ("###")
print ("---> You must have the MDF files:")
print ("--->    GN2011BC003-01.fits and GN2011BC003-02.fits")
print ("---> in your working directory. ")

#### Create Calibration Reference Files 
#
cd ./raw

print ("===Creating MasterCals===")
print (" --Selecting Bias Residual Exposures--")
#
## Select bias exposures. You may wish to partition by date interval.
# Bias exposures with a common observation class, RoI, and CCD binning.
s1 = "obstype?='BIAS' && obsclass?='dayCal' && detrO1ys>1024 && ccdsum?='2 2' "

# Select bias exposures within ~3 weeks of the target observations.
s2 = "&& @'date-obs' > '2012-01-16' && @'date-obs' < '2012-02-12'"
string biasSelect
biasSelect = s1 // s2

# Select exposures using information from both PHDU and HDU-1
hselect ("N*.fits[1,inherit=yes]", "$I", biasSelect, > "bias_tmp.txt")

# Now remove the IRAF index and kernel parameters from the file names
gemextn ("@bias_tmp.txt", omit="index,kernel", outfile="biasFull.txt")

# Also prepare CenterSpec RoI for the standard star.
biasSelect = "obstype?='BIAS' && obsclass?='dayCal' && detrO1ys==512 && ccdsum?='2 2'"
hselect ("N*.fits[1,inherit=yes]", "$I", biasSelect, > "bias_tmp.txt")
gemextn ("@bias_tmp.txt", omit="index,kernel", outfile="biasCenSp.txt")

### Flat-fields
#
print (" --Selecting Flat-field Expsoures--")
#
# Start with the GCAL flats for the LS spectra of the standard star.
# Flats must match the observation type & class, and CCD RoI & binning.
s1 = "obstype?='FLAT' && obsclass?='partnerCal' && detro1ys==512 && ccdsum?='2 2' "
string flatSelect

# Must also match the aperture, grating, & central wavelength.
#
# B600/420, CentSp
s2 = "&& grating?='B600+_G5307' && maskname?='1.0arcsec' && grwlen=420.0"
flatSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
gemextn ("@flt_tmp.txt", omit="index,kernel", outfile="flatCenSp_B6-420.txt")

# B600/520, CentSp
s2 = "&& grating?='B600+_G5307' && maskname?='1.0arcsec' && grwlen=520.0"
flatSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
gemextn ("@flt_tmp.txt", omit="index,kernel", outfile="flatCenSp_B6-520.txt")

# B600/620, CentSp
s2 = "&& grating?='B600+_G5307' && maskname?='1.0arcsec' && grwlen=620.0"
flatSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
gemextn ("@flt_tmp.txt", omit="index,kernel", outfile="flatCenSp_B6-620.txt")

# R400/620, CentSp
s2 = "&& grating?='R400+_G5305' && maskname?='1.0arcsec' && grwlen=620.0"
flatSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
gemextn ("@flt_tmp.txt", omit="index,kernel", outfile="flatCenSp_R4-620.txt")

# R400/740, CentSp
s2 = "&& grating?='R400+_G5305' && maskname?='1.0arcsec' && grwlen=740.0"
flatSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
gemextn ("@flt_tmp.txt", omit="index,kernel", outfile="flatCenSp_R4-740.txt")

## Full, GCAL, Mask01
# Must also match the grating, aperture, and central wavelength.
s1 = "obstype?='FLAT' && obsclass?='partnerCal' && detro1ys>1024 && ccdsum?='2 2' "

# B600/520
s2 = "&& grating?='B600+_G5307' && maskname?='GN2011BC003-01' && grwlen=520.0"
flatSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
gemextn ("@flt_tmp.txt", omit="index,kernel", outfile="flatFull-M01_B6-520.txt")

# R400/740
s2 = "&& grating?='R400+_G5305' && maskname?='GN2011BC003-01' && grwlen=740.0"
flatSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
gemextn ("@flt_tmp.txt", omit="index,kernel", outfile="flatFull-M01_R4-740.txt")

## Twilight flats for slit illumination
## Full, Mask01
# B600/520
s1 = "i_title?='Twilight' && obsclass?='dayCal' && detro1ys>1024 && ccdsum?='2 2' "
s2 = "&& grating?='B600+_G5307' && grwlen=520.0 && maskname?='GN2011BC003-01'"
flatSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
gemextn ("@flt_tmp.txt", omit="index,kernel", outfile="twiFull-M01_B6-520.txt")

# R400/740
s2 = "&& grating?='R400+_G5305' && grwlen=740.0 && maskname?='GN2011BC003-01'"
flatSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
gemextn ("@flt_tmp.txt", omit="index,kernel", outfile="twiFull-M01_R4-740.txt")

# Clean up.
dele ("*_tmp.txt")
rename ("bias*.txt,flat*.txt,twi*.txt", "../")

## Comparison arcs
print (" --Selecting comparison arcs--")
string arcSelect

# Arcs for Std stars
s1 = "obstype?='ARC' && detro1ys==512 && ccdsum?='2 2' "
s2 = "&& maskname?='1.0arcsec'"
arcSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", arcSelect, > "arc_tmp.txt")
gemextn ("@arc_tmp.txt", omit="index,kernel", outfile="arcCenSp.txt")

# Comparison arcs for Mask 01
# Select via observation type & class, CCD RoI & binning, and mask:
s1 = "obstype?='ARC' && obsclass?='progCal' && detro1ys>1024 && ccdsum?='2 2' "

# Select also by mask; grating, and cenWave. 
s2 = "&& maskname?='GN2011BC003-01' && grating?='B600+_G5307' && grwlen=520.0"
arcSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", arcSelect, > "arc_tmp.txt")
gemextn ("@arc_tmp.txt", omit="index,kernel", outfile="arcFull-M01_B6-520.txt")

s2 = "&& maskname?='GN2011BC003-01' && grating?='R400+_G5305' && grwlen=740.0"
arcSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", arcSelect, > "arc_tmp.txt")
gemextn ("@arc_tmp.txt", omit="index,kernel", outfile="arcFull-M01_R4-740.txt")

## Science exposures all have the same CCD RoI & binning.
print (" --Selecting science exposures--")

# Select via object name, observation class, mask, grating & cenWave:
s1 = "i_title?='M81-field1' && obsclass?='science' && maskname?='GN2011BC003-01' "
s2 = "&& grating?='B600+_G5307' && grwlen=520.0"
string sciSelect
sciSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", sciSelect, > "sci_tmp.txt")
gemextn ("@sci_tmp.txt", omit="index,kernel", outfile="m81Files-M01_B6-520.txt")

s2 = "&& grating?='R400+_G5305' && grwlen=740.0"
sciSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", sciSelect, > "sci_tmp.txt")
gemextn ("@sci_tmp.txt", omit="index,kernel", outfile="m81Files-M01_R4-740.txt")

## Standard stars all have the same CCD RoI & binning.
print (" --Selecting standard star exposures--")
string stdSelect

# Select via observation type & class, mask:
s1 = "obstype?='OBJECT' && obsclass?='partnerCal' && maskname?='1.0arcsec' "

# Select also via grating & cenWave, for each configuration:
s2 = "&& grating?='B600+_G5307' && grwlen=420."
stdSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", stdSelect, > "std_tmp.txt"
gemextn ("@std_tmp.txt", omit="index,kernel", outfile="stdFiles_B6-420.txt")

s2 = "&& grating?='B600+_G5307' && grwlen=520."
stdSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", stdSelect, > "std_tmp.txt"
gemextn ("@std_tmp.txt", omit="index,kernel", outfile="stdFiles_B6-520.txt")

s2 = "&& grating?='B600+_G5307' && grwlen=620."
stdSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", stdSelect, > "std_tmp.txt"
gemextn ("@std_tmp.txt", omit="index,kernel", outfile="stdFiles_B6-620.txt")

s2 = "&& grating?='R400+_G5305' && grwlen=620."
stdSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", stdSelect, > "std_tmp.txt"
gemextn ("@std_tmp.txt", omit="index,kernel", outfile="stdFiles_R4-620.txt")

s2 = "&& grating?='R400+_G5305' && grwlen=740."
stdSelect = s1 // s2
hselect ("N*.fits[1,inherit=yes]", "$I", stdSelect, > "std_tmp.txt"
gemextn ("@std_tmp.txt", omit="index,kernel", outfile="stdFiles_R4-740.txt")

## Move the file lists to the parent directory, and clean up.
dele ("*_tmp.txt")
rename ("arc*.txt,stdFiles*.txt,m81Files*.txt", "../")


print ("===Creating MasterCals===")
cd ..

print (" --Creating Bias Residual--")
# Use primarily the default task parameters.
unlearn gbias
unlearn gemextn    # Disarm a bug in gbias
gbias.log = "gbiasLog.txt"
gbias.rawpath="./raw"
gbias.verbose=no

gbias ("@biasCenSp.txt", "MCbiasCenSp.fits", fl_vardq+)
gbias ("@biasFull.txt", "MCbiasFull.fits", fl_vardq+)
imdele ("gN2012*.fits,gsN2012*.fits")

## Process flat-field, including VAR and DQ arrays.
print (" --Creating Flat-fields--")

# Use primarily the default task parameters.
unlearn gireduce
unlearn gsflat
gsflat.logfile="gsflatLog.txt"
gsflat.rawpath="./raw"
gsflat.verbose=no

# Normalize the spectral flats per CCD.
# Interactive curve fitting is better, but we won't interupt the flow here.
print (" --Flat-field normalization, non-interactive--")
#
gsflat ("@flatCenSp_B6-420.txt", "MCflatCenSp_B6-420", bias="MCbiasCenSp", \
   fl_detec+, fl_oversize-, fl_inter-, order="8")

gsflat ("@flatCenSp_B6-520.txt", "MCflatCenSp_B6-520", bias="MCbiasCenSp", \
   fl_detec+, fl_oversize-, fl_inter-, order="8")

gsflat ("@flatCenSp_B6-620.txt", "MCflatCenSp_B6-620", bias="MCbiasCenSp", \
   fl_detec+, fl_oversize-, fl_inter-, order="7")

gsflat ("@flatCenSp_R4-620.txt", "MCflatCenSp_R4-620", bias="MCbiasCenSp", \
   fl_detec+, fl_oversize-, fl_inter-, order="7")

gsflat ("@flatCenSp_R4-740.txt", "MCflatCenSp_R4-740", bias="MCbiasCenSp", \
   fl_detec+, fl_oversize-, fl_inter-, order="8")

gsflat ("@flatFull-M01_B6-520.txt", "MCflatFull-M01_B6-520",  bias="MCbiasFull", \
   fl_keep+, combflat="MCflatComb-M01_B6-520", fl_oversize-, fl_vardq+, fl_inter-, \
   fl_usegrad+, fl_detec-, fl_seprows-, order="53")

gsflat ("@flatFull-M01_R4-740.txt", "MCflatFull-M01_R4-740",  bias="MCbiasFull", \
   fl_keep+, combflat="MCflatComb-M01_R4-740", fl_oversize-, fl_vardq+, fl_inter-, \
   fl_usegrad+, fl_detec-, fl_seprows-, order="53")

# Clean up.
imdele ("gN2012*.fits,gsN2012*.fits")

### Basic processing
print ("===Begining Basic Processing===")
#
# Use primarily the default task parameters.
unlearn gsreduce
gsreduce.logfile="gsreduceLog.txt"
gsreduce.rawpath="./raw"
gsreduce.verbose=no

# Perform basic reductions on all long-slit arcs.
gsreduce ("@arcCenSp.txt", bias="MCbiasCenSp", fl_fixpix-, fl_flat-, fl_oversize-)

# Perform basic reductions on all MOS arcs.
gsreduce ("@arcFull-M01_B6-520.txt", bias="MCbiasFull", gradimage="MCflatComb-M01_B6-520", \
   fl_fixpix-, fl_flat-, fl_oversize-)
gsreduce ("@arcFull-M01_R4-740.txt", bias="MCbiasFull", gradimage="MCflatComb-M01_R4-740", \
   fl_fixpix-, fl_flat-, fl_oversize-)

# Long-slit std star exposures.
gsreduce ("@stdFiles_B6-420.txt", bias="MCbiasCenSp", flatim="MCflatCenSp_B6-420", fl_oversize-)
gsreduce ("@stdFiles_B6-520.txt", bias="MCbiasCenSp", flatim="MCflatCenSp_B6-520", fl_oversize-)
gsreduce ("@stdFiles_B6-620.txt", bias="MCbiasCenSp", flatim="MCflatCenSp_B6-620", fl_oversize-)

gsreduce ("@stdFiles_R4-620.txt", bias="MCbiasCenSp", flatim="MCflatCenSp_R4-620", fl_oversize-)
gsreduce ("@stdFiles_R4-740.txt", bias="MCbiasCenSp", flatim="MCflatCenSp_R4-740", fl_oversize-)

# Science exposures.
gsreduce ("@m81Files-M01_B6-520.txt", bias="MCbiasFull", flatim="MCflatFull-M01_B6-520", \
      gradimage="MCflatComb-M01_B6-520", fl_fixpix-, fl_oversize-, fl_vardq+, fl_fulldq+)
gsreduce ("@m81Files-M01_R4-740.txt", bias="MCbiasFull", flatim="MCflatFull-M01_R4-740", \
      gradimage="MCflatComb-M01_R4-740", fl_fixpix-, fl_oversize-, fl_vardq+, fl_fulldq+)

imdele ("gN2012*.fits")

### End of basic processing. Continue with advanced processing.
print ("===Finished Basic Calibration Processing===")
print ("\n")

## Cosmic ray rejection.
print ("===Begining multi-frame cosmic ray rejection===")
#
# Create a list of common settings for the target.
sections gs//@m81Files-M01_B6-520.txt > gsM81Files-M01_B6-520.txt
sections gs//@m81Files-M01_R4-740.txt > gsM81Files-M01_R4-740.txt

# Use primarily the default task parameters.
unlearn gemcombine
gemcombine.logfile="gemcombineLog.txt"
gemcombine.reject="ccdclip"
gemcombine.verbose=no

# Combine the exposures with outlier rejection for each grating.
gemcombine ("@gsM81Files-M01_B6-520.txt", "M81-field1_B6-520", fl_vardq+, fl_dqprop+)
gemcombine ("@gsM81Files-M01_R4-740.txt", "M81-field1_R4-740", fl_vardq+, fl_dqprop+)

# Clean up
imdele ("gN2012*.fits")

## Wavelength calibration.
print ("===Begining wavelength calibration===")
unlearn gswavelength
gswavelength.logfile="gswaveLog.txt"

# Begin with longslit Arcs.
# The fit to the dispersion relation should be performed interactively. 
# Here we will us a previously determined result.
#B600/420,520,620:
gswavelength ("gsN20120124S0247", fl_inter-, fwidth=6, order=5, nsum=50)
gswavelength ("gsN20120124S0246", fl_inter-, fwidth=6, order=5, nsum=50)
gswavelength ("gsN20120124S0248", fl_inter-, fwidth=6, order=5, nsum=50)
#R400/620,740,900:
gswavelength ("gsN20120124S0251", fl_inter-, fwidth=6, order=5, nsum=50)
gswavelength ("gsN20120124S0252", fl_inter-, fwidth=6, order=5, nsum=50)
gswavelength ("gsN20120125S0413", fl_inter-, fwidth=6, order=5, nsum=50)

# Now for MOS Arcs
sections gs//@arcFull-M01_B6-520.txt > gsArcFull-M01_B6-520.txt
sections gs//@arcFull-M01_R4-740.txt > gsArcFull-M01_R4-740.txt

# should achive RMS<0.20
gswavelength ("@gsArcFull-M01_B6-520.txt", fl_inter-, fwidth=6, order=7, nsum=20, step=2, verb-)
gswavelength ("@gsArcFull-M01_R4-740.txt", fl_inter-, fwidth=6, order=7, nsum=20, step=2, verb-)

# Apply wavelength calibration.
print (" --Applying wavelength calibration--")
unlearn gstransform
gstransform.logfile="gstransformLog.txt"
gstransform.verbose=no

# Long-slit standard stars
gstransform ("gsN20120124S0120", outimages="tG191B2B_B6-420", wavtraname="gsN20120124S0247")
gstransform ("gsN20120124S0117", outimages="tG191B2B_B6-520", wavtraname="gsN20120124S0246")
gstransform ("gsN20120124S0121", outimages="tG191B2B_B6-620", wavtraname="gsN20120124S0248")

gstransform ("gsN20120124S0128", outimages="tG191B2B_R4-620", wavtraname="gsN20120124S0251")
gstransform ("gsN20120124S0123", outimages="tG191B2B_R4-740", wavtraname="gsN20120124S0252")

gstransform ("gsN20120124S0229", outimages="tHZ44_B6-420", wavtraname="gsN20120124S0247")
gstransform ("gsN20120124S0226", outimages="tHZ44_B6-520", wavtraname="gsN20120124S0246")
gstransform ("gsN20120124S0230", outimages="tHZ44_B6-620", wavtraname="gsN20120124S0248")

# MOS mode
gstransform ("M81-field1_B6-520", outimages="tM81-field1_B6-520", wavtraname="gsN20120124S0169", fl_vardq+)
gstransform ("M81-field1_R4-740", outimages="tM81-field1_R4-740", wavtraname="gsN20120124S0187", fl_vardq+)

imdele ("gsN2012*.fits")

print ("===Begining Flux Calibration===")

## Sky subtraction. 
# This will require summing the spectra along columns, e.g.: 
#pcols tG191B2B_B6_520.fits[SCI] 1100 2040 wy1=40 wy2=320

# Subtract sky spectrum using selected regions. 
print (" --Subtracting sky from Std stars--")
# The regions should be selected with care, using e.g. prows/pcols. 
unlearn gsskysub
gsskysub.logfile="gsskysubLog.txt"
gsskysub.verbose=no

gsskysub ("tG191B2B_*", fl_oversize-, long_sample="95:195,320:420")
gsskysub ("tHZ44*", fl_oversize-, long_sample="100:200,320:420")

### Flux calibration with G191B2B and HZ44.
# Extract the std spectrum using a large aperture.
# It's important to trace the spectra interactively. 
unlearn gsextract
gsextract.logfile="gsextractLog.txt"
gsextract.verbose=no

gsextract ("stG191B2B_B6-520", fl_inter+, apwidth=3., tfunction="spline3", torder=8)
# Need to delete the bluest 10 points:
gsextract ("stG191B2B_B6-420", fl_inter+, apwidth=3., tfunction="spline3", torder=7)
# RMS: 4.7e-3
gsextract ("stG191B2B_B6-620", fl_inter+, apwidth=3., tfunction="spline3", torder=8, refimages="stG191B2B_B6-520")

# Considerable 2-nd order light longward of ~900. nm.
gsextract ("stG191B2B_R4-740", fl_inter+, apwidth=3., tfunction="spline3", torder=19)
gsextract ("stG191B2B_R4-620", fl_inter+, apwidth=3., tfunction="spline3", torder=19, refimages="stG191B2B_R4-740")

# RMS is 7.e-3:
gsextract ("stHZ44_B6-520", fl_inter-, apwidth=3., tfunction="spline3", torder=6)
# RMS = 0.039, if the bluest 7 points are removed.
gsextract ("stHZ44_B6-420", fl_inter-, apwidth=3., tfunction="spline3", torder=14, refimages="stHZ44_B6-520")
# RMS is 7.1e-3
gsextract ("stHZ44_B6-620", fl_inter-, apwidth=3., tfunction="spline3", torder=13, refimages="stHZ44_B6-520")

##########################
# Derive the sensitivity function.
# Copy the monochromatic mags file from ESO for G191B2B, and use it. 
unlearn gsstandard
gsstandard.logfile="gsstdLog.txt"

# Note that the S/N is very poor shortward of ~3300 Ang.
# We will use a newer calibration of G191B2B, placed in the work directory. 
gsstandard ("estG191B2B_B6*", sfile="std_B6", sfunction="sens_B6", fl_inter+, order=7, starname="g191b2b", caldir="./")
gsstandard ("estG191B2B_R4*", sfile="std_R4", sfunction="sens_R4", fl_inter+, order=7, starname="g191b2b", caldir="./")

### Extract target MOS spectra
gsextract ("tM81-field1_B6-520", fl_inter-, apwidth=2.5, tfunction="spline3", torder=1, tnsum=50, background="median", mos_bsample=1.0)
gsextract ("tM81-field1_R4-740", fl_inter-, apwidth=2.5, tfunction="spline3", torder=1, tnsum=50, background="median", mos_bsample=1.0)

## Apply the sensitivity function.
unlearn gscalibrate
gscalibrate.logfile="gscalibrateLog.txt"
gscalibrate.extinction="onedstds$kpnoextinct.dat"

gscalibrate ("etM81-field1_B6-520", sfunc="sens_B6", fl_ext+, fl_scale-)
gscalibrate ("etM81-field1_R4-740", sfunc="sens_R4", fl_ext+, fl_scale-)

print ("===Finished Sensitivity Processing===")