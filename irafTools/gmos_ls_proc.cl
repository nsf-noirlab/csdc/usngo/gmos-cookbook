# GMOS Data Reduction Cookbook companion script to the chapter:
#   "Reduction of Longslit Spectra with IRAF" 
#
# IRAF CL command script to: 
# Select GMOS calibration images for AM2306-721, in program GS-2007A-Q-76.
#    2016-Mar-04  shaw@noao.edu
#
# The names for the relevant header keywords and their expected values are described 
# in the DRC chapter entitled "Supplementary Material"
#
# Perform the following starting in the parent work directory:
#cd /path/to/work_directory
# 
# To run this script in an IRAF session, load the gemini, gemtools & gmos packages, 
# then enter the following at the IRAF prompt: 
#   cl < gmos_img_proc.cl

print ("### Begin Processing GMOS/MOS Spectra ###")
print ("###")

cd ./raw

print ("===Exosure Selection===")
print (" --Selecting Bias Exposures--")
#
## Select bias exposures. 
# Bias exposures with a common observation class, RoI, and CCD binning:
s1 = "obstype?='BIAS' && obsclass?='dayCal' && detrO1ys>1024 && ccdsum?='2 4'"

# Select bias exposures within ~2 months of the target observations:
s2 = " && @'date-obs' > '2007-06-05' && @'date-obs' < '2007-07-07'"
string biasSelect
biasSelect = s1 // s2

# Select exposures using information from both PHDU and HDU-1
hselect ("*.fits[1,inherit=yes]", "$I", biasSelect, > "bias_tmp.txt")

# Now remove the IRAF index and kernel parameters from the file names
gemextn ("@bias_tmp.txt", omit="index,kernel", outfile="biasFull.txt")

# Also prepare CenterSpec RoI for the standard star.
biasSelect = "obstype?='BIAS' && obsclass?='dayCal' && detrO1ys==256 && ccdsum?='2 4'"
hselect ("*.fits[1,inherit=yes]", "$I", biasSelect, > "bias_tmp.txt")
gemextn ("@bias_tmp.txt", omit="index,kernel", outfile="biasCenSp.txt")

### Flat-fields
#
print (" --Selecting Flat-field Expsoures--")
#
## Flat-fields must match the observation type, RoI and CCD binning.
s1 = "obstype?='FLAT' && obsclass?='partnerCal' && detro1ys>1024 && ccdsum?='2 4'"

# Must also match the grating, aperture, and central wavelength:
s2 = "&& grating?='B600+_G5323' && maskname?='1.0arcsec' && grwlen=485.0"
string flatSelect
flatSelect = s1 // s2

# Generate lists of calibration filenames using above criteria.
hselect ("S*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
gemextn ("@flt_tmp.txt", omit="index,kernel", outfile="flatFull.txt")

### Science, Arcs, Standards
#
s2 = "&& grating?='B600+_G5323' && maskname?='1.0arcsec' && grwlen=485.0"
#
print (" --Selecting Comparison Arc Expsoures--")
#
# Select comparison arc exposures via observation type, CCD RoI & binning:
s1 = "obstype?='ARC' && obsclass?='progCal' && detro1ys>1024 && ccdsum?='2 4'"
string arcSelect
arcSelect = s1 // s2
hselect ("S*.fits[1,inherit=yes]", "$I", arcSelect, > "arc_tmp.txt")
gemextn ("@arc_tmp.txt", omit="index,kernel", outfile="arcFull.txt")

print (" --Selecting Science Expsoures--")
#
# Select science exposures via observation type, RoI and CCD binning.
# This will also select data from object AM1401. Delete these filename if you want.
s1 = "obstype?='OBJECT' && obsclass?='science' && detro1ys>1024 && ccdsum?='2 4'"
string sciSelect
sciSelect = s1 // s2
hselect ("S*.fits[1,inherit=yes]", "$I", sciSelect, > "sci_tmp.txt")
gemextn ("@sci_tmp.txt", omit="index,kernel", outfile="sciFiles.txt")

print (" --Selecting Standard Star Expsoures--")
#
s1 = "obstype?='OBJECT' && obsclass?='partnerCal' && detro1ys==256 && ccdsum?='2 4'"
string stdSelect
stdSelect = s1 // s2
hselect ("S*.fits[1,inherit=yes]", "$I", stdSelect, > "std_tmp.txt"
gemextn ("@std_tmp.txt", omit="index,kernel", outfile="stdFiles.txt")

# Place all file lists in parent directory and clean up.
dele ("*_tmp.txt")
rename ("bias*.txt,flat*.txt,arc*.txt,stdFiles.txt,sciFiles.txt", "../")

print ("===Creating MasterCals===")
cd ..
print (" --Creating Bias Residual MasterCals--")

## Create Bias Residual, including VAR and DQ arrays.
# Use primarily the default task parameters.
unlearn gbias
unlearn gemextn    # Disarm a bug in gbias
gbias.log = "gbiasLog.txt"
gbias.rawpath="./raw"
gbias.verbose=no

gbias ("@biasCenSp.txt", "MCbiasCenSp", fl_vardq+)
gbias ("@biasFull.txt", "MCbiasFull", fl_vardq+)

print (" --Creating Flat-field MasterCals--")
#
## Process flat-field, including VAR and DQ arrays.
# Use primarily the default task parameters.
unlearn gireduce
unlearn gsflat
gsflat.logfile="gsflatLog.txt"
gsflat.rawpath="./raw"
gsflat.verbose=no

# Normalize the spectral flats per CCD.
# Interactive curve fitting is better, but we won't interupt the flow here.
print ("--Flat-field normalization, non-interactive--")
#
gsflat ("@flatFull.txt", "MCflatFull.fits",  bias="MCbiasFull.fits", \
   fl_vardq+, fl_fulldq+, fl_detec+, fl_oversize-, fl_inter-, order="13,11,28")

gsflat ("S20070623S0108", "MCflatCenSp.fits", bias="MCbiasCenSp.fits", \
   fl_vardq+, fl_fulldq+, fl_detec+, fl_oversize-, fl_inter-, order="13,11,28")

### Perform basic processing
print ("===Processing Science Files===")
print (" --Performing Basic Processing--")

# Use primarily the default task parameters.
unlearn gsreduce
gsreduce.logfile="gsreduceLog.txt"
gsreduce.rawpath="./raw"
gsreduce.verbose=no

# Perform basic reductions on all exposures for science targets.
gsreduce ("@sciFiles.txt", bias="MCbiasFull", flatim="MCflatFull", \
   fl_fixpix-, fl_oversize-, fl_vardq+, fl_fulldq+)

# Perform basic reductions on the Arcs and standard star.
gsreduce ("@arcFull.txt", bias="MCbiasFull", \
   fl_fixpix-, fl_flat-, fl_oversize-)
gsreduce ("S20070623S0109", bias="MCbiasCenSp", \
   fl_fixpix-, fl_flat-, fl_oversize-)

gsreduce ("@stdFiles.txt", bias="MCbiasCenSp", flatim="MCflatCenSp", \
   fl_fixpix+, fl_oversize-, fl_vardq-)

### End of basic processing. Continue with advanced processing.
## Cosmic ray rejection.
print (" --Perform multi-frame cosmic ray rejection--")
#
# Create a list of common slit alignments for the target & standar star.
sections gs//@stdFiles.txt > gsStdFiles.txt
s1 = "obstype?='OBJECT' && obsclass?='science' "
hselect ("gsS*.fits[0]", "$I", (s1 // "&& i_title?='AM2306-721_a'"), > "AM2306_tmp.txt")
gemextn ("@AM2306_tmp.txt", omit="index", outfile="AM2306a.txt")

hselect ("gsS*.fits[0]", "$I", (s1 // "&& i_title?='AM2306-72_b'"), > "AM2306_tmp.txt")
gemextn ("@AM2306_tmp.txt", omit="index", outfile="AM2306b.txt")

hselect ("gsS*.fits[0]", "$I", (s1 // "&& i_title?='AM2306-721_c'"), > "AM2306_tmp.txt")
gemextn ("@AM2306_tmp.txt", omit="index", outfile="AM2306c.txt")
dele ("AM2306_tmp.txt")

print (" --Combine science exposures--")

# Use primarily the default task parameters.
unlearn gemcombine
gemcombine.logfile="gemcombineLog.txt"
gemcombine.reject="ccdclip"
gemcombine.verbose=no

# Combine the exposures with outlier rejection for each orientation.
gemcombine ("@gsStdFiles.txt", "LTT9239.fits", fl_vardq-, fl_dqprop+)
gemcombine ("@AM2306a.txt", "AM2306a", fl_vardq+, fl_dqprop+)
gemcombine ("@AM2306b.txt", "AM2306b", fl_vardq+, fl_dqprop+)
gemcombine ("@AM2306c.txt", "AM2306c", fl_vardq+, fl_dqprop+)

# Clean up
imdele ("gS2007*.fits")

print (" --Perform wavelength calibration--")
#
# Use primarily the default task parameters.
unlearn gswavelength
gswavelength.logfile="gswaveLog.txt"

# In this case, the default medium-resolution line list will work well.
gswavelength.coordlist="gmos$data/CuAr_GMOS.dat"

# The fit to the dispersion relation should be performed interactively. 
# Here we will us a previously determined result.
gswavelength ("gsS20070623S0071", fl_inter-, fwidth=6, order=5, nsum=50)
gswavelength ("gsS20070623S0081", fl_inter-, fwidth=6, order=5, nsum=50)
gswavelength ("gsS20070623S0091", fl_inter-, fwidth=6, order=5, nsum=50)
gswavelength ("gsS20070623S0109", fl_inter-, fwidth=6, order=5, nsum=50)

print (" --Apply wavelength calibration--")
#
# Use primarily the default task parameters.
unlearn gstransform
gstransform.logfile="gstransformLog.txt"

gstransform ("LTT9239", wavtraname="gsS20070623S0109", fl_vardq+)
gstransform ("AM2306a", wavtraname="gsS20070623S0071", fl_vardq+)
gstransform ("AM2306b", wavtraname="gsS20070623S0081", fl_vardq+)
gstransform ("AM2306c", wavtraname="gsS20070623S0091", fl_vardq+)

#Clean up.
imdele ("gsS2007*.fits")

print (" --Perform sky subtraction--")
#
## Sky subtraction. This will require summing the spectra along columns, e.g.: 
#pcols tAM2306b.fits[SCI] 1100 2040 wy1=40 wy2=320

# Subtract sky spectrum using selected regions. 
# The regions should be selected with care, using e.g. prows/pcols. 
unlearn gsskysub
gsskysub.logfile="gsskysubLog.txt"

gsskysub ("tLTT9239", fl_oversize-, fl_vardq-, long_sample="20:70,190:230")
gsskysub ("tAM2306a", fl_oversize-, fl_vardq+, long_sample="520:720")
gsskysub ("tAM2306b", fl_oversize-, fl_vardq+, long_sample="670:760,920:1020")
gsskysub ("tAM2306c", fl_oversize-, fl_vardq+, long_sample="170:380,920:1080")

print (" --Extract Std spectrum--")
#
### Flux calibration with LTT9239.
# Extract the std spectrum using a large aperture.
# It's important to do this interactively. 
unlearn gsextract
gsextract.logfile="gsextractLog.txt"
gsextract ("stLTT9239", fl_inter-, apwidth=3., tfunction="spline3", torder=9)

print (" --Perform Flux calibration--")
#
# Derive the sensitivity function.
# Be sure to download the custom Mauna Kea atmospheric exteinction function. 
unlearn gsstandard
gsstandard.logfile="gsstdLog.txt"
gsstandard.caldir="onedstds$ctionewcal/"

gsstandard ("estLTT9239", sfile="std", sfunction="sens", fl_inter-, \
   order=7, starname="l9239", extinction="./mk_extinct.txt")

## Apply the sensitivity function.
unlearn gscalibrate
gscalibrate.logfile="gscalibrateLog.txt"
gscalibrate.extinction="./mk_extinct.txt"

gscalibrate ("stAM2306*", sfunc="sens", fl_ext+, fl_scale-, fl_vardq+)
gscalibrate ("estLTT9239", sfunc="sens", fl_ext+, fl_scale-)

print (" --Extract Target Spectra--")
#
nsum=4
sarith ("cstAM2306b.fits[SCI]", "copy", "", "ecstAM2306b.ms", apertures="222-346x4")

print ("===Finished Calibration Processing===")