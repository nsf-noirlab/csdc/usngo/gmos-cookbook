# Make a BPM for each CCD that includes the area used for imaging.
# Values in the BPM are:
#   0 pixel is good
#   1 pixel is unilluminated, or otherwise bad
#
# This applies to GMOS-S, EEV(old), 1x1, Full, imaging, MEF
#
# Start with the H-alpha flat-field MasterCal constructed for program GS-2006B-Q-18

copy MCflat_ha.fits bpm.fits

# Delete all extensions except the DQs
fxdel bpm.fits groups="1,2,3,4,6,8"

# Threshold the normalized flat-field intensity to select unilluminated region.
# Temporary files are needed here.
imcalc MCflat_ha.fits[sci,1] imfov_1.fits[DQ,1] "if im1 > 0.7 then 0 else 1" pixtype="ushort"
imcalc MCflat_ha.fits[sci,2] imfov_2.fits[DQ,2] "if im1 > 0.7 then 0 else 1" pixtype="ushort"
imcalc MCflat_ha.fits[sci,3] imfov_3.fits[DQ,3] "if im1 > 0.7 then 0 else 1" pixtype="ushort"

imcopy imfov_1.fits[DQ,1] bpm.fits[DQ,1,overwrite+]
imcopy imfov_2.fits[DQ,2] bpm.fits[DQ,2,overwrite+]
imcopy imfov_3.fits[DQ,3] bpm.fits[DQ,3,overwrite+]

imdele imfov*.fits

# Fix up the BPM header a bit. 
hedit bpm.fits[0] ADC* "" delete+ verify- update+
hedit bpm.fits[0] DEC "" delete+ verify- update+
hedit bpm.fits[0] DEW* "" delete+ verify- update+
hedit bpm.fits[0] DT* "" delete+ verify- update+
hedit bpm.fits[0] EQUINOX "" delete+ verify- update+
hedit bpm.fits[0] HA "" delete+ verify- update+
hedit bpm.fits[0] *OFFSET "" delete+ verify- update+
hedit bpm.fits[0] PRES* "" delete+ verify- update+
hedit bpm.fits[0] *OFF* "" delete+ verify- update+
hedit bpm.fits[0] *EQUI* "" delete+ verify- update+
hedit bpm.fits[0] RA "" delete+ verify- update+
hedit bpm.fits[0] RADECSYS "" delete+ verify- update+
hedit bpm.fits[0] RAW* "" delete+ verify- update+
hedit bpm.fits[0] WIND* "" delete+ verify- update+

string bpm = "bpm.fits[DQ,"
for (i=1; i<=3; i+=1) {
   s2=bpm//i//"]"
   hedit (s2, "CD*", " ", delete+, verify-, update+)
   hedit (s2, "CR*", " ", delete+, verify-, update+)
   hedit (s2, "CT*", " ", delete+, verify-, update+)
   hedit (s2, "GAIN*", " ", delete+, verify-, update+)
   hedit (s2, "LT*", " ", delete+, verify-, update+)
   hedit (s2, "WA*", " ", delete+, verify-, update+)
}

# Rename the file to something more meaningful.
imrename bpm.fits bpm_gmos-s_EEV_v1_2x2_img_MEF.fits
