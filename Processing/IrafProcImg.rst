.. _iraf-proc-img:

=============================
Reduction of Images with IRAF
=============================
This tutorial will use observations from program GS-2006B-Q-18 (PI: J. Arias): narrow- and broad-band imaging of three adjacent regions in M8 (or NGC 6523, the Lagoon Nebula). 
The science goals likely included studying the ionization and small-scale structures in this star forming region. 

The observations were obtained in Queue mode during several nights between 2006 Sep. 10 and Oct. 10. 
The science images were obtained in 5 filters: ``Ha``, ``HaC``, ``SII``, ``r``, and ``i``. 
(See the `description of the filters <https://www.gemini.edu/instrumentation/gmos/components#Filters>`_.)
The exposures were obtained with :math:`2\times2` binning (or about :math:`0.16\times0.16` arcsec, which still samples the PSF very well). 
Contemporaneous dayCal exposures were obtained including Twilight flats in all the filters, but not including photometric standards. 

For other tutorials, see the following links:

* :ref:`iraf-proc-ls` 
* :ref:`iraf-proc-mos`
* :ref:`iraf-proc-ifu`

Retrieve & Organize Data
------------------------
The first step is to retrieve the data from the Gemini Observatory Archive (see :ref:`archive-search`). 
You may search the `GOA <https://archive.gemini.edu/searchform>`_ yourself, or instead just cut-and-paste the direct URL: 

.. code-block:: html

   # M8 imaging data:
   https://archive.gemini.edu/searchform//sr=600/GS-2006B-Q-18/notengineering/GMOS-S/imaging/NotFail/present/canonical

After retrieving the science data, click the **Load Associated Calibrations** tab on the search results page and download the associated bias and flat-field exposures. 
Unpack all of them in a subdirectory of your working directory named ``/raw``. 
Be sure to uncompress the files. 
See :ref:`retrieve-data` for details. 

The steps below assume that the :term:`MasterCal` reference files have been created for all the filters. 
The required **MasterCals** are: 

* Bias Residual
* Flat-field (from twilight flats)

All of them will be constructed in this tutorial. 
See :ref:`master-ref` for details. 

Example Reduction Script
^^^^^^^^^^^^^^^^^^^^^^^^
You can perform all of the processing steps for this tutorial by downloading the MOS tutorial CL script. 

* Download: :download:`gmos_img_proc.cl </irafTools/gmos_img_proc.cl>` 

Place the script in the work directory. 
Within an IRAF session load the **gemini**, **gemtools**, and **gmos** packages, then execute it in command-mode:

.. code-block:: c

   cd /path/to/work_directory
   cl < gmos_img_proc.cl

You may find it useful to download the script to follow this tutorial in detail, and use it as the basis for reducing other imaging observations. 
The processing steps for this tutorial are described below, but only a subset of the commands are given in the interest of brevity. 

Create File Lists
^^^^^^^^^^^^^^^^^
The next steps will create *lists* of calibrations and science files for use in processing. 
The selection is achieved by matching a specific combination of header keyword-value pairs (see :ref:`dr-keywords`). 
Examples of file selection with the `imutil.hselect <https://iraf.net/irafhelp.php?val=hedit>`_ task are given below. 
Note the use of ``gemtools.gemextn`` to remove the FITS kernel syntax from the resulting file names. 

.. code-block:: c

   cd /path/to/work_directory/raw

   ## Bias exposures with a common observation class, RoI, and CCD binning:
   s1 = "obstype?='BIAS' && obsclass?='dayCal' && detrO1ys>1024 && ccdsum?='2 2'"

   # Select bias exposures within a few weeks of the target observations:
   s2 = "&& @'date-obs' > '2006-09-01' && @'date-obs' < '2006-10-30'"
   string biasSelect
   biasSelect = s1 // s2

   # Select biases using information from both PHDU and HDU-1
   hselect ("*.fits[1,inherit=yes]", "$I", biasSelect, > "bias_tmp.txt")
   string omit = "exten,index,kernel"
   gemextn ("@bias_tmp.txt", omit=omit, outfile="biasFiles.txt")

   ## Flat-fields must match: Object, RoI, CCD binning, grating & aperture.
   # Note: IRAF refers to the OBJECT keyword with the special name: "i_title" 
   s1 = "i_title?='Twilight' && obsclass?='dayCal' "

   # Flat-fields must also match the RoI, CCD binning, grating and aperture:
   s2 = "&& detro1ys>1024 && ccdsum?='2 2' && grating?='MIRROR' && maskname?='None' "

   # Select flats obtained contemporaneously with the observations
   string date
   date = "&& @'date-obs' > '2006-09-10' && @'date-obs' < '2006-10-10'"
   string flt
   flt = s1 // s2 // date

   # Generate lists of calibration filenames using above criteria & filter.
   hselect ("S*.fits[1,inherit=yes]", "$I", \
      (flt // "&& filter2?='r_G0326'"), > "flt_tmp.txt")
   gemextn ("@flt_tmp.txt", omit=omit, outfile="flat_r.txt")
   #...and so on for each filter. 

If you are concerned about the applicability of flat-field exposures taken weeks apart from the corresponding science exposures, it is simple to alter the ``date`` condition above and build more customized **Flat-field MasterCals**. 
Now build the list of science images.  

.. code-block:: c

   # Select science exposures via obs. type & class, and by CCD RoI and binning...
   string sci
   s1 = "obstype?='OBJECT' && obsclass?='science' "
   s2 = "&& detro1ys>1024 && ccdsum?='2 2' && grating?='MIRROR' && maskname?='None' "
   sci = s1 // s2

   hselect ("S*.fits[1,inherit=yes]", "$I", \
      (sci // "&& filter2?='Ha_G0336'"), > "sci_tmp.txt")
   gemextn ("@sci_tmp.txt", omit=omit, outfile="sci_ha.txt")
   # ...and so on for each filter. 

   rename ("biasFiles.txt,flat*.txt,sci*.txt", "../")

Most of the other processing lists you will need may be built from the above lists and the IRAF `sections <https://iraf.net/irafhelp.php?val=sections>`_ task, as shown below. 
Be sure to move the list files to the parent directory for processing.

Basic Processing
----------------
Basic image reductions are performed by the ``gireduce`` task, using the **Static BPM**, **Bias** and **Flat-field MasterCal** files. 

* See :ref:`bpm-files` for the location of the appropriate BPM files.
* See :ref:`master-ref` to create the other **MasterCals**. 

The ``gireduce`` task has more than 50 parameters; the table below lists the defaults for the processing flag keywords---i.e., the keywords with logical values to indicate whether to perform an operation. 
For the most part you can use the default parameter values; exceptions are noted explicitly in the code blocks below. 

.. csv-table:: **gireduce Processing Flag Defaults**
   :header: "Flag", "Default", "Description"
   :widths: 12, 8, 50

   ``fl_addmdf``,    No, Append :term:`MDF` extension? Not applicable to imaging.
   ``fl_bias``,      Yes, Subtract bias residual?
   ``fl_dark``,       No, Subtract scaled dark image?
   ``fl_flat``,      Yes, Apply flat-field correction?
   ``fl_mult``,     Yes, Multiply by the CCD gains?
   ``fl_inter``,      No, Fit overscan levels interactively? 
   ``fl_over``,      Yes, Perform overscan correction?
   ``fl_qecorr``,     No, Apply QE correction?
   ``fl_trim``,      Yes, Trim overscan region?
   ``fl_vardq``,      No, Propagate VAR and DQ?
 
All of the processing steps below can be performed from the work directory on the lists of files constructed above, using the IRAF ``@filelist`` notation. 

.. code-block:: c

   # Use primarily the default task parameters.
   unlearn gireduce
   gireduce.logfile="gireduceLog.txt"
   gireduce.rawpath="./raw"
   gireduce.bpm="./bpm_gmos-s_EEV_v1_2x2_img_MEF.fits"

   # Perform basic reductions on exposures, 1 filter at a time.
   gireduce ("@sci_ha.txt", bias="MCbias", fl_vardq+, flat1="MCflat_ha", verb-)

Use similar commands to reduce the remaining filters. 
Examine the output files (now prefixed with ``rg``) to assess data quality, and adjust the processing or the lists of input files if necessary. 

.. warning::

   Though one might ordinarily use an ``@list`` of all science files and set each filter in **gireduce** with ``flat1=,... flat4=``, this will likely result in an unhelpful error message if the set of input images includes more than 4 filters (as it does in this tutorial). So in this tutorial the exposures in a given filter will be processed separately. 

Mosaic Extensions
^^^^^^^^^^^^^^^^^
The next step is to mosaic the multiple image extensions in each file into one extension. 
The **gmosaic** task will transform the image extensions, taking into account the relative offsets and orientations of the CCDs, and generate a single (interpolated) image. 
Unfortunately, **gmosaic** appears not to support ``@filelist`` notation reliably, so explicit loops must be used.

.. code-block:: c

   # Use primarily the default task parameters.
   unlearn gmosaic
   # disarm a bug in gmosaic:
   unlearn gemextn
   gemextn.logfile="gmosaicLog.txt"

   # Create the list of images to process, and mosaic the extensions in each
   sections ("rg//@sci_ha.txt", > "rgsci_ha.txt")
   list = "rgsci_ha.txt"
   while (fscan (list, s1) != EOF) {
       gmosaic (s1, geointer="nearest", fl_vardq+, fl_fulldq+)
   }

The output files will have the same name as the input, but prefixed with ``m``. 

.. caution::

   It is important to use the **gmosaic** task to combine the extensions, as information about the relative position and orientation of the CCDs in the :term:`FPA` is hard-coded within the task. There are other options beyond **imcoadd** (discussed below) to combine exposures once they are mosaiced. 

Fringe Correction
^^^^^^^^^^^^^^^^^
A fringe pattern may be apparent in the extreme red (*i*-band) for some CCDs. 
This pattern is derived from many low-background science images (with very few extended targets) by masking the targets and combining many dithered (preferably, non-overlapping) images that have been flat-fielded. 
The **gmos.gifringe** task would be used to construct the fringe frame, and the **gmos.girmfinge** task would scale and remove the pattern from science images. 
Since this science program obtained images of a single, extremely extended emission line region, the genuine astrophysical intensity variations completely overwhelm the fringe signature. 
The purist may wish to derive a fringe frame from from another science program (but with the same CCDs), obtained under similar conditions of sky background. 
Such an approach is beyond the scope of this tutorial, however. 

This marks the end of calibration processing for GMOS images; what to do next depends on your specific science goals. 
Other tools in the **gemini** package, or other third-party tools may be essential for your analysis. 

Advanced Processing
-------------------
Image Stacking
^^^^^^^^^^^^^^
If there is more then one overlapping exposure of the same field taken with the same filter you may wish to stack them to, e.g., create a deep image for source identification or, as in this case, to create a mosaic of offset exposures to cover an extended target. 
This can be accomplished in part with the ``gemtools.imcoadd`` task, which handles DQ masking and outlier rejection. 
It does have some limitations, however: 

* It takes a lot of patience and trial-and-error to get good results
* There is little control over sky background
* The output image is no bigger than the first (reference) image, rather than the union of the footprints

The best procedure for ``imcoadd`` stacking depends upon the details of the program and the observing sequence. 
In this program, M8 was observed in 3 marginally overlapping positions, and several dithered exposures were obtained at each position. 
The complex background requires that the dithered images at each position be combined first; tiling the images from the 3 positions requires subsequent processing. 

.. code-block:: c

   # Select the files per filter, and per object region.
   s1 = "i_title?='M8-1' && "
   s2 = "filter2?='r_G0326'"
   hselect ("mrg*.fits[0]", "$I", (s1 // s2), > "mrg_tmp.txt")
   gemextn ("@mrg_tmp.txt", omit=omit, outfile="m8-1_r.txt")
   # ...and so on for each position and filter.

   # Co-add the images.
   # Use primarily the default task parameters.
   unlearn imcoadd
   imcoadd.logfile="imcoaddLog.txt"
   imcoadd.fwhm=3
   imcoadd.datamax=6.e4
   imcoadd.geointer="nearest"

   imcoadd ("@m8-1_r.txt", outimage="m8-1_r.fits")
   # ...and so on for each position and filter.

Proceed as above for the other object positions and filters. 

.. warning::

   Images with a complex background, such as narrow-band images of a region of extended nebulosity, may not combine well. This is in large part because **imcoadd** uses a very simple characterization of the background, and confuses it with genuine astrophysical background. Unfortunately the ``statsec`` parameter (where the sky is sampled) is specified in image pixels, rather than world (Ra, Dec) coordinates, so that regions on the celestial sphere cannot be specified. 

Inspect the output images for quality; tweak the task parameters as necessary for improved results. 
These co-added images may be tiled *after* the background levels have been brought into agreement. 

.. _iraf-proc-wcs:

WCS Refinement
^^^^^^^^^^^^^^
GMOS images have a complete WCS description in the header, with values for the reference coordinate and rotation angle as obtained in the observing environment. 
These values are believed to yield accurate relative celestial coordinates within 0.2 arcsec, with typical absolute accuracy of about 5 arcsec. 

An approximate correction to the reference point can be determined using the DS9 image display server, as described in :ref:`refine-imaging-wcs`. 
In this case, it appears that the image WCS is rotated slightly with respect to available catalogs, such as the HST Guide-Star 2.0. 
A fix requires determining the change in rotation, and modifying (with `hedit <https://iraf.net/irafhelp.php?val=hedit>`_) the ``CDi_j`` keywords in the header.

To refine the WCS still further, and to characterize distortions in the image (using the ``TNX`` projection), you can use the IRAF ``mscfinder.msctpeak`` task; a `tutorial <https://iraf.net/irafdocs/astrometry/astrom.html#msctpeak>`_ is available. 
This task requires a reasonably good approximate WCS, so the above steps are likely to be necessary. 

.. caution::
   If you use the image display server `SAOImage DS9 <http://ds9.si.edu/site/Home.html>`_, you may need to set an environment variable **before starting the cl** in order for IRAF to communicate with it. 

.. code-block:: csh

   setenv IMTDEV inet:5137     # (t)csh users
   export IMTDEV="inet:5137"   # bash users

Photometric Calibration
^^^^^^^^^^^^^^^^^^^^^^^
Photometric calibration for images is beyond the scope of this *Cookbook*, and there no standard way of recording the calibration within the images. 
However the mechanism in its simplest form is straightforward: Use a photometry program such as `SExtractor <http://www.astromatic.net/software/sextractor>`_ to measure instrumental magnitudes in each passband for detected stars, and determine the photometric zero-point for each image. 
Complications arise for very crowded fields (where stellar PSFs overlap), and PSF shapes that vary over the :term:`FoV`. 

If you are interested in deriving photometric zero-points and color terms, you may find it helpful to consult the paper by Jorgensen 2009, [CP]_ where these quantities are derived for the broad-band SDSS filters over a two-year period early in the life of GMOS. 
