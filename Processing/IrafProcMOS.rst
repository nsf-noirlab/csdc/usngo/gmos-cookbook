.. _iraf-proc-mos:

===========================================
Reduction of Multi-Object Spectra with IRAF
===========================================
This tutorial will reduce selected observations from program GN-2011B-C-3: MOS spectra of the H_II region populations in NGC185, and two distinct fields in M81 (NGC3031), as well as the Galactic planetary nebula PN_G235.3-03.9. 
The observing program was executed on the night of 2012 Jan 24-25, and included *g*-band imaging of the fields of interest (partly for acquisition of the targets), and MOS spectroscopy using gratings:

* B600 centered at 520 and 525 nm. 
* R400 centered at 525, 740 and 745 nm

The original science goal was to determine chemical abundances from emission line ratios in several H_II regions within the galaxy. 
Flux calibration standards were included in the observing plan, as were comparison arcs, GCAL and twilight flats. 

For other tutorials, see the following links:

* :ref:`iraf-proc-img` 
* :ref:`iraf-proc-ls`
* :ref:`iraf-proc-ifu`

Retrieve & Organize Data
------------------------
The first step is to retrieve the data from the Gemini Observatory Archive (see :ref:`archive-search`). 
You may search the `GOA <https://archive.gemini.edu/searchform>`_ yourself, but be sure to restrict the search to limit the number of science and calibration files to the relevant for reducing these spectra:[#2]_ 

* **Mode:** *Spectroscopy*
* **Adaptive Optics:** *Not AO*
* **Not&Shuffle:** *Classic*
* **Binning:** :math:`2\times2`

.. rubric:: Footnotes

.. [#2] You will also need to download the M81 masks: `GN2011BC003-01.fits <https://archive.gemini.edu/file/GN2011BC003-01.fits>`_ and `GN2011BC003-02.fits <https://archive.gemini.edu/file/GN2011BC003-02.fits>`_, and place them in your work directory. 

You may alternatively cut-and-paste the direct URL in your browser. 

.. code-block:: html

   # M81 H_II region MOS spectroscopic data:
   https://archive.gemini.edu/searchform/Classic/cols=CTOWMDBELQ/NOTAO/notengineering/2x2/GN-2011B-C-3/20120101-20120301/GMOS-N/NotFail/spectroscopy#

After retrieving the science data, click the **Load Associated Calibrations** tab on the search results page and download the associated bias and flat-field exposures. 
See :ref:`retrieve-data` for details. 
Unpack all of them in a subdirectory of your working directory named ``/raw``. 
Be sure to uncompress the files. 

It is *essential* that you review the observing log to decide how to select exposures for processing. 
It is advisable to display the files of interest to eliminate any bad exposures before they corrupt the reduction processing. 

Observing Configurations
^^^^^^^^^^^^^^^^^^^^^^^^
The table below shows the set of observing configurations that were used for the targets in this program. 
Since the number of configurations is large, this tutorial will be restricted (*filled squares*) to reducing MOS exposures of the first M81 field and associated calibrations, mostly for gratings B600/520 and R400/740. 
Note that the standard star exposures were observed with the Central Spectrum :term:`RoI`, but most other exposures were obtained with the Full RoI. 

.. csv-table:: **Observing Configurations Used for GN-2011B-C-3**
   :header: Config., NGC 185, G191-B2B, "PN G235.3", "M81-field1", "M81-field2", "HZ44"
   :widths: 12, 10, 11, 11, 8, 8, 8

   B600/420,                  , :math:`\blacksquare`,                 ,                     ,                 , :math:`\blacksquare`
   B600/520,  :math:`\diamond`, :math:`\blacksquare`, :math:`\diamond`, :math:`\blacksquare`, :math:`\diamond`, :math:`\blacksquare`
   B600/525,  :math:`\diamond`, :math:`\blacksquare`, :math:`\diamond`, :math:`\blacksquare`,                 , 
   B600/620,                  , :math:`\blacksquare`,                 ,                     ,                 , 
   R400/420,                  , :math:`\blacksquare`,                 ,                     ,
   R400/525,                  ,                     , :math:`\diamond`, :math:`\blacksquare`, :math:`\diamond`, 
   R400/620,                  , :math:`\blacksquare`,                 ,                     ,                 , :math:`\blacksquare`
   R400/740,  :math:`\diamond`, :math:`\blacksquare`, :math:`\diamond`, :math:`\blacksquare`, :math:`\diamond`
   R400/745,  :math:`\diamond`,                     ,                 ,                     ,                 , 
   R400/900,                  , :math:`\diamond`,                     ,                     ,                 , 
   1.0arcsec, :math:`\diamond`, :math:`\blacksquare`, :math:`\diamond`,                     ,                 , :math:`\blacksquare`
   MOS,                       ,                     ,                 , :math:`\blacksquare`, :math:`\diamond`
   Full,      :math:`\diamond`,                     ,                 , :math:`\blacksquare`, :math:`\diamond`
   CenterSp,                  , :math:`\blacksquare`, :math:`\diamond`,                     ,                 , :math:`\blacksquare`

.. note::

   All of the standard star settings except R400/900 will be reduced because the wavelength range covered by the MOS slits for a given grating and central wavelength setting is much greater than that for a single long-slit configuration. The R400/900 setting is not useful because of second-order light :math:`\gt 910` nm from the very hot standard star SED.

Calibration Reference Files
^^^^^^^^^^^^^^^^^^^^^^^^^^^
The steps below assume that the :term:`MasterCal` reference files have been created for all the spectral settings. 
The required **MasterCals** are: 

* Bias Residuals
* Flat-fields 
* Gradient images (unnormalized flat-fields used to determine the locations of the slitlets on the detector format)
* Wavelength calibrations (CuAr comparison arcs)
* Flux calibration

See :ref:`master-ref` for details. 

Example Reduction Script
^^^^^^^^^^^^^^^^^^^^^^^^
You can perform all of the processing steps for this tutorial by downloading the MOS tutorial CL script. 

* Download: :download:`gmos_mos_proc.cl </irafTools/gmos_mos_proc.cl>` 

Place the script in the work directory. 
Within an IRAF sesssion load the **gemini**, **gemtools**, and **gmos** packages, then execute it in command-mode:

.. code-block:: c

   cd /path/to/work_directory
   cl < gmos_mos_proc.cl

You may find it useful to download the script to follow this tutorial in detail, and use it as the basis for reducing other MOS observations. 
The processing steps for this tutorial are described below, but only a subset of the commands are listed in the interest of brevity. 

.. caution::

   The reduction script includes steps that should be performed interactively for best results, but the interactive options have been turned off in the script in order not to interupt the automated processing flow. 

Create File Lists
^^^^^^^^^^^^^^^^^
The next steps will create lists of calibrations and science files for use in processing. 
The selection is achieved by matching a specific combination of header keyword-value pairs (see :ref:`dr-keywords`). 
Examples of file selection with the `hselect <https://iraf.net/irafhelp.php?val=hselect>`_ task are given below. 
Note the use of ``gemtools.gemextn`` to remove the IRAF FITS kernel syntax from the resulting file names. 

.. code-block:: c

   cd /path/to/work_directory/raw

   ## Bias exposures with a common observation class, RoI, and CCD binning:
   s1 = "obstype?='BIAS' && obsclass?='dayCal' && detrO1ys>1024 && ccdsum?='2 2'"

   # Select bias exposures within ~3 weeks of the target observations.
   s2 = " && @'date-obs' > '2012-01-16' && @'date-obs' < '2012-02-12'"
   string biasSelect
   biasSelect = s1 // s2

   # Select biases using information from both PHDU and HDU-1
   hselect ("N*.fits[1,inherit=yes]", "$I", biasSelect, > "bias_tmp.txt")
   string omit = "index,kernel"
   gemextn ("@bias_tmp.txt", omit=omit, outfile="biasFull.txt")
   # Also select biases for the CenterSpec RoI for the standard star.

   ## Flat-fields 
   # Flats must match the observation type & class, and CCD RoI & CCD,
   # as well as the aperture, grating, and central wavelength. 
   # Start with the GCAL flats for the LS spectra of the standard star:
   string flatSelect
   s1 = "obstype?='FLAT' && obsclass?='partnerCal' && detro1ys==512 && ccdsum?='2 2'"
   s2 = "&& maskname?='1.0arcsec' && grating?='R400+_G5305' && grwlen=740.0"
   flatSelect = s1 // s2 

   hselect ("N*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
   gemextn ("@flt_tmp.txt", omit=omit, outfile="flatCenSp_R4-740.txt")
   # Construct the lists for the remaining modes (see below). 

From these lists the **Bias Residual MasterCals**: ``MCbiasFull.fits`` and ``MCbiasCenSp.fits`` may be built, as well as the following **Flat-field MasterCals** (see script):

.. csv-table:: **Needed Flat-field MasterCals**
   :header: Source, B600, R400
   :widths: 15, 30, 30

   GCAL,     ``MCflatCenSp_B6-420.fits``,    ``MCflatCenSp_R4-620.fits``
   ,         ``MCflatCenSp_B6-520.fits``,    ``MCflatCenSp_R4-740.fits``
   ,         ``MCflatCenSp_B6-620.fits``,    
   ,         ``MCflatFull-M01_B6-520.fits``, ``MCflatFull-M01_R4-740.fits``
   Twilight, ``MCtwiFull-M01_B6-520.fits``,  ``MCtwiFull-M01_R4-740.fits``

Now build the list of arc, standard star, and science exposures. 
This is tedious, but will make the processing much easier to follow. 
Comparison arcs for the M81 fields were taken contemporaneously with the science exposures, so use those. 

.. code-block:: c

   string arcSelect

   # Arcs for Std stars, all configurations
   s1 = "obstype?='ARC' && detro1ys==512 && ccdsum?='2 2' "
   s2 = "&& maskname?='1.0arcsec'"
   arcSelect = s1 // s2
   hselect ("N*.fits[1,inherit=yes]", "$I", arcSelect, > "arc_tmp.txt")
   gemextn ("@arc_tmp.txt", omit=omit, outfile="arcCenSp.txt")

   # Comparison arcs for Mask 01
   # Select via observation type & class, CCD RoI & binning, and mask:
   s1 = "obstype?='ARC' && obsclass?='progCal' && detro1ys>1024 && ccdsum?='2 2' "

   # Select also by mask; grating, and cenWave. 
   s2 = "&& maskname?='GN2011BC003-01' && grating?='B600+_G5307' && grwlen=520.0"
   arcSelect = s1 // s2
   hselect ("N*.fits[1,inherit=yes]", "$I", arcSelect, > "arc_tmp.txt")
   gemextn ("@arc_tmp.txt", omit=omit, outfile="arcFull-M01_B6-520.txt")
   # ...and similarly for arcFull_M01_R4-740.txt

Continue with the science and standard star exposures. 

.. code-block:: c

   string sciSelect

   ## Science exposures all have the same CCD RoI & binning.
   # Select via object name, observation class, mask, grating & cenWave:
   s1 = "i_title?='M81-field1' && obsclass?='science' && maskname?='GN2011BC003-01' "
   s2 = "&& grating?='B600+_G5307' && grwlen=520.0"
   sciSelect = s1 // s2
   hselect ("N*.fits[1,inherit=yes]", "$I", sciSelect, > "sci_tmp.txt")
   gemextn ("@sci_tmp.txt", omit=omit, outfile="m81Files-M01_B6-520.txt")
   # ...and similarly for m81Files_M01_R4-740.txt

   ## Standard stars all have the same CCD RoI & binning.
   # Select via observation type & class, mask, grating & cenWave:
   s1 = "obstype?='OBJECT' && obsclass?='partnerCal' && maskname?='1.0arcsec' "
   s2 = "&& grating?='B600+_G5307' && grwlen=520."
   string stdSelect
   stdSelect = s1 // s2
   hselect ("N*.fits[1,inherit=yes]", "$I", stdSelect, > "std_tmp.txt"
   gemextn ("@std_tmp.txt", omit=omit, outfile="stdFiles_B6-520.txt")
   # ...and similarly for the other settings: B6-420, B6-620, R4-640, etc.

Now move the file lists to the parent directory.
Most other processing lists may be built from the above lists and the IRAF `sections <https://iraf.net/irafhelp.php?val=sections>`_ task, as shown below. 

Basic Processing
----------------
Basic image reductions are performed by the **gsreduce** task, using the **Bias** and **Flat-field MasterCal** files. 
Processing for the MOS exposures also requires a reference image from which the spatial boundaries of the slitlets can be determined; this should be the combined (but unnormalized) flat-field image for each combination of mask and grating. 

The following steps with can be performed on the lists of files constructed above, using the ``@filelist`` notation. 
Note that we will be building the variance and DQ extensions along the way.

.. code-block:: c

   cd /path/to/work_directory

   # Use primarily the default task parameters.
   unlearn gsreduce
   gsreduce.logfile="gsreduceLog.txt"
   gsreduce.rawpath="./raw"

   # Perform basic reductions on long-slit arcs...
   gsreduce ("@arcCenSp.txt", bias="MCbiasCenSp", \
      fl_fixpix-, fl_flat-, fl_oversize-)

   # ...and on std star exposures. 
   # Note the selection of interpolation accross chipgaps.
   gsreduce ("@stdFiles_B6-520.txt", \
      bias="MCbiasCenSp", flatim="MCflatCenSp_B6-520", fl_oversize-)
   # ...and similarly for the other grating/central wavelength configurations.

   # Perform basic reductions on MOS arcs & science targets.
   gsreduce ("@arcFull-M01_B6-520.txt", bias="MCbiasFull", \
      gradimage="MCflatComb-M01_B6-520", fl_fixpix-, fl_flat-, fl_oversize-)

   gsreduce ("@m81Files_M01_B6-520.txt", bias="MCbiasFull", \
      flatim="MCflatFull-M01_B6-520", gradimage="MCflatComb-M01_B6-520", \
      fl_fixpix-, fl_oversize-, fl_vardq+, fl_fulldq+)
   # ...and similarly for the other grating/central wavelength settings. 

Examine the output files to assess data quality, and adjust the processing parameters or the lists of input files as necessary. 

Multi-frame Cosmic Ray Rejection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The targets in this program were observed with two grating/central wavelength configurations, and a few exposures were obtained at each position. 
This provides an opportunity to combine the sequential exposures at each position to remove cosmic rays, rather than rejecting CRs on single frames using the ``gsreduce.fl_gscrrej+`` flag or running the **gemcrspec** task. The result will be the average of the exposures, with outlier rejection. 

.. code-block:: c

   # Create a list of common slit alignments for the target.
   sections gs//@m81Files-M01_B6-520.txt > gsM81Files-M01_B6-520.txt
   sections gs//@m81Files-M01_R4-740.txt > gsM81Files-M01_R4-740.txt

   # Use primarily the default task parameters to combine.
   unlearn gemcombine
   gemcombine.logfile="gemcombineLog.txt"
   gemcombine.reject="ccdclip"

   # Combine the exposures with outlier rejection for each grating.
   gemcombine ("@gsM81Files-M01_B6-520.txt", "M81-field1_B6-520", \
      fl_vardq+, fl_dqprop+)
   gemcombine ("@gsM81Files-M01_R4-740.txt", "M81-field1_R4-740", \
      fl_vardq+, fl_dqprop+)

Note the need above to explicitly propagate the DQ and VAR extensions from the input files. 

Wavelength Calibration
^^^^^^^^^^^^^^^^^^^^^^
Image rectification and wavelength linearization are performed next, using the wavelength calibrated arc lamp exposures taken immediately before each sequence of science and standard star exposures (see :ref:`wave-cal`). 
Use the default Chebyshev polynomial, but with ``order=5`` for longslit, and ``order=7`` for MOS Arcs. 
Then apply the calibration to the standard stars and science exposures. 

.. code-block:: c

   # Apply wavelength calibration.
   unlearn gstransform
   gstransform.logfile="gstransformLog.txt"

   # Long-slit mode
   gstransform ("gsN20120124S0117", outimages="tG191B2B_B6-520", \
      wavtraname="gsN20120124S0246")
   gstransform ("gsN20120124S0226", outimages="tHZ44_B6-520", \
      wavtraname="gsN20120124S0246")

   # MOS mode
   gstransform ("M81-field1_B6-520", outimages="tM81-field1_B6-520", \
      wavtraname="gsN20120124S0169", fl_vardq+)

   # ...and similarly for the other grating/central wavelength settings. 

Standard Star Processing
------------------------
Sky Subtraction
^^^^^^^^^^^^^^^
The **gsskysub** task will determine the night sky emission spectrum from a selected spatial region, and subtract it row-by-row from the spectral image. 
Sky subtraction for MOS spectra is usually performed on each slitlet prior to spectral extraction. 
However **gsskysub** is not well matched to the nature of these sources: extended H_II regions that are not the same size nor identically centered in the slitlets.
So for the MOS exposures, sky subtraction will be performed interactively during the spectral extraction step. 

Sky subtraction for the longslit spectra of the standard stars could similarly be deferred to extraction. 
But the selection of a sky region from a spatial profile of the standard star spectrograms can be determined easily by inspection, e.g., by using the `pcols <https://iraf.net/irafhelp.php?val=pcols>`_ task.

.. code-block:: c

   pcols ("tG191B2B_B6-520.fits[SCI]", 1030, 2040, wy1=100, wy2=500, \
           wx1=80, wx2=420)

Select your preferred sky regions and perform the sky subtraction, propagating the variance and data quality extensions.

.. code-block:: c

   # Subtract sky spectrum using selected regions.
   unlearn gsskysub
   gsskysub.logfile="gsskysubLog.txt"

   gsskysub ("tG191B2B_B6-520", fl_oversize-, fl_vardq+, \
      long_sample="95:195,320:420")
   # ...and similarly for the other standard star settings. 

Sky subtraction for MOS spectra may be performed, provided the size of the source is known (and the same) for all slitlets. 

Longslit Extraction
^^^^^^^^^^^^^^^^^^^
Extract the 1-D spectra of the standard stars from the 2-D spectrograms, using **gsextract** with a 3-arcsec aperture:

.. code-block:: c

   # Extract the standard star spectrum
   unlearn gsextract
   gsextract.logfile="gsextractLog.txt"

   gsextract ("stG191B2B_B6-520", fl_inter+, \
      apwidth=3., tfunction="spline3", torder=9)
   gsextract ("stG191B2B_B6-420", fl_inter+, refimages="stG191B2B_B6-520", \
      apwidth=3., tfunction="spline3", torder=9)

   gsextract ("stG191B2B_R4-740", fl_inter+, \
      apwidth=3., tfunction="spline3", torder=11)
   # ...and similarly for the other standard stars and settings. 

Note that the ``refimages`` parameter assures that same trace will be used for subsequent central wavelength settings. 
Now derive the sensitivity calibration, as described in :ref:`flux-cal`. 

Advanced Processing
-------------------
Spectrum Extraction
^^^^^^^^^^^^^^^^^^^
The penultimate step is to extract the MOS spectra of M81. 
We will define extraction and sky apertures interactively, following a path similar to that described by [SM14]_. 
The spectrograms of these targets represent perhaps the worst-case senario for spectral extraction using **gsextract** (which is a wrapper around the `apextract.apall task <https://iraf.net/irafhelp.php?val=apall>`_, in that they contain

* weak emission 
* no continuum 
* widely spaced emission lines 
* small or non-existant regions for sky background (owing to the small spatial extent of the slitlets)

This makes it impractical to identify extraction regions automatically. 
Instead, identify the location of the brightest emission features in advance using an image display server. 
The figure below shows a display of all the image extensions of ``tM81-field1_R4-740.fits`` in **SAOImage/DS9**. 

.. figure:: /_static/M81_R4_tile.*
   :width: 90 %

   The 27 slitlet spectra with grating R400/740 of M81 H_II regions, displayed simultaneously in SAOImage/DS9 (columns 1, 4, and 7). Note that the VAR and DQ frames are also displayed (columns 2-3, 5-6, 8-9), though they are not relevant for this exercise. Click image to enlarge. 

To display the brightest emission regions efficiently from within DS9, 

* Select **File** :math:`\rightarrow` **Open as** :math:`\rightarrow` **Multiple Extension Frames...** and load the file
* Bring up the **Zoom** :math:`\rightarrow` **Pan Zoom Rotate Parameters...** menu and enter the wavelength of an emision line (:math:`\mathrm{H}\alpha` 6563 in this case) in the *Pan* dialog box
* Select **Frame** :math:`\rightarrow` **Match** :math:`\rightarrow`  **Frame** :math:`\rightarrow`  **WCS**

For each SCI image in turn, use the cursor to determine the image **line** (i.e., *x*-coordinate) of the feature of interest and the spatial extent (*y*-coordinate) of the desired extraction window, and record these parameters for use during the spectral extraction. 

Now extract the spectra from the slitlets interactively. 
If you are not already comfortable using IRAF's **apextract** package and the associated cursor commands, see :ref:`apextract-summary`.
Note that with such weak continuum emission, it is best to use a very low-order spectrum trace function. 

.. code-block:: c

   # Extract the standard star spectrum
   unlearn gsextract
   gsextract.logfile="gsextractLog.txt"

   gsextract ("tM81-field1_B6-520", fl_inter+, apwidth=2.5, tfunction="spline3", \
      torder=1, tnsum=50, background="median", mos_bsample=1.0)
   gsextract ("tM81-field1_R4-740", fl_inter+, apwidth=2.5, tfunction="spline3", \
      torder=1, tnsum=50, background="median", mos_bsample=1.0)

Flux Calibration
^^^^^^^^^^^^^^^^
Flux calibration is a necessary final step for this program's science goals. 
The standard stars G191B2B and HZ44 were observed with each grating and a variety of central wavelengths as a part of this program. 
The investigators for this program in essence derived one sensitivity function for each grating, even though the grating efficiency functions for different central wavelength do not match perfectly. 
See :ref:`flux-cal` for deriving the sensitivity function. 

Calibrate the target spectra by applying the atmospheric extinction correction (download: :download:`mk_extinct.txt <../calib/mk_extinct.txt>`) and the flux calibration.

.. code-block:: c

   # Apply the sensitivity function.
   unlearn gscalibrate
   gscalibrate.logfile="gscalibrateLog.txt"
   gscalibrate.extinction="./mk_extinct.dat"

   gscalibrate ("estM81-field1_B6-520", sfunc="sens_B6", fl_ext+, fl_scale-)
   gscalibrate ("estM81-field1_R4-740", sfunc="sens_R4", fl_ext+, fl_scale-)

A calibrated spectrum of one of the brighter MOS targets is shown below. 

.. figure:: /_static/M81_targ107.*
   :width: 90 %

   H_II region target 107 in M81 with grating B600/520 in M81. The Balmer recombination lines are visible, as are prominent lines of [O_III], [N_II], and [S_II]. Click image to enlarge. 
