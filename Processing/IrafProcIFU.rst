.. _iraf-proc-ifu:

==========================================
Reduction of IFU Spectral Images with IRAF
==========================================
This tutorial will reduce observations from programs GS-2012B-Q-26 and GN-2012B-Q-226 (PI: M. Schirmer), :term:`IFU` spectroscopy of the "green bean" galaxy, J224024.1--092748. 
The observations were obtained on the nights of 2012 Oct 18-19 (GMOS-N) and 2012 Aug 27+29 (GMOS-S), and included *g*-band direct (acquisition) imaging of the field of interest, IFU 2-slit spectroscopy, and supporting calibration exposures. 
The spectra were obtained with the following configurations:

.. csv-table:: **Configurations and Spectral Coverage**
   :header: Configuration, Passband, Rest Passband
   :widths: 20, 15, 15

   B600/499 + *g*,       429--542 nm, 324--409 nm
   B600/625 + *r*,       559--688 nm, 422--519 nm
   R831/853 + *z* + CaT, 847--905 nm, 639--683 nm

The original science goal was to analyze the 3-D kinematics, ionization state, and physical parameters of the gas in this Type-2 Seyfert galaxy at :math:`z=0.326` (Davies, et al. 2015: [DST]_). 

IFU spectra are especially complex to reduce, and demanding science goals require careful attention to detail. 
For an advanced discussion and a detailed recipe for deriving high quality science products from this program's data, see the posting by James Turner on the `Gemini Data Reduction User Forum <http://drforum.gemini.edu/topic/gmos-ifu-data-reduction-example-2/>`_ (download the reduction script: `allred.cl <http://drforum.gemini.edu/wp-content/uploads/2015/04/allred.cl>`_) . 
For a high-level description of IFU reductions, see :ref:`ifu-workflow`. 
Here is an outline for reduction of this program's IFU data: 

.. contents:: 
   :depth: 3

For other tutorials, see the following links:

* :ref:`iraf-proc-img` 
* :ref:`iraf-proc-ls`
* :ref:`iraf-proc-mos`

Retrieve & Organize Data
------------------------
The first step is to retrieve the data from the Gemini Observatory Archive (see :ref:`archive-search`). 
You may search the `GOA <https://archive.gemini.edu/searchform>`_ yourself, or instead just cut-and-paste the direct URLs in your browser. 

.. code-block:: html

   # IFU data of Green Bean galaxy:
   https://archive.gemini.edu/searchform/cols=CTOWEQ/GS-2012B-Q-26/notengineering/GMOS/NotFail
   https://archive.gemini.edu/searchform/cols=CTOWEQ/GN-2012B-Q-226/notengineering/GMOS/NotFail

After retrieving the science data, click the **Load Associated Calibrations** tab on the search results page and download the associated bias and flat-field exposures. 
See :ref:`retrieve-data` for details. 
Unpack all of them in a subdirectory of your working directory named ``/raw``. 
Be sure to uncompress the files. 

It is highly recommended to create an observing log (see :ref:`gen-obslog`), and review the exposures to understand the observing program and the nature of the science exposures. 
A review of the log for this program reveals the following:

* :term:`GCAL` flat exposures were obtained contemporaneously with the science and standard star exposures, which will enable an accurate tracing for the fiber locations on the detector format. 

* The Arc exposures were obtained as :term:`dayCal` observations, well separated in time from the science exposures and other Arcs. Thus, it is only useful to process one Arc for each configuration, since they cannot be combined and instrument flexure will introduce zero-point errors. A refinement to the wavelength calibration for science exposures will be necessary. 

* Exposures of standard stars were obtained, but not on the same nights as the science exposures, and no single star was observed in all configurations. 

* Multiple science exposures were obtained in each configuration, all with long durations. The emission features are weak, the cosmic rays are plentiful, and (not surprisingly) the spectral images will have to be aligned post-extraction, prior to combining. 

* Some twilight flat dayCal exposures were obtained, but are not needed or used for this IFU program.

.. csv-table:: **Science & Standard Star Observations**
   :header: DateObs, Configuration, Target, Used?
   :widths: 15, 20, 20, 10

   **2012-08-27**, **GS-B600/625**, **J2240-0927**, **Yes**
   2012-08-29, GS-B600/499, EG131,      No
   ,           **GS-B600/625**, **EG131**, **Yes**
   2012-10-05, GN-B600/499, BD+28_4211, Yes
   2012-10-08, GN-B600/499, J2240-0927, Yes
   2012-10-18, GN-R831/853, J2240-0927, Yes
   **2012-10-31**, **GS-B600/625**, **LTT9239**, **Yes**
   2013-04-03, GS-R831/853, Wolf1346,   No

Reductions for the observations highlighted in **bold**, above, will be described in the following sections. 

.. _ifu-proc-prep:

Processing Preparation
----------------------
Software
^^^^^^^^
Example Reduction Script
::::::::::::::::::::::::
You can perform all of the processing steps for this tutorial by downloading the IFU Tutorial CL script. 

* Download: :download:`gmos_ifu_proc.cl </irafTools/gmos_ifu_proc.cl>` 

Place the script in the work directory. 
Within an IRAF session load the **rv**, **gemini**, **gemtools**, and **gmos** packages, then execute it in command-mode.
It will take awhile to run (the better part of a day on a 2010-era Mac Pro).

.. code-block:: c

   #...then:
   cd /path/to/work_directory
   cl < gmos_ifu_proc.cl

You may find it more useful to download the script just to follow this tutorial in detail, and use it as the basis for reducing other IFU observations. 
The processing steps for this tutorial are described below, but only a subset of the commands for G600/625 are given in the interest of brevity. 

Suppressing Cosmic Rays
:::::::::::::::::::::::
To suppress the cosmic rays effectively, particularly for the long-duration science exposures, install the **L.A.Cosmic** script (see [VD]_) 

* Download: download: :download:`lacos_spec.cl </irafTools/lacos_spec.cl>` 

if you have not already done so. 
You may define this task in your ``login.cl`` file to avoid this step each time you start IRAF. 
See: setting up the :ref:`la-cosmic`. 
This task, which is called by **gemtools.gemcrspec**, works *much* better than the **gmos.gscrrej** task, which is called when the ``fl_gscrrej`` is set in **gfreduce** processing. 

.. _ifu-reffile-checklist:

Reference File Checklist
^^^^^^^^^^^^^^^^^^^^^^^^
The Table below lists the basic calibration files that will be constructed for the science data processing steps, apart from the wavelength solutions (which are written to the ``/database`` subdirectory). 
Files that include an asterisk (``*``) wildcard character refer to any names with the specified suffix. 

.. csv-table:: **Required MasterCals**
   :header: Type, GMOS-N, GMOS-S
   :widths: 20, 30, 40

   Static BPM,    [N/A],               ``bpm_gmos-s_EEV_1x1_3amp_full``
   Bias Residual, ``MCbiasN``,         ``MCbiasS1`` and ``MCbiasS2``
   Flat-field,    ``eprgN20121018S0070_flat``, ``eprgS20120829S0062_flat``
   ,              ``eprgN20121018S0073_flat``, ``eprgS20120827S0069_flat``
   ,              ``eprgN20121005S0296_flat``, ``eprgS20121031S0025_flat``
   Sensitivity,   ``Sens_B6-499``,       ``Sens_B6-625``
   ,              ``Sens_R8-853``, 

Fetch and uncompress the **Static BPM MasterCal** 

* Download: :download:`bpm_gmos-s_EEV_v2_1x1_spec_MEF.fits.bz2 <../calib/bpm_gmos-s_EEV_v2_1x1_spec_MEF.fits.bz2>` 

for full-frame read-outs for the GMOS-S EEV CCDs. 
The GMOS-N CCDs have very few cosmetic defects, so a **Static BPM MasterCal** is not necessary. 

You will need to make local copies (in your work directory) of the monochromatic magnitudes for the standard stars (see: :ref:`stdstar-list`). 
These standards will be used to make a common sensitivity function for GMOS-S B600/625, but the reference files reside in different directories in the IRAF system. 

* EG131: ``gmos$calib/eg13.dat``
* LTT9238: ``onedstds$ctionewcal/l9239.dat``

Now take a deep breath; this is going to be a bit of a slog. 

Create File Lists
^^^^^^^^^^^^^^^^^
The next steps will create lists of calibrations and science files for use in processing. 
This is tedious, but will make the processing much easier to follow. 
The selection is achieved by matching a specific combination of header keyword-value pairs (see :ref:`dr-keywords`). 
Examples of file selection with the `hselect <https://iraf.net/irafhelp.php?val=hselect>`_ task are given below. 
Note the use of ``gemtools.gemextn`` to remove the :term:`FITS` kernel syntax from the resulting file names. 

Build the lists of calibration files.

.. code-block:: c

   cd /path/to/work_directory/raw

   string obsSelect
   string ccdSelect

   # All bias & flat files match a single CCD RoI and binning.
   ccdSelect = "&& detrO1ys>1024 && ccdsum?='1 1' "

   ## Biases must match the instrument, obs. type & class, and CCD RoI & binning:
   obsSelect = "instrume?='GMOS-S' && obstype?='BIAS' && obsclass?='dayCal' "

   # Select bias exposures within ~10 days of the target observations.
   s2 = " && @'date-obs' > '2012-08-20' && @'date-obs' < '2012-09-01'"
   s1 = obsSelect // ccdSelect // s2 

   # Select biases using info. from both PHDU & HDU-1.
   hselect ("S*.fits[1,inherit=yes]", "$I", s1, > "bias_tmp.txt")

   # Trim IRAF kernel notation.
   string omit = "exten,index,kernel"
   gemextn ("@bias_tmp.txt", omit=omit, outfile="biasFilesS1.txt")
   # ...and similarly for the Oct. GMOS-S and the GMOS-N biases.

   ## Flats must also match the aperture, grating, and central wavelength. 
   string bandSelect

   # Start with the GCAL flats for B600/625:
   obsSelect = "instrume?='GMOS-S' && obstype?='FLAT' "
   bandSelect = "&& maskname?='IFU-2' && grating?='B600+_G5323' && grwlen=625."
   s1 = obsSelect // ccdSelect // bandSelect 
   hselect ("S*.fits[1,inherit=yes]", "$I", s1, > "flt_tmp.txt")
   gemextn ("@flt_tmp.txt", omit=omit, outfile="fltGcal_B6-625.txt")
   #...and similarly for the GMOS-N settings: R831/83 and B600/499

Continue with building the list of arc, standard star, and science exposures. 
Ultimately the standard star selection criteria below match only one file per configuration, but the exercise is more generally applicable. 
Note the ``S`` prefix in selecting filenames below.
This is not strictly necessary since ``INSTRUMENT`` is constrained to be ``GMOS-S``, but it does reduce by about half the number of file headers to be opened. 

.. code-block:: c

   ## Arcs
   # Only one arc is useful for each configuration, 
   # so choose filenames from the log.
   print ("N20121005S0902.fits", > "arc_B6-499.txt")
   print ("S20120828S0005.fits", > "arc_B6-625.txt")
   print ("N20121018S0197.fits", > "arc_R8-853.txt")

   bandSelect = "&& maskname?='IFU-2' && grating?='B600+_G5323' && grwlen=625."

   ## Science exposures
   # Match the instrument, object name, observation class, mask, grating & cenWave:
   obsSelect = "instrume?='GMOS-S' && i_title?='J2240-0927' && obsclass?='science' "
   s1 = obsSelect // bandSelect
   hselect ("S*.fits[1,inherit=yes]", "$I", s1, > "sci_tmp.txt")
   gemextn ("@sci_tmp.txt", omit=omit, outfile="sci_B6-625.txt")

   ## Standard stars (two epochs)
   # EG131:
   obsSelect = "instrume?='GMOS-S' && obstype?='OBJECT' && i_title?='EG131' "
   s1 = obsSelect // bandSelect
   hselect ("S*.fits[1,inherit=yes]", "$I", s1, > "std_tmp.txt"
   gemextn ("@std_tmp.txt", omit=omit, outfile="EG131_B6-625.txt")

   # LTT9239:
   obsSelect = "instrume?='GMOS-S' && obstype?='OBJECT' && i_title?='LTT9239' "
   s1 = obsSelect // bandSelect
   hselect ("S*.fits[1,inherit=yes]", "$I", s1, > "std_tmp.txt"
   gemextn ("@std_tmp.txt", omit=omit, outfile="LTT9239_B6-625.txt")
   # ...and similarly for the other settings: B6-499 and R8-853

Move the file lists to the parent work directory.
Most other processing lists may be built from the above lists and the IRAF `sections <https://iraf.net/irafhelp.php?val=sections>`_ task, as shown in the following sections. 

.. _ifu-calib-proc: 

Calibration Processing
----------------------
Generating calibration reference files beyond the **Bias Residual MasterCal** is sufficiently involved that the topic will be covered here. 
The following section describes the preparation of calibration reference files necessary for the removal of the instrumental signature. 

Processing with gfreduce
^^^^^^^^^^^^^^^^^^^^^^^^
Many reduction steps are performed by the **gfreduce** task. 
This task has more than 75 parameters; the table below lists the defaults for the "flag" keywords---i.e., keywords with logical values to indicate whether to include a specific step in the processing. 

.. csv-table:: **gfreduce Processing Flag defaults**
   :header: "Flag", "Default", "Description"
   :widths: 12, 8, 50

   ``fl_addmdf``,    Yes, Append :term:`MDF` extension?
   ``fl_bias``,      Yes, Subtract Bias Residual?
   ``fl_fixgaps``,    No, Interpolate over chip gaps after extraction?
   ``fl_fixnc``,      No, Auto-correct for nod count-mismatch in N&S observations
   ``fl_fulldq``,     No, Decompose DQ into constituent bits before transforming them?
   ``fl_extract``,   Yes, Extract the spectra?
   ``fl_fluxcal``,   Yes, Apply flux calibration?
   ``fl_nodsuffle``,  No, Using one of the :term:`N&S` slitmasks?
   ``fl_gsappwave``, Yes, Insert approximate dispersion solution into header?
   ``fl_gnsskysub``,  No, Subtract sky from N&S images?
   ``fl_gscrrej``,   Yes, Clean images of cosmic rays?
   ``fl_inter``,     Yes, Perform operations interactively? 
   ``fl_novlap``,    Yes, Extract only non-overlapping sections of 2-slit spectra?
   ``fl_over``,      Yes, Perform overscan correction?
   ``fl_skysub``,    Yes, Subtract mean sky spectrum?
   ``fl_trim``,      Yes, Trim overscan region?
   ``fl_vardq``,      No, Propagate VAR and DQ?
   ``fl_wavtran``,   Yes, Apply wavelength calibration?

Processing IFU data is more complicated than for other instrument configurations, for a variety of reasons including: 

* The **gfreduce** task does not fully integrate some options and operations (e.g., an input parameter for the **Static BPM**), and frankly, it is a little cantankerous. 

* The narrow spatial extent of the IFU fibers, and the close packing of the output fiber spectra on the :term:`FPA` means that effects such as instrument flexure and cosmic-ray hits have an outsized impact on the data processing, and must be addressed with care. 

* Some operations such as flat-field generation have not been broken out into a separate task, and other tasks do not fully support file lists. 

The workflow for IFU processing is, therefore, somewhat iterative and requires using the IRAF **cl** to iterate over file lists. 

Spectrum Trace
^^^^^^^^^^^^^^
Ideally GCAL flats were obtained contemporaneously with the science exposures so that they may be used as templates for tracing the spectrum from each fiber on the science image array; that is the case for this program. 
A representative shift in location of the fiber traces for two different flat-field exposures is shown in the figure below, which illustrates the need for a contemporaneous GCAL flat with science exposures. 

.. figure:: /_static/IFU_fiberSpec.*
   :width: 80 %

   A shift of about 1 pixel in the horizontal position of fiber spectra in two GCAL flat-fields, taken weeks apart. Each fiber spectrum subtends only about 6 spatial pixels, corresponding to 0.2 arcsec in the focal plane. Wavelength increases from right to left. 

The first reduction step is to process those GCAL exposures though bias correction and extraction, so that the extraction parameters will be written to the ``/database`` directory for downstream use. 
Recall that there are two epochs for the B600/625 configuration. 

.. code-block:: c

   unlearn gfreduce
   unlearn gireduce
   gfreduce.logfile="gfreduceLog.txt"

   # Note the trailing slash, which is for some reason required for gfreduce:
   gfreduce.rawpath="./raw/"

   # Extract GCAL flat-fields (two epochs for GMOS-S)
   gfreduce ("@flatGcal_B6-625-1.txt", bias="MCbiasS1", fl_extract+, \
      fl_gscrrej-, fl_gsappwave-, fl_wavtran-, fl_skysub-, fl_fluxcal-, \
      fl_vardq+, fl_inter-)

   gfreduce ("@flatGcal_B6-625-2.txt", bias="MCbiasS2", fl_extract+, \
      fl_gscrrej-, fl_gsappwave-, fl_wavtran-, fl_skysub-, fl_fluxcal-, \
      fl_vardq+, fl_inter-)
   # ...and so on for the other configurations.

Recall also that the intermediate files from each major step in the data reduction process are tagged with a prefix: see :ref:`file-nomenclature` for a list. 
The ``erg*`` extracted flat-field files generated above are used for two purposes: the fiber extraction parameters, and as input to the flat-field normalization. 

The **gfreduce** task does not have an option for inserting a **Static BPM MasterCal** into the DQ extensions of the processed flats, so this must be done explicitly. 
Alas, the applicable task, **addbpm**, appears not to work properly.
So, insert the DQ extensions in-place, then interpolate over the bad columns.

.. code-block:: c

   string bpm = "bpm_gmos-s_EEV_1x1_3amp_full.fits"
   string extn = "[DQ,"
   # Build a list of files with the processing prefixes attached.
   sections ("rg//@flatGcal_B6-625.txt", option="root", >"rgFlat_B6-625.txt")

   # Use an IRAF list-structure to loop over the file names.
   list = "rgFlat_B6-625.txt"
   while (fscan (list, s1) != EOF) {
      for (i=1; i<=3; i+=1) {
         s2=bpm//extn//i//"]"
         imcopy (s2, s1//extn//i//",overwrite+]")
      }
   }

The flat-field exposures are of such short duration that cosmic-rays (CRs) are not a concern. 
Now interpolate over bad columns in the science array, so as not to corrupt down-stream extractions. 

.. code-block:: c

   unlearn gemfix
   gemfix.logfile="gemfixLog.txt"
   gemfix ("@rgFlat_B6-625.txt", "p@rgFlat_B6-625.txt", \
      method="fit1d", bitmask=1, order=32)

.. Caution::

   It would be best at this point to model and subtract scattered light from the flat-fields (or any well exposed source). Unfortunately the **gfscatsub** task is unreliable, sometimes producing wildly incorrect results, so that the scattered light correction will be skipped in this tutorial. 

   For some detectors on GMOS-S (the EEV and the Hamamatsu CCDs) the relative QE correction between sensors should be applied. This step is skipped in this tutorial, but if implemented then the correction must be applied to *both* the flats *and* the astrophysical targets. 

   If the **gfscatsub** task is fixed in some future release, and the QE correction is performed (in v1.13.1+), bear in mind that these steps prepend a ``b`` and a ``q``, respectively, to the output filenames. Therefore the filename lists used below would all need to be updated. 

Flat Normalization
^^^^^^^^^^^^^^^^^^
Now extract the flats from the exposures, and normalize by fitting a 1-D curve in the dispersion direction to remove the color term. 
Note that **gfresponse** must be called in a loop to process a list of exposures, and that a very high order is required to fit the variations in the spectral response. 

.. code-block:: c

   # Fit the response to the extracted flat-field fibers.
   unlearn gfresponse
   gfresponse.logfile="gfresponseLog.txt"

   sections ("ep//@rgFlat_B6-625.txt", option="root", >"eprgFlats_B6-625.txt")
   list = "eprgFlats_B6-625.txt"
   while (fscan (list, s1) != EOF) {
       gfresponse (s1, s1//'_flat', skyimage='', function='spline3', 
           order=47, sample="1:1,30:2855,2879:2879")
   }
   # ...and so on for the other configurations.

Arcs
^^^^
The wavelength calibration can be performed once the order trace has been defined, so the arc exposures will be processed next. 
The traces (specified with the ``reference`` parameter) do not match the :term:`dayCal` arcs perfectly, but they are adequate for wavelength calibration. 
The following basic reduction performs overscan correction and trimming, but does not apply the **Bias Residual MasterCal** because dayCal Arc exposures are normally obtained in fast read-out mode. 

.. code-block:: c

   gfreduce ("@arc_B6-625.txt", reference="eprgS20120829S0062.fits", \
      fl_bias-, fl_extract+, trace-, recen-, fl_gsappwave+, fl_wavtran-, \
      fl_gscrrej-, fl_skysub-, fl_fluxcal-, fl_vardq-, fl_inter-)

The default CuAr line list (``gmos$calib/CuAr_GMOS.dat``) has been pretty heavily groomed to produce good fits at low S/N, and with 1500 fibers one either has to trust the default or perform only the first few wavelength solutions interactively. 

.. code-block:: c

   unlearn gswavelength
   gswavelength.logfile="gswaveLog.txt"
   gswavelength ("erg@arc_B6-625.txt", fl_inter-, fwidth=8, minsep=2.5)


Basic Processing
----------------
With the internal calibrations in place, proceed with processing the science and standard star exposures through Bias correction. 
Start by assembling the list of all exposures, but segregate them by date so that the appropriate **Bias Mastercal** may be used. 

.. code-block:: c

   concatenate ("EG131_B6-625.txt,s*_B6-625.txt", "allSfiles.txt")

   gfreduce ("@allSfiles.txt", bias="MCbiasS1", reference="eprgS20120829S0062", \
      fl_extract-, fl_gscrrej-, fl_gsappwave-, fl_wavtran-, fl_skysub-, \
      fl_fluxcal-, fl_vardq+, fl_inter-)

   gfreduce ("@LTT9239_B6-625.txt", bias="MCbiasS2", reference="eprgS20121031S0025.fits", \
      fl_extract-, fl_gscrrej-, fl_gsappwave-, fl_wavtran-, fl_skysub-, \
      fl_fluxcal-, fl_vardq+, fl_inter-)

   # Insert the MasterCal BPM into the [DQ] extensions.
   concatenate ("allSfiles.txt,LTT9239_B6-625.txt", "allFiles.txt")
   sections ("rg//@allFiles.txt", option="root", >"rgAllfiles.txt")
   list = "rgStd_B6-625.txt"
   while (fscan (list, s1) != EOF) {
      for (i=1; i<=3; i+=1) {
         s2=bpm//extn//i//"]"
         imcopy (s2, s1//extn//i//",overwrite+]")
      }
   }

Reject Artifacts
^^^^^^^^^^^^^^^^
Cleaning the exposures of cosmic rays is important for the standard stars, but *essential* for the science spectrograms. 
Perhaps 4 or 5 iterations will be necessary, at least for the science exposures. 
Note that the **gemcrspec** task requires that the ``lacos_spec.cl`` task be installed (see :ref:`la-cosmic`). 
This task will run for quite awhile, perhaps overnight on a desktop machine....

.. code-block:: c

   unlearn gemcrspec
   gemcrspec.logfile="gemcrspecLog.txt"
   gemcrspec ("@rgAllfiles.txt", "x@rgAllfiles.txt", sigfrac=0.32, \
      niter=4, fl_vardq+)

   # Replace bad pixels (CRs & bad collumns) with interpolated values. 
   unlearn gemfix
   gemfix.logfile="gemfixLog.txt"
   gemfix ("x@rgAllfiles.txt", "px@rgAllfiles.txt", method="fixpix")

Note that **lacosmic** does not completely clean the CRs, although they are marked in the BPM, and it does not deal well with bad columns. 
Thus the final interpolation above over bad pixels with **gemfix** is necessary, and vital for downstream processing. 

.. Caution::

   Again, this is the place where the scattered light and QE corrections would appear in the processing workflow. 

Standard Star Processing
------------------------
Resume basic processing of the Standard Stars with **gfreduce** to apply the flat-field and wavelength calibrations, then sky subtraction. 
The wavelength transformation parameters below will select image columns in the dispersion direction that are in common to all the fibers in the undistorted frame. 
(The specific parameters were adopted from inspecting a prior reduction run.)
Recall that there are different trace references for the standard stars, so they are processed separately.

.. code-block:: c

   gfreduce.rawpath="./"
   sections ("pxrg@EG131_B6-625.txt", >"cleanEG131_B6-625.txt")
   sections ("pxrg@LTT9239_B6-625.txt", >"cleanLTT9239_B6-625.txt")

   gfreduce ("@cleanEG131_B6-625.txt", fl_addmdf-, fl_over-, fl_trim-, \
      fl_bias-, fl_extract+, reference="eprgS20120829S0062", trace-, recenter-, \
      response="eprgS20120829S0062_flat", \
      fl_gsappwave+, fl_wavtran+, wavtraname="ergS20120828S0005", \
      w1=5618., w2=INDEF, dw=0.4622, nw=2822, \
      fl_skysub+, sepslits+, fl_fluxcal-, fl_gscrrej-, fl_vardq+, fl_inter-)

   gfreduce ("@cleanLTT9239_B6-625.txt", fl_addmdf-, fl_over-, fl_trim-, \
      fl_bias-, fl_extract+, reference="eprgS20121031S0025", trace-, recenter-, \
      response="eprgS20121031S0025_flat", \
      fl_gsappwave+, fl_wavtran+, wavtraname="ergS20120828S0005", \
      w1=5618., w2=INDEF, dw=0.4622, nw=2822, \
      fl_skysub+, sepslits+, fl_fluxcal-, fl_gscrrej-, fl_vardq+, fl_inter-)

Aperture Sum
^^^^^^^^^^^^
All the fibers from the *object* field are combined with **gfapsum** into a 1-D spectrum, using the MDF extension (which was inserted into each file) to select the fibers. 

.. code-block:: c

   unlearn gfapsum
   gfapsum.logfile="gfapsumLog.txt"
   sections ("ste@cleanEG131_B6-625.txt", option="root", >"steEG131_B6-625.txt")
   list = "steEG131_B6-625.txt"
   while (fscan (list, s1) != EOF) {
      gfapsum (s1, lthreshold=0., fl_inter-)
   }
   # Make a more informative file name for EG131.
   copy astepxrgS20120829S0061.fits EG131sum_B6-625.fits

   sections ("ste@cleanLTT9239_B6-625.txt", option="root", >"steLTT9239_B6-625.txt")
   list = "steLTT9239_B6-625.txt"
   while (fscan (list, s1) != EOF) {
      gfapsum (s1, lthreshold=0., fl_inter-)
   }

The three sequential exposures for LTT9239 can be combined. 

.. code-block:: c

   unlearn gemcombine
   gemcombine.logfile="gemcombineLog.txt"
   sections ("a@steLTT9239_B6-625.txt", option="root", >"asteLTT9239_B6-625.txt")
   gemcombine ("@asteLTT9239_B6-625.txt", "LTT9239sum_B6-625", \
      reject="none", scale="exposure")

Sensitivity Function
^^^^^^^^^^^^^^^^^^^^
The next step is to derive the sensitivity function from the standard star spectra. 
To use the Mauna Kea extinction function for GMOS-N data, download: :download:`mk_extinct.txt <../calib/mk_extinct.txt>` and place it in your work directory. 
If you have not already done so, make a local copy of the monochromatic magnitudes reference files for these standards (see :ref:`ifu-reffile-checklist`). 

.. code-block:: bash

   copy onedstds$ctionewcal/l9239.dat ./
   copy gmos$calib/eg131.dat ./

Now create the sensitivity function **MasterCal**, setting ``caldir`` to the current directory. 

.. code-block:: c

   unlearn gsstandard
   gsstandard.logfile="gsstandardLog.txt"
   gsstandard.observatory="Gemini-South"

   gsstandard ("EG131sum_B6-625.fits,LTT9239sum_B6-625.fits", "std_B6-625", \
      "Sens_B6-625", starname="eg131,l9239", caldir="./", \
      extinction="onedstds$ctioextinct.dat", order=11, fl_inter+)

The sensitivity curves for the separate standards agree very well, once a grey shift is applied. 

.. figure:: /_static/IFU_B6-625_sens.*
   :width: 90 %

   Fit to the sensitivity function with B600/625 after greyshift of the two contributing standard stars EG131 and LTT9239. One artificial point (*blue*) was added beyond the end of the reddest passband to avoid an otherwise unphysical decline in the derived sensitivity. Click image to enlarge.  

Science Processing
------------------
Almost done. 
With all the calibrations in place, resume processing with **gfreduce** to extract the spectra, apply flat-field and wavelength calibrations, subtract sky, then apply extinction correction and flux calibration. 

Calibration
^^^^^^^^^^^
Since GCAL flats were taken contemporaneously with the science exposures, the flat obtained closest in time will be used for the trace reference. 
Note that the wavelength range has been trimmed slightly (using the ``w1, w2, dw``, and ``nw`` parameters), in order to exclude ends of the spectrum that are not covered in some fibers once the distortion correction has been applied. 
This truncation is not strictly necessary, but can be convenient for display and downstream processing. 

.. code-block:: c

   gfreduce.rawpath="./"
   sections ("pxrg@sci_B6-625.txt", >"cleanSci_B6-625.txt")
   
   gfreduce ("@cleanSci_B6-625.txt", fl_addmdf-, fl_over-, fl_trim-, fl_bias-, 
      fl_extract+, reference="eprgS20120827S0069", trace-, recenter-, \
      response="eprgS20120827S0069_flat", \
      fl_gsappwave+, fl_wavtran+, wavtraname="ergS20120828S0005", \
      w1=5618., w2=INDEF, dw=0.4622, nw=2822, \
      fl_skysub+, sepslits+, fl_fluxcal+, sfunction="Sens_B6-625", \
      extinction="onedstds$ctioextinct.dat", \
      fl_gscrrej-, fl_vardq+, fl_inter-)
   # ...and similarly for other grating settings.


This concludes basic processing and calibration, though some refinements and advance data products are discussed below. 

.. _ifu-rv-correction:

Wavelength Alignment
^^^^^^^^^^^^^^^^^^^^
Since a :term:`dayCal` Arc exposure was used to determine the wavelength scale, the separate science exposures will in general not have identical wavelength zero-points owing to instrument flexure and the time-dependent barycentric correction. 
To determine the wavelength offsets, load the **rv** package and run the **rvidlines** task on the *background* aperture (i.e., the ``[SKY]`` extension). 
Reference night sky emission lines will be adopted from Osterbrock et al. (`1996, PASP, 108, 277 <http://adsabs.harvard.edu/abs/1996PASP..108..277O>`_). 
Download: :download:`skylines.txt <../calib/skylines.txt>` and place it in your work directory. 

.. code-block:: c

   unlearn rvidlines
   rv.observatory="gemini-south"
   rvidlines.coordlist="./skylines.txt"
   rvidlines.ftype="emission"
   rvidlines.logfile="rvLog.txt"

   rvidlines ("stepxrgS20120827S0066.fits[SKY]", threshold=7., \
      nsum=1, maxfeatures=10, fwidth=10., cradius=10., minsep=5.)
   # ...and so on for the other target exposures. 

The output will include a mean shift in velocity and redshift *z*. 
Compute a wavelength difference and add the WCS reference wavelength: 

.. math::

   \Delta\lambda = \lambda_0 + (-z * \bar{\lambda})

where :math:`\lambda_0` is taken from the file header (:math:`\mathtt{CRVAL1} = 5618`, the zero-point specified during the **gfreduce** calibration processing), and :math:`\bar{\lambda}` is the central wavelength of the configuration (6250). 
The output from this same analysis by James Turner produced the shifted wavelengths used below to update the file headers. 

.. code-block:: c

   unlearn hedit
   hedit.update=yes
   hedit.verify=no

   hedit ("cstepxrgS20120827S0066.fits[sci]", "CRVAL1", 5618.164)
   hedit ("cstepxrgS20120827S0067.fits[sci]", "CRVAL1", 5618.268)
   hedit ("cstepxrgS20120827S0068.fits[sci]", "CRVAL1", 5618.387)
   hedit ("cstepxrgS20120827S0070.fits[sci]", "CRVAL1", 5618.560)
   hedit ("cstepxrgS20120827S0071.fits[sci]", "CRVAL1", 5618.667)
   hedit ("cstepxrgS20120827S0072.fits[sci]", "CRVAL1", 5618.787)

The calibrated spectral images are now on the same wavelength scale, but are not yet perfectly aligned spatially. 

Data Cube
^^^^^^^^^
One optional but, for this program, important output product is a datacube of the science exposures which is a 3-dimensional representation of the IFU observations. 
The datacube is an :math:`(x,y,\lambda)` 3-space that can be used to visualize the observations, or to facilitate the extraction of spatially summed spectra or monochromatic, 2-dimensional images. 
The **gfcube** task resamples the image when the atmospheric dispersion correction is enabled, and can optionally rejects pixel flagged in the BPM (but recall that they were interpolated over in prior processing). 
We also elect to convert the brightness units to flux per square arcsec.

.. code-block:: c

   unlearn gfcube
   gfcube.logfile="gfcubeLog.txt"
   
   # Unfortunately, gfcube does not accept file lists.
   sections ("cste@cleanSci_B6-625.txt", >"calibSci_B6-625.txt")
   list = "calibSci_B6-625.txt"
   while (fscan (list, s1) != EOF) {
      gfcube (s1, fl_atmdisp+, fl_flux+, fl_var+, fl_dq+)
   }

Spatial Alignment
:::::::::::::::::
The separate exposures should be combined maximize the S/N for analysis, but they must first be aligned spatially. 
This can be accomplished with the `PyFU <http://drforum.gemini.edu/topic/pyfu-datacube-mosaicking-package/>`_ datacube mosaicing packge, posted by James Turner to the `Gemini Data Reduction User Forum <http://drforum.gemini.edu/topic/gmos-ifu-data-reduction-example-2/>`_. 
Use of this package is beyond the scope of this tutorial, but the results show that spatial shifts affect the final 3 science exposures in the sequence, obtained just after the GCAL flat. 
Edit the WCS keywords in the headers of these calibrated spectra:

.. code-block:: c

   hedit dcstepxrgS20120827S0070.fits[sci] CRVAL1 65.48042 

   hedit dcstepxrgS20120827S0071.fits[sci] CRVAL1 65.47042 
   hedit dcstepxrgS20120827S0071.fits[sci] CRVAL2 0.045000 

   hedit dcstepxrgS20120827S0072.fits[sci] CRVAL1 65.50042 
   hedit dcstepxrgS20120827S0072.fits[sci] CRVAL2 0.035000 

Combine Cubes
:::::::::::::
Now the datacubes may be combined, again with the **PyFU** package. 
Here we use the **imcombine** task, which will suffice for a quick-look; no rejection is enabled, for simplicity. 

.. code-block:: c

   unlearn imcombine
   imcombine ("dcstepxrgS20120827S00*.fits[SCI]", "j2240-097.fits")

The datacube may be visualized in an appropriate tool, such as `SAOImage DS9 <http://ds9.si.edu/site/Home.html>`_. 

.. figure:: /_static/J2240_6645.*
   :width: 70 %

   Combined, false color image of the galaxy J224024.1--092748 obtained with with grating R600/625 in the rest frame of [O_III] :math:`\lambda5007`. This narrow-band (about 28 km/s) image is one spatial frame of a combined data cube, with north up and east to the left. Click image to enlarge. 

Extracting spectra from the datacube is straightforward using **apsum**. 

.. figure:: /_static/j2240_spec.*
   :width: 90 %

   Extracted spectra in the vicinity of [O_III] :math:`\lambda5007` from the nucleus (*left*) and the nearby ionized region (*right*). These spectra correspond to the left- and right-hand side, respectively, of the color image above. Click image to enlarge. 

A great deal of science analysis is possible with these data products, as the [DST]_ paper demonstrated. 
