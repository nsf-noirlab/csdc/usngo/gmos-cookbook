.. _processing-index:

=======================
Processing Science Data
=======================
The recipes described in the linked chapters provide a recommended, **but not unique** path for processing your GMOS science data. 
The various tutorials will generate scientifically viable, but not necessarily optimal data products for analysis. 
As described in the :ref:`gmos-overview`, GMOS has four distinct modalities: 

 * imaging, 
 * long-slit spectroscopy,  
 * multi-object (:term:`Slit-mask`) spectroscopy, and
 * :term:`IFU` spectral imaging

The path through data reduction is somewhat different for each mode, though for spectral modes there is considerable overlap. 

.. _processing-preliminaries:

Preliminaries
-------------
Prerequisites
^^^^^^^^^^^^^
Processing of individual targets can proceed once the following steps have been concluded:

  - Software tools have been installed (see :ref:`getting-started` and :ref:`resources`)
  - Science and calibration data have been acquired from the `Gemini Observatory Archive <https://archive.gemini.edu/searchform>`_ (see :ref:`getting-started`)
  - Data have been reviewed and categorized, and problem exposures have been identified (see :ref:`gen-obslog`)
  - Instrument issues have been reviewed for the epoch of your data (see `GMOS Status <https://www.gemini.edu/news/instrument-announcements/gmos>`_)

Data Retrieval
^^^^^^^^^^^^^^
Download the files of interest from the `Gemini Observatory Archive <https://archive.gemini.edu/searchform>`_ and unpack them in a working directory. 
Or if you want to practice, then use the URLs given in the tutorials linked below to select the files to reduce, and download them to a working directory. 

.. note::

   When an archive search is performed, the search term values are encoded into the URL of the search results. This means you may save or bookmark the URL; if you use it to search again at a later time it will generate the same (or perhaps, an updated) table of results. 

Uncompress Files
::::::::::::::::
The first order of business is to uncompress all of the compressed :term:`FITS` files you retrieved from the archive, because IRAF tasks (and many other applications) cannot read them directly. 
In the directory that contains the raw data to be processed: 

.. code-block:: c

   # At the unix prompt, uncompress the desired files. 
   bunzip2 *.fits.bz2

Generate an Observing Log
^^^^^^^^^^^^^^^^^^^^^^^^^
Data files from a single observing program can number in the hundreds. 
Files generated during processing may increase that total by a factor of a few. 
This makes the task of keeping track of files in your workflow a significant challenge. 
See :ref:`gen-obslog` to help manage the problem. 
The observing log is essential to planning your data reduction strategy. 

Be sure to review the exposures, and identify both the science and supporting calibration data that should be processed. 

Build File Lists
^^^^^^^^^^^^^^^^
The fundamental approach in this *Cookbook* is to apply selection criteria to the exposure metadata (in the :term:`FITS` file headers) to build generic lists of files of various types, which serve as input to the processing tasks. 
For processing with IRAF, carefully choosing names for files containing the list of input exposures allows the tutorial processing scripts to serve as templates for reducing *any* GMOS data, not just specific datasets. 
For processing with PyRAF, the metadata is stored in an SQLite3 database, and **python** lists of exposures are created on-the-fly with SQL queries. 
The process for building these lists is discussed at the beginning of each tutorial, and in the Chapter :ref:`master-ref`.

Reduction Work-flow
-------------------
The chapters linked below provide a conceptual workflow, and a set of step-by-step tutorials for data processing within your data reduction environment. The conceptual overview should provide sufficient context for understanding *what* steps are required and *why*, while the tutorials describe *how* the processing is done. 

.. toctree::
   :maxdepth: 3

   Concept_Overview

The first step (after dealing with the :ref:`processing-preliminaries`) is to prepare the calibration reference (:term:`MasterCal`) files for your data. 
The specific **MasterCal** files you will need depends upon the instrument configuration and your science goals for the reductions. 

.. toctree::
   :maxdepth: 2

   masterRef

There are various supplementary topics that describe tools and techniques, conventions, and calibration reference material for reducing your data. 

.. toctree::
   :maxdepth: 2

   SqlFileSelect
   Supplement

.. _proc-tutorials:

Processing Tutorials
--------------------
The `Astroconda <https://astroconda.readthedocs.io/en/latest/>`_ data processing environment, which includes `PyRAF <https://iraf-community.github.io/pyraf.html>`_, `IRAF <https://iraf-community.github.io/>`_ and the **gemini** package, offers a large number of tools for data reduction and calibration. 
The recipes below provide a detailed path in either IRAF or PyRAF for processing your GMOS science data. 

.. note::

   The code blocks of commands in the tutorials linked below are intended to be executed within the chosen environment (i.e., an interactive PyRAF session, or an IRAF session), unless otherwise noted. The traditional prompt (e.g., "--->" or "cl>") has been omitted to make it easier to cut-and-paste commands into your own interactive session. Non-executable comments are prepended with a pound sign ("#", or hashtag). 

.. _pyraf-proc:

Processing with PyRAF
^^^^^^^^^^^^^^^^^^^^^
Processing data with PyRAF allows scripting and IRAF task execution from the **python** environment. 
See `The PyRAF Tutorial <http://stsdas.stsci.edu/pyraf/doc.old/pyraf_tutorial/>`_ for a primer. 
In an interactive session **pyraf** emulates the IRAF command language very closely. 
In the PyRAF tutorials linked below more of the power of **python** is leveraged to process data, which results in processing scripts that are easier to understand and modify (not to mention more interesting). 
One of the keys to processing data efficiently is to generate **python** lists of files from an observing log database (see: :ref:`gen-obslog`), and either to supply these lists to the processing tasks directly, or to iterate over the lists to process files individually. See :ref:`sql-file-select` for details. 

.. note::

   The PyRAF based tutorials assume a basic understanding of **python**. In addition a familiarity with SQL and the **sqlite** package as called from **python**, while not necessary, would be useful background information. The software module that makes use of **sqlite** is isolated from the tutorial processing scripts, however, so it is fine to treat the SQL selection machinery as a black box. 

Images
^^^^^^
This tutorial reduces and combines images from the program GS-2006B-Q-18 (PI: J. Arias): narrow- and broad-band imaging of M8 (or NGC 6523, the Lagoon Nebula).

.. image:: /_static/M8_large.*
   :width: 300px
   :align: right

.. toctree::
   :maxdepth: 2

   IrafProcImg

.. toctree::
   :maxdepth: 2

   PyrafProcImg

Longslit Spectra
^^^^^^^^^^^^^^^^
This tutorial reduces long-slit spectra from program GS-2007A-Q-76 (PI: C. Winge): longslit spectra of interacting galaxy pairs, specifically AM2306-721. 
See *Interaction effects on galaxy pairs with Gemini/GMOS--I: Electron density* by Krabbe, et al. (2014) [K14]_. 

.. image:: /_static/AM2306.*
   :width: 300px
   :align: right

.. toctree::
   :maxdepth: 2

   IrafProcLS

.. toctree::
   :maxdepth: 2

   PyrafProcLS

Longslit Nod&Shuffle Spectra
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The next tutorial reduces long-slit nod-and-shuffle spectra from program GN-2003B-Q-14 (PI: B. Schmidt): longslit spectra of Supernova candidates from the ESSENCE survey, specifically f017.wdd9_10. 
See *Spectroscopy of High-Redshift Supernovae from the ESSENCE Project: The First Two Years* by Matheson, et al. (2005) [M05]_. 

.. image:: /_static/f017_spec.*
   :width: 200px
   :align: right

.. toctree::
   :maxdepth: 3

   PyrafProcLSNS

Multi-Object Spectra
^^^^^^^^^^^^^^^^^^^^
This tutorial reduces :term:`MOS` spectra from program `GN-2011B-C-3 <http://adsabs.harvard.edu/abs/2011noao.prop...57S>`_ (PI: L. Stanghellini): MOS spectra of H_II regions in M81. 
See *The radial metallicity gradient and the history of elemental enrichment in M81 through emission-line probes* by Stanghellini et al. (2014) [SM14]_. 

.. image:: /_static/M81_MOS.*
   :width: 300px
   :align: right

.. toctree::
   :maxdepth: 2

   IrafProcMOS

.. toctree::
   :maxdepth: 2

   PyrafProcMOS

IFU Spectral Images
^^^^^^^^^^^^^^^^^^^
This tutorial reduces spectral images from programs GS-2012B-Q-26 and GN-2012B-Q-226 (PI: M. Schirmer), :term:`IFU` spectoscopy of a "Green Bean" galaxy. 
See *The "Green Bean" galaxy SDSS J224024.1-092748: unravelling the emission signature of a quasar ionization echo* by Davies, et al. (2015) [DST]_. 

.. image:: /_static/GB_IFU.*
   :width: 300px
   :align: right

.. toctree::
   :maxdepth: 2

   IrafProcIFU

.. toctree::
   :maxdepth: 2

   PyrafProcIFU
