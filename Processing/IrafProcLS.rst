.. _iraf-proc-ls:

========================================
Reduction of Long-Slit Spectra with IRAF
========================================
This tutorial will use observations from program GS-2007A-Q-76 (PI: C. Winge), longslit spectra of interacting galaxy pairs, specifically AM2306-721. 
The spectra were obtained at 3 orientations with a ``1.0arcsec`` slit and the B600 grating centered at 485.0 nm. 
Note that the spectral exposures have rather large CCD binning factors (:math:`2\times4`), in part because this program was executed in the `poor weather queue <http://www.gemini.edu/sciops/schedules/badWeatherSchedQueue2007A.html>`_ where poor seeing is the norm. 
The original technical goal was to measure emission line ratios at several positions along the slit. A flux calibration standard was included in the observing plan, as were comparison arcs at each slit position. 

For other tutorials, see the following links:

* :ref:`iraf-proc-img` 
* :ref:`iraf-proc-mos`
* :ref:`iraf-proc-ifu`

Retrieve & Organize Data
------------------------
The first step is to retrieve the data from the Gemini Observatory Archive (see :ref:`archive-search`). 
You may search the `GOA <https://archive.gemini.edu/searchform>`_ yourself, or instead just cut-and-paste the direct URL in your browser. 

.. code-block:: html

   # longslit data of galaxy pairs:
   https://archive.gemini.edu/searchform/cols=CTOWEQ/GS-2007A-Q-76/notengineering/GMOS-S/LS/20070623/Win#

After retrieving the science data, click the **Load Associated Calibrations** tab on the search results page and download the associated bias and flat-field exposures. 
See :ref:`retrieve-data` for details. 
Unpack all of them in a subdirectory of your working directory named ``/raw``. 
Be sure to uncompress the files. 

The steps below assume that the :term:`MasterCal` reference files have been created for all the spectral settings and :term:`RoI`. 
The required **MasterCals** are: 

* Bias Residual
* Flat-field (GCAL source)
* Wavelength calibration (CuAr comparison arcs)
* Flux calibration (standard star LTT 9239)

See :ref:`master-ref` for details. 

Example Reduction Script
^^^^^^^^^^^^^^^^^^^^^^^^
You can perform all of the processing steps for this tutorial by downloading the Longslit Tutorial CL script. 

* Download: :download:`gmos_ls_proc.cl </irafTools/gmos_ls_proc.cl>` 

Place the script in the work directory. Within an IRAF session load the ``gemini``, ``gemtools``, ``gmos``, and ``onedspec`` packages, then execute it in command-mode:

.. code-block:: c

   cd /path/to/work_directory
   cl < gmos_ls_proc.cl

You may find it useful to download the script to follow this tutorial in detail, and use it as the basis for reducing other longslit observations. 
The processing steps for this tutorial are described below, but only a subset of the commands are given in the interest of brevity. 

.. caution::

   The reduction script includes steps that should be performed interactively for best results, but the interactive options have been disabled in the script in order not to interupt the automated processing flow. 

Create File Lists
^^^^^^^^^^^^^^^^^
The next steps will create lists of calibrations and science files for use in processing. 
The selection is achieved by matching a specific combination of header keyword-value pairs (see :ref:`dr-keywords`). 
Examples of file selection with the `hselect <https://iraf.net/irafhelp.php?val=hselect>`_ task are given below. 
Note the use of ``gemtools.gemextn`` to remove the IRAF FITS kernel syntax from the resulting file names. 

.. code-block:: c

   cd /path/to/work_directory/raw

   ## Bias exposures with a common observation class, RoI, and CCD binning.
   # Science exposures use Full-frame RoI.
   s1 = "obstype?='BIAS' && obsclass?='dayCal' && detrO1ys>1024 && ccdsum?='2 4'"

   # Select bias exposures within ~2 months of the target observations.
   s2 = " && @'date-obs' > '2007-06-05' && @'date-obs' < '2007-07-07'"
   string biasSelect
   biasSelect = s1 // s2

   # Select biases using information from both PHDU and HDU-1
   hselect ("*.fits[1,inherit=yes]", "$I", biasSelect, > "bias_tmp.txt")
   string omit = "index,kernel"
   gemextn ("@bias_tmp.txt", omit=omit, outfile="biasFull.txt")

   # Also prepare a bias for the CenterSpec RoI for the standard star.
   # Build the bias MasterCals: MCbiasFull.fits and MCbiasCenSp.fits (see script)

   ## Flat-fields 
   # Flats should match the observation type, detector RoI and CCD binning,
   # as well as the grating, aperture, and central wavelength:
   s1 = "obstype?='FLAT' && obsclass?='partnerCal' && detro1ys>1024 && ccdsum?='2 4'"
   s2 = "&& grating?='B600+_G5323' && maskname?='1.0arcsec' && grwlen=485.0"
   string flatSelect
   flatSelect = s1 // s2
   hselect ("S*.fits[1,inherit=yes]", "$I", flatSelect, > "flt_tmp.txt")
   gemextn ("@flt_tmp.txt", omit=omit, outfile="flatFull.txt")

Now build the list of arc, standard star, and science exposures. 

.. code-block:: c

   # Select comparison arc exposures via observation type, RoI and CCD binning:
   s1 = "obstype?='ARC' && obsclass?='progCal' && detro1ys>1024 && ccdsum?='2 4'"
   string arcSelect
   arcSelect = s1 // s2
   hselect ("S*.fits[1,inherit=yes]", "$I", arcSelect, > "arc_tmp.txt")
   gemextn ("@arc_tmp.txt", omit="index,kernel", outfile="arcFull.txt")

   # Select science exposures via observation type, RoI and CCD binning.
   # This will also select data from object AM1401: 
   #    delete these filenames if desired.
   s1 = "obstype?='OBJECT' && obsclass?='science' && detro1ys>1024 && ccdsum?='2 4'"
   string sciSelect
   sciSelect = s1 // s2
   hselect ("S*.fits[1,inherit=yes]", "$I", sciSelect, > "sci_tmp.txt")
   gemextn ("@sci_tmp.txt", omit=omit, outfile="sciFiles.txt")

   # Select standard star exposures.
   s1 = "obstype?='OBJECT' && obsclass?='partnerCal' && detro1ys==256 && ccdsum?='2 4'"
   s2 = "&& grating?='B600+_G5323' && maskname?='1.0arcsec' && grwlen=485."
   string stdSelect
   stdSelect = s1 // s2
   hselect ("S*.fits[1,inherit=yes]", "$I", stdSelect, > "std_tmp.txt"
   gemextn ("@std_tmp.txt", omit=omit, outfile="stdFiles.txt")
   # Move the list files to the parent directory. 

Most other processing lists may be built from the above lists and the IRAF `sections <https://iraf.net/irafhelp.php?val=sections>`_ task, as shown below. 

Now move to the work directory and construct the **MasterCal** files as described in :ref:`master-ref`, and as shown in the reduction script for this tutorial.  

Basic Processing
----------------
Basic image reductions begin with the ``gsreduce`` task, using the **Bias** and **Flat-field MasterCal** files.  
These steps with can be performed from the work directory on the lists of files constructed above, using the ``@filelist`` notation. 

.. code-block:: c

   # Use primarily the default task parameters.
   unlearn gsreduce
   gsreduce.logfile="gsreduceLog.txt"
   gsreduce.rawpath="./raw"

   # Perform basic reductions on all exposures for science targets.
   gsreduce ("@sciFiles.txt", bias="MCbiasFull", flatim="MCflatFull", \
      fl_fixpix-, fl_oversize-, fl_vardq+, fl_fulldq+)

   # Perform basic reductions on the Arcs and standard star.
   gsreduce ("@arcFull.txt", bias="MCbiasFull", \
      fl_fixpix-, fl_flat-, fl_oversize-)
   gsreduce ("S20070623S0109", bias="MCbiasCenSp", \
      fl_fixpix-, fl_flat-, fl_oversize-)

   gsreduce ("@stdFiles.txt", bias="MCbiasCenSp", flatim="MCflatCenSp", \
      fl_fixpix+, fl_oversize-, fl_vardq+, fl_fulldq+)

Examine the output files to assess data quality, and adjust the processing or the lists of input files as necessary. 

Multi-frame Cosmic Ray Rejection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The targets in this program were observed with 3 slit orientations, and a few exposures were obtained at each position. 
This provides an opportunity to combine the sequential exposures at each position to remove cosmic rays, rather than rejecting CRs on single frames using the ``gsreduce.fl_gscrrej+`` flag or running the **gemcrspec** task. 

.. code-block:: c

   # Create a list of common slit alignments for the target & standard star.
   sections gs//@stdFiles.txt > gsStdFiles.txt
   s1 = "obstype?='OBJECT' && obsclass?='science' "
   hselect ("gsS*.fits[0]", "$I", \
      (s1 // "&& i_title?='AM2306-721_a'"), > "AM2306_tmp.txt")
   gemextn ("@AM2306_tmp.txt", omit="index", outfile="AM2306a.txt")
   # ...and similarly for 'AM2306-72_b' and 'AM2306-721_c'. 

   # Use primarily the default task parameters to combine.
   unlearn gemcombine
   gemcombine.logfile="gemcombineLog.txt"
   gemcombine.reject="ccdclip"

   # Combine the exposures with outlier rejection for each orientation.
   gemcombine ("@gsStdFiles.txt", "LTT9239", fl_vardq+, fl_dqprop+)
   gemcombine ("@AM2306a.txt", "AM2306a", fl_vardq+, fl_dqprop+)
   # ...and similarly for 'AM2306-72_b' and 'AM2306-721_c'

Note the need above to explicitly propagate the DQ and VAR extensions from the input files. 

Wavelength Calibration
^^^^^^^^^^^^^^^^^^^^^^
Image rectification and wavelength linearization are performed next, using the wavelength calibrated arc lamp exposures taken immediately before each sequence of science and standard star exposures (see :ref:`wave-cal`). In this case, the default medium-resolution line list ``gmos$data/CuAr_GMOS.dat`` will work well.

.. code-block:: c

   # Apply wavelength calibration.
   unlearn gstransform
   gstransform.logfile="gstransformLog.txt"

   gstransform ("LTT9239", wavtraname="gsS20070623S0109", fl_vardq+)
   gstransform ("AM2306a", wavtraname="gsS20070623S0071", fl_vardq+)
   # ...and similarly for 'AM2306-72_b' and 'AM2306-721_c'

Sky Subtraction
^^^^^^^^^^^^^^^
The **gsskysub** task will determine the night sky emission spectrum from a selected spatial region, and subtract it row-by-row from the spectral image. 
Sky subtraction can be performed during the course of spectral extraction, but for this program measuring the extended, weak emission flux of interest calls for a custom extraction procedure (see below). 
While **gsskysub**  can be run interactively, the selection of a sky region from a longslit spatial profile can be determined easily by inspection using the `pcols <https://iraf.net/irafhelp.php?val=pcols>`_ task. 

.. code-block:: c

   pcols ("tAM2306b.fits[SCI]", 1100, 2040, wy1=40, wy2=320)

Expanding the scale shows a good, source-free region, as shown below.
Be sure to plot the sky spectrum in the selected regions to ensure there is no signal from the target. 

.. figure:: /_static/AM2306b_profile.*
   :width: 100 %

   Screen shots of the spatial profile (*left*) of the spectrum for AM2306b, and the resulting sky spectrum (*right*). Click image to enlarge. 

Select your preferred sky regions and perform the sky subtraction, propagating the variance and data quality extensions.

.. code-block:: c

   # Subtract sky spectrum using selected regions.
   unlearn gsskysub
   gsskysub.logfile="gsskysubLog.txt"

   gsskysub ("tLTT9239", long_sample="20:80,180:230", \
      fl_oversize-, fl_vardq+)
   gsskysub ("tAM2306b", long_sample="670:760,920:1020", \
      fl_oversize-, fl_vardq+)
   # ...and similarly for 'AM2306-72_a' and 'AM2306-721_c'

Standard Star Processing
------------------------
Extraction and Calibration
^^^^^^^^^^^^^^^^^^^^^^^^^^
Flux calibration is a necessary final step for this program's science goals. 
The standard star LTT9239 was observed as a part of this program using the ``CenterSpec`` RoI and processed in parallel with the target spectra. 
Extract the 1-D spectrum from the 2-D spectrogram interactively, using a large (3 arcsec) aperture to ensure that all of the signal is captured:

.. code-block:: c

   # Extract the standard star spectrum
   unlearn gsextract
   gsextract.logfile="gsextractLog.txt"
   gsextract ("stLTT9239", apwidth=3., tfunction="spline3", torder=9, \
      fl_inter+)

Now derive the sensitivity calibration, as described in :ref:`flux-cal`. 

Advanced Processing
-------------------
Flux Calibration
^^^^^^^^^^^^^^^^
Calibrate the target spectra by applying the atmospheric extinction correction (download: :download:`mk_extinct.txt <../calib/mk_extinct.txt>`) and the flux calibration.

.. code-block:: c

   # Apply the sensitivity function.
   unlearn gscalibrate
   gscalibrate.logfile="gscalibrateLog.txt"
   gscalibrate.extinction="./mk_extinct.dat"

   gscalibrate ("estLTT9239", sfunc="sens", fl_ext+, fl_scale-)
   gscalibrate ("stAM2306*",  sfunc="sens", fl_ext+, fl_scale-, fl_vardq+)

Note that **gscalibrate** works on both 2-D spectrograms and 1-D extracted spectra. 

Spectrum Extraction
^^^^^^^^^^^^^^^^^^^
The final step is to extract the science spectra in much the same way as in Krabbe et al. (2014). 
That is, spectra are extracted over an interval along the slit, and divided into segments of 4 spatial rows each. 
Recall that the CCD binning in the spatial direction is 4, so the spatial scale is 0.288 arcsec/pixel), yielding an extraction aperture of :math:`1.00\times1.15` arcsec. 
The target SED consists of moderate-level, spatially variable continuum emission, stellar absorption, and sparse emission lines from ionized gas. 
Thus **gsextract** is not well suited for extraction in the way described by Krabbe et al. (2014).

Instead, use the `onedspec.sarith <https://iraf.net/irafhelp.php?val=sarith>`_ task, 
even though it will not automatically propagate the VAR or DQ array (it is possible to work around this limitation). 
Load the **onedspec** package and extract based on inspection of the bright [O_III] 5007 emission (which is redshifted to about 5150), e.g. for AM2306b:

.. code-block:: c

   # Be sure the onedspec package is loaded.
   # Set the number of spatial pixels over which to sum
   nsum=4
   sarith ("cstAM2306b.fits[SCI]", "copy", "", "cstAM2306b.ms", \
      apertures="222-346x4")

An extracted spectrum of one of the brighter apertures is shown below. 

.. figure:: /_static/AM2306b_spec.*
   :width: 80 %

   Screen shot of a portion of a 1-D spectrum for AM2306b, which is ths sum of 4 spatial pixels. Click image to enlarge. 

