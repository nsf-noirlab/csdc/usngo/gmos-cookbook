.. _supplement:

======================
Supplementary Material
======================
Several topics intimately tied to data reduction are gathered here for reference. 
Most topics apply to multiple GMOS observing configurations. 

.. _gen-obslog:

Creating an Observing Log
-------------------------
Raw data files from a single observing program can number in the hundreds. 
Add to that the files generated during the course of data reduction: intermediate files, :term:`MasterCal` reference files, combined exposures, extracted spectra, etc. and the task of keeping track of files in your workflow rapidly becomes a challenge. 
There is no uniquely correct way to manage the large number of files, but there are a couple of common approaches to sorting through the raw input files: 

1. Separate files of various types (e.g., calibrations, science, etc.) into subdirectories based on common attributes. This is a typical approach for preparing to run IRAF file processing tasks. 

2. Create an `sqlite3 <https://www.sqlite.org>`_ database of files and the relevant metadata (see below). Such a database is useful even if you choose IRAF for your image processing, since you can view and select by exposure attributes with the `sqlite browser <http://sqlitebrowser.org>`_. The tool enables you to develop selection criteria, from which file lists with common attributes may be built with the IRAF `hselect <https://iraf.net/irafhelp.php?val=hselect&help=Help+Page>`_ task. 

In either case, it is usually helpful to do the following: 

* Collect the raw calibration exposures (bias, dark, flat) into a subdirectory (called ``/raw`` in the tutorials).
* Collect the raw science exposures (arcs, standard stars, science targets) into the same subdirectory.

The primary reduction tasks all have a ``rawpath`` parameter that allows processing in the main work directory, while accessing input files from a subdirectory (called ``/raw`` in the tutorials). 
Finally, the intermediate files, by default, follow a :ref:`file-nomenclature` based on a prefix to the input file names, to make them easy to identify. 
Once these intermediate files have been checked for quality *and* you are certain you no longer need them, they can be deleted.

Using IRAF hselect
^^^^^^^^^^^^^^^^^^
An observing log may be built with the IRAF task `imutil.hselect <https://iraf.net/irafhelp.php?val=hselect&help=Help+Page>`_.
Within an IRAF session, create two string variables: 

* one with the names of the fields (i.e., the header row), 
* and the the other with the corresponding names of the header keywords (which are not case-sensitive) containing the metadata. 

.. code-block:: c

   # Create a log header with more meaningful field names:
   string fieldNames = "# File ObsDate ObsTime Object ObsType ObsClass CcdBin RoI Filter1 Filter2 Disperser SlitName SlitType Rotator CenWave T_exp Airmass"
   print (fieldNames, > "obsLog.txt")

   string keywords = "$I Date-Obs Time-Obs Object ObsType ObsClass Ccdsum DetRO1ys Filter1 Filter2 Grating MaskName MaskType PA GrWlen ExpTime Airmass"
   
Now build the log with the header row plus one row for each :term:`FITS` file in the directory, excluding acquisition exposures. 
Redirect the task output to a log file:

.. code-block:: c

   hselect ("*.fits[1,inherit=yes]", keywords, "obstype!='acq'", >> "obsLog.txt")

Note the use above of the first HDU header and the "inherit" kernel switch, which retrieves information in the files from both the PHU and HDU-1.

Using Python
^^^^^^^^^^^^
Alternatively, you can build an observing log with a python script (download: :download:`obslog.py <../pyTools/obslog.py>`), in the form of an `sqlite3 <https://www.sqlite.org>`_ database. 
The script will create a database with the fields and metadata described above. 
At the unix prompt, create the observing log and store it a file (``obsLog.sqlite3``) in the ``/raw`` subdirectory. 

.. code-block:: bash

   cd /path/to/work_directory/raw
   python obslog.py obsLog.sqlite3
   
.. _sqlite3-browser:

You may add the ``-v`` option to also write the metadata for each file to STDOUT (which you can re-direct to an ASCII file). The output file is an SQLite3 database, which can be accessed via the python API, or viewed directly with the `SQLite3 browser <http://sqlitebrowser.org>`_. 
An example of the data browser is shown below. 
The browser provides a quick way of assessing the content of an observing program, and for crafting the correct IRAF selection criteria for grouping files with related content. 

.. figure:: /_static/SQLite3.*
   :width: 100 %

   SQLite3 browser showing the observing log for Longslit program GN-2011B-C-3. Of the 279 entries in the database, 10 have been selected by entering text of interest into the dialog boxes above the top row. In this case, file header keywords matching ``ObsType=OBJECT``, ``ObsClass=partnerCal`` and ``RoI=CentSp`` yield the standard star exposures, along with the associated aperture, disperser, and central wavelength. 

The PyRAF tutorials use SQL to select files that match exposure metadata stored in the database.

.. _header-metadata:

Header Metadata
^^^^^^^^^^^^^^^
Values from the keywords listed below are harvested from the data headers. 
Some of the names are obscure, so they are re-mapped to somewhat more intuitive field names if you create an **sqlite3** database. 
Fields may be added (or deleted: *not recommended*) by changing the KW_MAP definition at the top of the python script. 

.. _log-keywords:

.. csv-table:: **Critical Header Metadata**
   :header: "Keyword", "DB Field", "Description"
   :widths: 15, 15, 60

    , ``use_me``, Flag: indicates file usage or exclusion [``1|0``]. Not present in the ASCII version of the log. 
   ``FILE``, ``File``, Name of the data file
   ``DATE-OBS``, ``DateObs``, UT Date of observation start (YYYY-MM-DD)
   ``TIME-OBS``, ``TimeObs``, UT Time of observation start (HH:MM:SS.S)
   ``OBJECT``, ``Object``, Name for target of exposure.
   ``RA``, ``RA``, Right Ascension of target
   ``DEC``, ``Dec``, Declination of target
   ``OBSTYPE``, ``ObsType``, Type of observation: [``arc|bias|cal|dark|flat|fringe|mos|mask|pinhole|ronchi|object``]
   ``OBSCLASS``, ``ObsClass``, Class of observation: [``acq|acqCal|dayCal|partnerCal|progCal|science``]
   ``CCDSUM``, ``CcdBin``, Pixel binning during read-out; combinations of :math:`1\times [1|2|4]` through :math:`4\times [1|2|4]`
   ``DETRO1YS``, ``RoI``, Region of interest for CCD read-out [``Full|CentSpec|CentStamp``]
   ``NODMODE``, ``NodMode``, Mode for :term:`Nod-and-Shuffle` [``STANDARD|<blank>``]
   ``NODPIX``, ``NS_Shift``, Size of shift (pix) for Nod 
   ``NODCOUNT``, ``NS_Cycles``, Number of N&S cycles during exposure
   ``DTAX``, ``DTA_Xoffset``, Detector translation X position (:math:`\mu\mathrm{m}`)
   ``FILTER1``, ``Filter1``, Name of selected filter in wheel 1
   ``FILTER2``, ``Filter2``, Name of selected filter in wheel 2
   ``GRATING``, ``Disperser``, Name of selected disperser (name of blocking filter is appended after the ``+``): [``MIRROR|R150+|R400+|R600+|R831+|B600+|B1200+``]
   ``MASKNAME``, ``AperMask``, Name for selected slit(mask) 
   ``MASKTYPE``, ``MaskType``, Type of selected slit(mask) [``None|IFU|Mask``]
   ``PA``, ``Rotator``, Position angle (:math:`\rho`) of slit on sky (N through E). At :math:`\rho = 0` (default) N is "up" and E is to the "left" in a default **ds9** display 
   ``GRWLEN``, ``CentWave``, Grating approximate central wavelength (nm)
   ``EXPTIME``, ``T_exp``, Exposure duration (s)
   ``AIRMASS``, ``Airmass``, Atmospheric column through which exposure was obtained

Be sure to browse the observing log and/or display the exposures, and remove (or mark as "excluded") any files that should **not** be processed, such as test or acquisition exposures. 

.. _dr-keywords:

GMOS Processing Keywords
------------------------
The keywords listed below are introduced or modified during the course of processing. 

.. csv-table:: **Processing Keywords**
   :header: "Keyword", "Added by", "Description"
   :widths: 15, 15, 70

   ``GEMCOMB``,  ``gemcombine``, UT Timestamp indicating successful processing
   ``GEMCRSPE``, ``gemcrspec``,  UT Timestamp indicating successful processing
   ``GFCUBE``,   ``gfcube``,     UT Timestamp indicating successful processing
   ``GFEXTRAC``, ``gfextract``,  UT Timestamp indicating successful processing
   ``GFSCATSU``, ``gfscatsub``,  UT Timestamp indicating successful processing
   ``GFSKYSUB``, ``gfskysub``,   UT Timestamp indicating successful processing
   ``GFTRANSF``, ``gftransform``, UT Timestamp indicating successful processing
   ``GGAIN``,    ``ggain``,      UT Timestamp indicating update of gain and readnoise
   ``GIREDUCE``, ``gireduce``,   UT Timestamp indicating successful processing
   ``GMOSAIC``,  ``gmosaic``,    UT Timestamp indicating successful processing
   ``GPREPARE``, ``gprepare``,   UT Timestamp indicating header updated
   ``GSAPPWAV``, ``gsappwav``,   UT Timestamp indicating successful processing
   ``GSCALIBR``, ``gscalibrate``, UT Timestamp indicating successful processing
   ``GSCRREJ``,  ``gscrrej``,    UT Timestamp indicating successful processing
   ``GSEXTRAC``, ``gsextract``,  UT Timestamp indicating successful processing
   ``GSREDUCE``, ``gsreduce``,   UT Timestamp indicating successful processing
   ``GSSKYSUB``, ``gsskysub``,   UT Timestamp indicating successful processing
   ``GSTRANSF``, ``gstransform``, UT Timestamp indicating successful processing
   , ,
   ``BIASIM``,   ``gbias``,      Name of **Bias Residual MasterCal**
   ``CA-FLAG``,  ``gscalibrate``, Flag for flux calibration
   ``CD1_1``,    ``gsappwav``,   :term:`WCS` deriviative matrix
   ``CD1_2``,    ``gsappwav``,   WCS deriviative matrix
   ``CD2_1``,    ``gsappwav``,   WCS deriviative matrix
   ``CD2_2``,    ``gsappwav``,   WCS deriviative matrix
   ``CRPIX1``,   ``gsappwav``,   WCS reference pixel for axis1
   ``CRPIX2``,   ``gsappwav``,   WCS reference pixel for axis2
   ``CRVAL1``,   ``gsappwav``,   Value of WCS at reference pixel for axis1
   ``CRVAL2``,   ``gsappwav``,   Value of WCS at reference pixel for axis2
   ``CTYPE1``,   ``gsappwav``,   Type of WCS coordinate for axis1
   ``CTYPE2``,   ``gsappwav``,   Type of WCS coordinate for axis2
   ``DC-FLAG``,  ``gtransform``, Flag for dispersion correction
   ``DCLOG1``,   ``gtransform``, Name of reference arc
   ``DISPAXIS``, ``gprepare``,   Axis number of dispersion direction (1 for spectroscopy)
   ``EX-FLAG``,  ``gscalibrate``, Flag for extinction correction
   ``EXTNAME``,  ``gprepare``,   Name of extension (SCI)
   ``EXTVER``,   ``gprepare``,   Version of extension (1 to no. amps in use)
   ``FLATIM``,   ``gireduce``,   Name of **Flat-field MasterCal**
   ``GAIN``,     ``gprepare``,   Updated value of gain from DB
   ``GAINORIG``, ``ggain``,      Value of gain after gain correction
   ``GEM-TLM``,  <Any task>,     UT Timestamp for last modification by a gemini task
   ``GMOSCLVA``, ``gmosaic``,    gmosaic clean value
   ``GMSINTER``, ``gmosaic``,    Interpolant used to resample image
   ``GSCRRES``,  ``gscrrej``,    GSCRREJ equivalent instrumental FWHM
   ``GSCRTHRE``, ``gscrrej``,    GSCRREJ sigma rejection threshold
   ``NEXTEND``,  ``gprepare``,   Total number of :term:`MEF` extensions
   ``NSCIEXT``,  ``gprepare``,   Number of SCI extensions
   ``OBSMODE``,  ``ggain``,      Observing mode; derived from ``MASKTYP`` (IMAGE|MOS|IFU)
   ``RDNOISE``,  ``gprepare``,   Updated value of read noise from DB
   ``RONORIG``,  ``ggain``,      Value of read noise after gain correction
   ``TRIMMED``,  ``gireduce``,   Indicates removal of overscan (yes|no)
   ``WMEF``,     ``wmef``,       UT Timestamp indicating successful processing
   ``WAT0_001``, ``gsappwav``,   WCS description
   ``WAT1_001``, ``gsappwav``,   Continuation of WCS description
   ``WAT2_001``, ``gsappwav``,   Continuation of WCS description
   ``WAVTRAN``,  ``gtransform``, Name of reference wavelength solution

.. caution::
   The **gmos** tasks *require* the above keywords from prior reduction stages to be present and populated correctly for processing to proceed to the next stage. If you choose to perform some of the processing with your own or other IRAF tools, you will need to ensure that the expected processing keywords are inserted into the output headers if you want to continue processing with **gmos** tasks. 

.. _bpm-encoding:

GMOS BPM Flag Encoding
----------------------
Bad pixel masks are (optionally) used in GMOS processing to track the various conditions that apply to individual pixels during the course of data reduction. 
The :term:`BPM` pixel values correspond to a logical ``or`` of one or more bit values: :math:`2^{N-1}`, where N is the bit corresponding the a particular, recognized pathology. 
The **Static BPM MasterCal** files describe the permanent features of the GMOS detectors; other flags are introduced during data reduction. 
The following table describes the meaning assigned to values that are used (and reserved) by GMOS software. 

.. csv-table:: **BPM Flag Encoding**
   :header: "Bit", "Pixel Value", "Meaning"
   :widths: 10, 10, 50

   0, ``0``, No conditions apply
   1, ``1``, Generic bad pixel: region not illuminated (or occulted); hot pixel; bad column.
   2, ``2``, Highly non-linear response
   3, ``4``, Saturated pixel
   4, ``8``, Affected by cosmic-ray hit
   5, ``16``, Pixel contains invalid data (e.g. all data rejected during stacking)
   6, ``32``, [Not used]
   7, ``64``, [Not used]
   8, ``128``, [Not used]
   9, ``256``, SCI pixel value has been replaced by interpolation
   10, ``512``, SCI pixel value has been replaced but not interpolated

The IRAF help for the **gemfix** task describes how they may be used. 
Be aware that the BPM for processed data (intermediate or final data products) may be different than that for raw data. 
For example, when images are stacked the flagged data will be excluded during the combine operation. 

.. _calib-suppl:

Calibration Material
--------------------
Many calibration reference files are distributed with IRAF and the **gemini** package. 
Others are available from other sources. 
This sections provides pointers to many of the files you will need for GMOS. 

.. _bpm-files:

Bad Pixel Masks
^^^^^^^^^^^^^^^
A set of Static Bad Pixel Masks (:term:`BPM`) are available in the ``gmos$data/`` directory. 
The BPMs differ for each type of CCD, and the choice of binning used during read-out; there are often separate versions for individual CCDs and mosaiced images. 
The BPMs depend in principle upon the number of amplifiers used for read-out, but in practice science programs use only one choice of amps for a given generation of CCDs. 
Beware that many of the BPMs are appropriate for mosaic'd exposures, rather than CCDs in separate image extensions. 
Additional BPMs have been constructed for this *Cookbook*, and may be downloaded via a link in the last column. 
See the table below for the set of available files.

.. csv-table:: **Static Bad Pixel Masks**
   :header: "Instrument", "CCD Type", "Binning", "Mode", "Location"
   :widths: 12, 12, 10, 12, 40

   GMOS-N, EEV,       :math:`1\times1`, Imaging,      ``gmos$data/``
   ,       ,          :math:`2\times2`, ,             ``gmos$data/``
   ,       e2vDD,     :math:`1\times1`, ,             ``gmos$data/``
   ,       ,          :math:`2\times2`, ,             ``gmos$data/``
   GMOS-S, EEV (old), :math:`1\times1`, Imaging,      ``gmos$data/``
   ,       ,          :math:`2\times2`, ,       :download:`bpm_gmos-s_EEV_v1_2x2_img_MEF.fits.bz2 <../calib/bpm_gmos-s_EEV_v1_2x2_img_MEF.fits.bz2>`
   GMOS-S, EEV (new), :math:`1\times1`, Imaging,      ``gmos$data/``
   ,       ,          :math:`2\times2`, ,             ``gmos$data/``
   ,       ,          :math:`1\times1`, Spect., :download:`bpm_gmos-s_EEV_v2_1x1_spec_MEF.fits.bz2 <../calib/bpm_gmos-s_EEV_v2_1x1_spec_MEF.fits.bz2>`
   GMOS-S, Hamamatsu, :math:`1\times1`, Imaging,      ``gmos$data/``
   ,       ,          :math:`2\times2`, ,             ``gmos$data/``
   ,       ,          :math:`4\times4`, ,             ``gmos$data/``


.. _arc-atlas:

Arc Lamp Atlas
^^^^^^^^^^^^^^
Line identifications may be found on the Gemini/GMOS `CuAr Spectra <https://www.gemini.edu/instrumentation/gmos/calibrations#Examples>`_ page.  
A one-page atlas of the comparison arc is shown below. 

.. figure:: ../_static/CuAr_HiRes.png

   Cu-Ar spectrum at full scale (*blue*) with portions magnified (*purple*) and offset vertically for clarity. More than 400 identifiable lines are marked (*red ticks*) along the wavelength axis. Some of the brighter or more isolated lines are labelled, which should suffice to bootstrap a wavelength solution. Second-order lines in the near IR (*lower right, labelled in green*) may be useful for constraining the dispersion solution in this sparse part of the arc lamp spectrum. 
   Click image to enlarge. 

The Cu-Ar lamp is the primary wavelength calibration source, and line lists are provided in the **gmos** IRAF package. 
However for the highest dispersion modes, the standard line lists are incomplete *if the arc is well exposed*. 
The following line lists are available:

.. csv-table:: **CuAr Line Lists**
   :header: "Location", "Notes"
   :widths: 20, 40

   ``gmos$calib/CuAr_GMOS.dat``, Default list for **gmos** package; subset for non-interactive fits.
   ``gmos$calib/GCALcuar.dat``, Preliminary list; more lines but less precise wavelengths.
   Download: :download:`cuarHiRes.txt <../_static/cuarHiRes.txt>`, Complete list downloadable with this *Cookbook*; suitable for deeply exposed Arcs at high resolution.

.. Note:: 

   You may wish to use the high-dispersion line list to determine the dispersion solution for the highest resolution configurations (B1200 and R831 with a slitwidth <1.0arcsec). If the Arc exposure is sufficiently deep it should be possible to include more than 100 lines in the dispersion solution (with an RMS less than a few tenths of a pixel), except in the near IR. The fit will require a polynomial of order 6 or 7 to yield an acceptable solution. 

Spectrophotometry
^^^^^^^^^^^^^^^^^

.. _atm-extinction:

Atmospheric Extinction
::::::::::::::::::::::
The correction for atmospheric extinction normally involves the application of averaged extinction coefficients for a given site, in units of magnitudes per unit airmass over a grid of wavelengths. 
The correction typically only includes Rayleigh scattering, aerosols, and ozone, but not telluric absorption. 
Gemini data reduction procedures provide for standard IRAF extinction curves, which are available for KPNO and CTIO (and an undocumented curve ostensibly for Mauna Kea), but not for the higher altitude of the Gemini sites on Mauna Kea and Cerro Pachon where the extinction is (not surprisingly) lower. 
These extinction curves are plotted below, along with a model developed by Buton et al. (2013; [BC]_), derived from extensive observations on Mauna Kea. 

.. figure:: /_static/Extinction.*
   :width: 90 %

   Variation of typical atmospheric extinction at unit airmass vs. wavelength for CTIO (*purple*), KPNO (*blue*), and Mauna Kea (*teal*). 

Although the Buton et al. model also does not include the (time dependent) telluric absorption, it is clearly more appropriate for GMOS-N data. 
The extinction files may be found in the locations given below (locations beginning with ``onedstds$`` refer to an IRAF system directory). 

.. csv-table:: **Atmospheric Extinction Curves**
   :header: "Observatory", "File Location"
   :widths: 15, 30

   CTIO, ``onedstds$ctioextinct.dat``
   KPNO, ``onedstds$kpnoextinct.dat``
   GMOS-N, Download: :download:`mk_extinct.txt <../calib/mk_extinct.txt>`

.. note::

   The extinction curve named ``gmos$calib/mkoextinct.dat`` is very coarsely sampled, has no provenance, and is very different from the curve from Buton et al.; it *should not* be used. 

.. _stdstar-list:

Standards List
::::::::::::::
A set of photometric standards are routinely observed with GMOS to monitor instrument performance and to enable flux calibration of spectra. Individual observing programs may in addition have observed these or other flux standards. 

There are often multiple published monochromatic magnitudes of spectrophotometric standards. 
The following table is the tertiary standard subset of the spectrophotometric standards included in the IRAF **noao** package.  
The monochromatic magnitudes have been published in one of the references given below the table. 
These files may be found under the IRAF directory ``noao$lib/`` (except for EG 131 which is in ``gmos$calib/``) in the subdirectory indicated. 
The table heading ``caldir`` is the IRAF name of the calibration directory in which the monochromatic magnitudes for the standard are located. 

Some alternative standards are available, such as those provided by `ESO <https://www.eso.org/sci/observing/tools/standards/spectra.html>`_ or `STScI <https://www.stsci.edu/hst/instrumentation/reference-data-for-calibration-and-tools/astronomical-catalogs/calspec>`_, which may have better sampling, or have been extended to the UV or near IR. 
In these cases a link from the star name has been provided to the relevant repository. 
For other standards the monochromatic magnitudes may only be available from an external source. 

.. csv-table:: **Spectrophotometric Standards**
   :header: "Star", "Caldir", "IRAF Name", "Literature Codes"
   :widths: 15, 25, 15, 20

   `AGK+81 266 <http://www.eso.org/sci/observing/tools/standards/spectra/agk81d266.html>`_,   ,  ``agk81266``, HST
   `BD+28 4211 <http://www.eso.org/sci/observing/tools/standards/spectra/bd28d4211.html>`_, ``onedstds$spec50cal/``,  ``bd284211``, MS88; MG90
   `BD+25 4655 <https://www.eso.org/sci/observing/tools/standards/spectra/bd25d4655.html>`_,  ,  ``bd254655``, Oke90
   `BD+33 2642 <https://www.eso.org/sci/observing/tools/standards/spectra/bd33d2642.html>`_,  ,  ``bd33d2642``, Oke90; HST
   `BD+75 325 <https://www.eso.org/sci/observing/tools/standards/spectra/bd75d325.html>`_, ``onedstds$oke90/``,  ``bd75325``, Oke90; HST
   CD-32 9927,   ``onedstds$ctionewcal/``, ``cd32``, HSHWG  
   EG 21,        ``onedstds$ctionewcal/``, ``eg21``, HSHWG  
   EG 81,        ``onedstds$spec50cal/``,  ``eg81``, HSHWG  
   EG 131,       ``gmos$calib/``,          ``eg131``, 
   `EG 274 <http://www.eso.org/sci/observing/tools/standards/spectra/eg274.html>`_,         ``onedstds$ctionewcal/``, ``eg274``, HSHWG  
   `Feige 34 <http://www.eso.org/sci/observing/tools/standards/spectra/feige34.html>`_,     ``onedstds$spec50cal/``,  ``feige34``, HST; MS88; MG90
   Feige 56,     ``onedstds$ctionewcal/``, ``f56``, HSHWG  
   `Feige 66 <http://www.eso.org/sci/observing/tools/standards/spectra/feige66.html>`_,     ``onedstds$spec50cal/``,  ``feige66``, HSHWG  
   `Feige 67 <http://www.eso.org/sci/observing/tools/standards/spectra/feige67.html>`_,     ``onedstds$spec50cal/``,  ``feige67``, HSHWG  
   `Feige 110 <http://www.eso.org/sci/observing/tools/standards/spectra/feige110.html>`_,   ``onedstds$spec50cal/``,  ``feige110``, HSHWG; MG90
   `G191-B2B <http://www.eso.org/sci/observing/tools/standards/spectra/g191b2b.html>`_,     ``onedstds$spec50cal/``,  ``g191b2b``, MS88; MG90; HST 
   `GD 50 <http://www.eso.org/sci/observing/tools/standards/spectra/gd50.html>`_,           ,                         ``gd50``,    Oke90; HST
   `GD 71 <http://www.eso.org/sci/observing/tools/standards/spectra/gd71.html>`_,           ,                         ``gd71``,    HST
   `GD 108 <http://www.eso.org/sci/observing/tools/standards/spectra/gd108.html>`_,         ,                         ``gd108``,   HST
   `GD 153 <http://www.eso.org/sci/observing/tools/standards/spectra/gd153.html>`_,         ,                         ``gd153``,   HST
   `GRW+70 5824 <https://www.eso.org/sci/observing/tools/standards/spectra/grw70d5824.html>`_,                     ,  ``grw70d5824``, Oke90; HST
   `Hiltner 600 <http://www.eso.org/sci/observing/tools/standards/spectra/hilt600.html>`_,  ``onedstds$spec50cal/``,  ``hilt600``, HSHWG; MG90
   HD 192281,    ``onedstds$spec50cal/``, ``hd192281``, HSHWG  
   HD 217086,    ``onedstds$spec50cal/``, ``hd217086``, HSHWG  
   `HZ 2 <http://www.eso.org/sci/observing/tools/standards/spectra/hz2.html>`_,                                     ,  ``hz2``, HST 
   `HZ 4 <http://www.eso.org/sci/observing/tools/standards/spectra/hz4.html>`_,                                     ,  ``hz4``, Oke90; HST 
   `HZ 21 <http://www.eso.org/sci/observing/tools/standards/spectra/hz21.html>`_,           ``onedstds$oke90/``,  ``hz21``, Oke90; HST
   `HZ 43 <http://www.eso.org/sci/observing/tools/standards/spectra/hz43.html>`_,           ``onedstds$iidscal/``,  ``hz43``, HST
   `HZ 44 <http://www.eso.org/sci/observing/tools/standards/spectra/hz44.html>`_,           ``onedstds$spec50cal/``,  ``hz44``, MS88; MG90; HST
   `L745-46A <http://www.eso.org/sci/observing/tools/standards/spectra/cd_34241.html>`_ ,   ``onedstds$ctionewcal/``, ``l745``, HSHWG
   `LB 227 <http://www.eso.org/sci/observing/tools/standards/spectra/lb227.html>`_,         ,                         ``lb227``,   HST
   `LDS 749B <http://www.eso.org/sci/observing/tools/standards/spectra/lds749b.html>`_,     ,                         ``lds749b``, Oke90; HST
   LTT 377*,     ``onedstds$ctionewcal/``, ``l377``, HSHWG  
   LTT 1020,     ``onedstds$ctionewcal/``, ``l1020``, HSHWG
   LTT 1788,     ``onedstds$ctionewcal/``, ``l1788``, HSHWG 
   LTT 2415,     ``onedstds$ctionewcal/``, ``l2415``, HSHWG
   `LTT 3218 <http://www.eso.org/sci/observing/tools/standards/spectra/ltt3218.html>`_,     ``onedstds$ctionewcal/``, ``l3218``, HSHWG 
   LTT 3864,     ``onedstds$ctionewcal/``, ``l3864``, HSHWG 
   LTT 4364,     ``onedstds$ctionewcal/``, ``l4364``, HSHWG 
   LTT 4816,     ``onedstds$ctionewcal/``, ``l4816``, HSHWG 
   LTT 6248,     ``onedstds$ctionewcal/``, ``l6248``, HSHWG 
   LTT 7379,     ``onedstds$ctionewcal/``, ``l7379``, HSHWG 
   `LTT 7987 <http://www.eso.org/sci/observing/tools/standards/spectra/ltt7987.html>`_,     ``onedstds$ctionewcal/``, ``l7987``, HSHWG 
   LTT 9239,     ``onedstds$ctionewcal/``, ``l9239``, HSHWG 
   `LTT 9491 <http://www.eso.org/sci/observing/tools/standards/spectra/ltt9491.html>`_,     ``onedstds$ctionewcal/``, ``l9491``, HSHWG 

Literature references:
 
   **B99**: Bessell (`1999, PASP, 111, 1426 <https://ui.adsabs.harvard.edu/abs/1999PASP..111.1426B/>`_), *A revision of the HSHWG spectrophotometry redward of 670 nm to remove telluric features* 

   **HSHWG**: Hamuy et al. `(1994, PASP, 106, 566 <http://ui.adsabs.harvard.edu/abs/1994PASP..106..566H>`_), *Southern hemisphere secondary and tertiary spectrophotometric standards over the range 330--1050 nm*

   **HST**: Bohlin, Colina, & Finlay `(1995, AJ, 110, 1316 <http://ui.adsabs.harvard.edu/abs/1995AJ....110.1316B>`_), *White dwarf Standard Stars: G191-B2B, GD 71, GD 153, HZ 43*

   **MS88**: Massey & Strobel (`1988, ApJ, 328, 315 <http://ui.adsabs.harvard.edu/abs/1988ApJ...328..315M>`_) *Northern hemisphere spectrophotometric standards over the range 320--8100 nm*

   **MG90**: Massey & Gronwall (`1990, ApJ, 358, 344 <http://ui.adsabs.harvard.edu/abs/1990ApJ...358..344M>`_) *Extension of MS88 spectrophotometric data to 1000 nm*

   **Oke90**: Oke (`1990, AJ, 99, 1621 <http://ui.adsabs.harvard.edu/abs/1990AJ.....99.1621O>`_) *Faint spectrophotometric standard stars, intended to support HST, covering the range 320 to 920 nm* 

\*Note that L745-46A is labelled CD-34 241 in the `ESO list <http://www.eso.org/sci/observing/tools/standards/spectra/stanlis.html>`_. CD-34 241 was mistakenly named LTT 377 in Hamuy et al. (1992 & 1994).


IRAF Reduction Tools
--------------------
A few of the IRAF data reduction tasks have interactive options, where the user provides input via the IRAF graphics utility. 
These tools involve cursor interactions and keystrokes, which can be viewed by entering ``?`` when in cursor mode. 
The most commonly used options for two of the most complex tasks are given below, for reference. 

.. _wav-identify:

Wavelength Calibration
^^^^^^^^^^^^^^^^^^^^^^
Wavelength calibration is performed with the **gmos.gswavelength** task, which is really a wrapper for the IRAF **identify** family of tasks. 

Cursor Keys
:::::::::::
.. csv-table:: **Identify Cursor Keys**
   :header: "Key", "Description"
   :widths: 5, 60

   ``?``, Clear the screen and print a menu of options.
   ``a``, Apply next *center* or *delete* operation to *all* features
   ``b``, Identify features and find a dispersion function automatically using the coordinate line list and approximate values for the dispersion.
   ``c``, Center the feature nearest the cursor. Used when changing the position finding parameters or when features are defined from a previous feature list.
   ``d``, Delete the feature nearest the cursor. Delete all features when preceded by the ``a`` ll key. This does not affect the dispersion function.
   ``e``, Find features from a coordinate list without doing any fitting. This is like the ``l`` key without any fitting.
   ``f``, Fit a function of the pixel coordinates to the user coordinates. **This enters the interactive function fitting package.**
   ``g``, Fit a zero point shift to the user coordinates by minimizing the difference between the user and fitted coordinates. The coordinate function is not changed.
   ``i``, Initialize (delete features and coordinate fit).
   ``l``, Locate features in the coordinate list. A coordinate function must be defined or at least two features must have user coordinates from which a coordinate function can be determined. If there are features an initial fit is done; then features are added from the coordinate list; and then a final fit is done.
   ``m``, Mark a new feature using the cursor position as the initial position estimate.
   ``n``, Move the cursor or zoom window to the next feature (same as ``+``).
   ``p``, Pan to the original window after zooming on a feature.
   ``q``, Quit and continue with next image.
   ``r``, Redraw the graph.
   ``s``, Shift the fit coordinates relative to the pixel coordinates. The user specifies the desired fit coordinate at the position of the cursor and a zero point shift to the fit coordinates is applied. If features are defined then they are recentered and the shift is the average shift. The shift is printed in pixels & user coordinates & z (fractional shift).
   ``u``, Enter a new user coordinate for the current feature. When marking a new feature the user coordinate is also requested.
   ``w``, Window the graph. A window prompt is given and a number of windowing options may be given. For more help type ``?`` to the window prompt or see help under gtools.
   ``x``, Find a zero point shift for the current dispersion function. This is used by starting with the dispersion solution and features from a different spectrum. The mean shift is printed in user coordinates & mean shift in pixels & the fractional shift in user coordinates.
   ``z``, Zoom on the feature nearest the cursor. The width of the zoom window is determined by the parameter zwidth.
   ``.``, Move the cursor or zoom window to the feature nearest the cursor.
   ``+``, Move the cursor or zoom window to the next feature.
   ``-``, Move the cursor or zoom window to the previous feature.

Colon-command Summary
:::::::::::::::::::::
The following is an abridged list of *colon commands* (i.e., command names preceded by the ``:`` key) to view (with no argument) or set (including trailing argument) a **gswavelength** task parameter. 
The commands may be abbreviated. 
For a full list see `identify <https://iraf.net/irafhelp.php?val=identify>`_ or invoke the ``?`` cursor command within an interactive session.

.. csv-table:: **Identify Cursor Keys**
   :header: Key, Value, Description
   :widths: 5, 10, 60

   ``:show``, file, Show the values of all the parameters. If a file name is given then the output is appended to that file. If no file is given then the terminal is cleared and the output is sent to the terminal.
   ``:features``, file, Print the feature list and the fit rms. If a file name is given then the output is appended to that file. If no file is given then the terminal is cleared and the output is sent to the terminal.
   ``:coordlist``, file, Set or show the coordinate list file.
   ``:cradius``, value, Set or show the centering radius in pixels.
   ``:threshold``, value, Set or show the detection threshold for centering.
   ``:database``, name, Set or show the database for recording feature records.
   ``:ftype``, value, Set or show the feature type (emission or absorption).
   ``:fwidth``, value, Set or show the feature width in pixels.
   ``:labels``, value, Set or show the feature label type (``none|index|pixel|coord|user|both``). None produces no labeling; index labels the features sequentially in order of pixel position; pixel labels the features by their pixel coordinates; coord labels the features by their user coordinates (such as wavelength); user labels the features by the user or line list supplied string; and both labels the features by both the user coordinates and user strings.
   ``:match``, value, Set or show the coordinate list matching distance.
   ``:maxfeatures``, value, Set or show the maximum number of features automatically found.
   ``:minsep``, value, Set or show the minimum separation allowed between features.
   ``:zwidth``, value, Set or show the zoom width in user units. 

.. _apextract-summary:

APEXTRACT Summary
^^^^^^^^^^^^^^^^^
The aperture extraction utility (`apextract <https://iraf.net/irafhelp.php?val=apextract&help=Help+Page&pkg=1>`_) in IRAF is invoked from the **gmos.gsextract** task. 
When run interactively, this utility provides a variety of cursor keys to control the extraction of target spectra. 
If you use IRAF for your data reduction, you will need to get comfortable with this task. 
See the `apextract.apall documentation <https://iraf.net/irafhelp.php?val=apall>`_ for details. 

The following are the available cursor commands for aperture definition and spectrum extraction. 

.. csv-table:: **Aperture Editor Cursor Keys**
   :header: Key, Ap, Description
   :widths: 5, 5, 60

   ``?``,   , Print help
   ``a``,   , Toggle the ALL flag
   ``b``, an, Set background fitting parameters
   ``c``, an, Center aperture(s)
   ``d``, an, Delete aperture(s)
   ``f``,   , Find apertures up to the requested number 
   ``g``, an, Recenter aperture(s) 
   ``l``, ac, Set *lower* limit of current aperture at cursor position (see ``u``)
   ``m``,   , Define and center a new aperture on the profile near the cursor
   ``n``,   , Define a new aperture centered at the cursor
   ``q``,   , Quit
   ``r``,   , Redraw the graph
   ``s``, an, Shift the center(s) of the current aperture to the cursor position
   ``t``, ac, Trace aperture positions
   ``u``, ac, Set *upper* limit of current aperture at cursor position  (see ``l``)
   ``w``,   , Window the graph using the window cursor keys
   ``y``, an, Set aperture limits to intercept the data at the cursor y position
   ``z``, an, Resize aperture(s) 
   ``.``,  n, Select the aperture nearest the cursor to be the current aperture
   ``+``,  c, Select the next aperture (in ID) to be the current aperture
   ``-``,  c, Select the previous aperture (in ID) to be the current aperture
   ``I``,   , Interrupt task immediately. Database information is not saved.

The letter a following the key indicates if all apertures are affected when the ALL flag is set. The letter ``c`` indicates that the key affects the *current* aperture while the letter ``n`` indicates that the key affects the aperture whose center is *nearest* the cursor. 

Colon-command Summary
:::::::::::::::::::::
The following is an abridged list of colon commands (i.e., command names preceded by the ``:`` key) to view (with no argument) or set (including trailing argument) a **gsextract** task parameter. 
For a full list see `apall <https://iraf.net/irafhelp.php?val=apall>`_ or invoke the ``?`` cursor command within an interactive session.

.. csv-table:: **Aperture Editor General Colon-commands**
   :header: "Command", "Description"
   :widths: 18, 50

   ``:b_function``, Background fitting function
   ``:b_function``, Background fitting function
   ``:b_high_reject``/ ``:b_low_reject``, Background high/low rejection limits
   ``:b_naverage``, Determine background from average or median
   ``:b_order``, Function order for background fit
   ``:b_sample``, Comma-separated list of background sample region(s) [``nnn:nnn``]
   ``:background``, Background to subtract (e.g. ``none``)
   ``:bkg``, Subtract background in automatic width? [``yes`` | ``no``]
   ``:clean``, Detect and replace bad pixels? [``yes`` | ``no``]
   ``:extras``, Extract sky & sigma etc. in addition to spectrum?
   ``:line``, Dispersion line over which to display profile
   ``:nsum``, Extent over which to determine profile (positive for *sum* or negative for *median*)
   ``:lower/:upper``, Lower/upper aperture limits relative to center
   ``:lsigma/:usigma``, Lower/upper rejection threshold
   ``:parameters``, Print the current value of all parameters
   ``:radius``, Profile centering radius
   ``:t_function``, Type of fitting function for trace
   ``:t_high_reject``/ ``:t_low_reject``, Upper/lower rejection limits for trace [sigma]
   ``:t_nsum``, Number of dispersion pixels to sum for trace
   ``:t_order``, Order of trace fitting function
   ``:t_step``, Step size for fitting function 
   ``:weights``, Extraction weights [``none`` | ``variance``]
   ``:width``, Profile centering width

Note that all parameters having to do with positions or distances are in units of binned pixels. 

Background Information
----------------------

.. _dispersion-solution:

Description of the Dispersion Solution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The gratings used in GMOS introduce significant nonlinearity to the dispersion relation, which can generally be well characterized with a Legendre or Chebyshev polynomial of order 6 or 7. 
If a non-linear dispersion solution is written directly into the FITS header (as it is by the IRAF task ``dispcor`` when linearization is turned off), it will consist of a number of terms including the coefficients of the fitted polynomial. 
The coefficients are described in the paper `The IRAF/NOAO Spectral World Coordinate Systems <http://www.researchgate.net/publication/2308651_The_IRAFNOAO_Spectral_World_Coordinate_Systems>`_ (1991, F. Valdes). 
The following excerpt describes how to compute wavelengths from the nonlinear function of choice. 

There are three coordinates of relevance: the pixel coordinate *p* of the spectrum array; the normalized coordinates *n* over the domain of the fitting function, in the interval [-1, 1]; and the world coordinates *w* at each pixel. The transformation from pixel to normalized coordinates *n* is: 

.. math::
   n = \frac{2p - (p_{max} +p_{min})}{(p_{max}-p_{min})} 

Note that in practice the range of pixels will extend somewhat beyond the domain over which the fitting function was defined. 
For a single function type (the usual case unless comparison arcs taken immediately before and after a science exposure are used to refine the wavelength zero-point), the transformation from pixel coordinates *p* and world coordinates *w* is:

.. math::
   w = \frac{\Delta \lambda+\Lambda(p)}{(1+z)}

where *z* is the Doppler factor. The dispersion function :math:`\Lambda(p)` at pixel *p* can be evaluated over the function coefficients :math:`c_i`: 

.. math::
   \Lambda(p) = \sum_{i=1}^{order} c_i x_i

where :math:`x_1 = 1; x_2 = n`. The non-linear terms for order :math:`i>2` may be computed recursively; for Chebyshev polynomials we have:

.. math::
   x_i = 2nx_{i-1} - x_{i-2}

or for Legendre polynomials: 

.. math::
   x_i = \frac{(2i-3)nx_{i-1} - (i-2)x_{i-2}}{(i-1)}

IRAF spectroscopic tasks have a built-in capability to read dispersion solutions with the above form. 
For python users the following snippet of code may be used to construct a wavelength array from Legendre or Chebyshev function parameters and coefficients, using the functions in :download:`poly.py  <../pyTools/poly.py>`:

.. code-block:: python

   import numpy as np
   import poly as pl

   # Extract function, parameters, and coefficients from WAT2_00x keywords. 
   # An example from the center of an arc comparison exposure using grism b2k 
   # and facility longslit 3pxC:
   pMin, pMax = [90.65229797363281, 4054.766357421875]
   c = np.array([5160.180854771875, 1399.010545377342, 64.60055185877235, -24.74632014374652, 0.1313465583541718, -0.1962541400576848, -0.06403879553807495])
   nPix = 4096

   # Generate an array of world coordinates (in Angstroms, the declared WCS unit).
   n = pl.getNormCoords(pMin, pMax, nPix)
   w = pl.evDispersion(pl.evLegendre, c, n)

.. _wcs:

World Coordinate Systems
^^^^^^^^^^^^^^^^^^^^^^^^
It is useful to have at least an approximate World Coordinate Solution (:term:`WCS`) specified in the header of your science images; this solution can be refined later in target processing. 

.. _imaging-wcs:

Imaging WCS
:::::::::::
For imaging mode exposures, the WCS may be of scientific interest even when imaging was not the focus of the original observing program. 
For observing programs that used custom MOS slits, the WCS in acquisition images is helpful for associating slit locations with specific targets (or regions within extended astronomical objects).  

Setting the WCS Description
:::::::::::::::::::::::::::
An approximate solution was inserted in the observing environment from the telescope alignment during each exposure, and the instrument rotator angle. 
The following table lists the WCS keywords that are necessary to specify a complete FITS WCS in the image extension header. 

.. csv-table:: **GMOS Image WCS Keywords**
   :header: "Keyword", "Update", "Value", "Meaning"
   :widths: 10, 10, 15, 50

   ``RADECSYS``, Deleted, ``-``, Deprecated keyword
   ``WCSASTRM``, Deleted, ``-``, Not used for WCS
   ``RADESYS``,  Added,   ``FK5``, Celestial coordinate reference frame
   ``WCSAXES``,  Added,   ``2``,   Number of axes in WCS description
   ``CTYPE1``,   Updated, ``RA---TAN``, Coordinate type for axis1
   ``CTYPE2``,   Updated, ``DEC--TAN``, Coordinate type for axis2
   ``CUNIT1``,   Updated, ``deg``, Coordinate units for axis1
   ``CUNIT2``,   Updated, ``deg``, Coordinate units for axis2
   ``LTV1``,     Updated, ``0``,   CCD to image offset: axis1
   ``LTV2``,     Updated, ``0``,   CCD to image offset: axis2
   ``CDi_j``,    Updated, (see below), Derivative of World Coordinate values ``i`` w.r.t pixel array ``j`` at the reference location

The CD matrix is given by the following: 

.. math::
   \begin{pmatrix}
   \mathtt{CD1\_1}  & \mathtt{CD1\_2} \\ 
   \mathtt{CD2\_1}  & \mathtt{CD2\_2} 
   \end{pmatrix} = \sigma
   \begin{pmatrix}
   -\cos\theta & \sin\theta \\ 
   -\sin\theta & -\cos\theta 
   \end{pmatrix} 

where:

.. math::
   & \theta =  \mathtt{PA} \\
   & \sigma =  \mathit{(plate scale)}/\mathrm{3600} \\

In the above, *platescale* is in arcsec/pixel, ``PA`` is the position angle in degrees given by this keyword value, and :math:`\theta` is measured from North through East. 
For both GMOS instruments, :math:`\theta=0` yields an orientation where N is down and E is to the left.

.. _refine-imaging-wcs:

Refining the Reference Coordinates
::::::::::::::::::::::::::::::::::
The world coordinates at the reference pixel are taken from the commanded telescope pointing, which may be off by up to a few arcmin. 
The WCS zero-point can be adjusted by correcting the CRVALi keywords with offsets determined from stars in the field. 
Often this correction can be determined using the `SAOImage DS9 <https://sites.google.com/cfa.harvard.edu/saoimageds9/home>`_ image display tool. 
The process is: 

   * Process the image through bias- and flat-fielded correction
   * Display the image in DS9
   * From the pull-down menu, select "**WCS** :math:`\rightarrow` degrees" for the coordinate display
   * Select **Analysis** :math:`\rightarrow` Catalogs :math:`\rightarrow` Optical :math:`\rightarrow` USNO UCAC3
   * Compare the pattern of star locations with those of the catalog, as shown below

.. figure:: /_static/catalog_wcs.*
   :scale: 75 %

   Image of NGC 6302, with positions of catalog stars plotted (*green circles*). Note the "c" shaped pattern of catalog stars (bottom, left) appears to match that of stars in the image (bottom, center).

   * Select any star from the catalog (sorting by RA or Dec may help) and:

     * record the coordinates from both the catalog star and the image display cursor at the position of that star in the image
     * compute the difference (i.e., the offset values in degrees) in each coordinate

   * Update the ``CRVAL1`` and ``CRVAL2`` keyword values with these offsets

Highly Accurate WCS
:::::::::::::::::::
If your science objectives require a highly accurate WCS you must determine a full WCS solution with community software, such as the IRAF ``mscred.mscfinder.msctpeak`` task (see the `tutorial <https://iraf.net/irafdocs/astrometry/astrom.html#msctpeak>`_). 
An astrometric catalog will be needed for this calibration; magnitudes in the same bandpass will be needed for the photometric calibration. 
Although the process to fit a full WCS solution is involved, it is possible to characterize optical distortions into the WCS (using the ``TNX`` projection); RMS uncertainties of 200 mas should be achievable. 

Refined Imaging WCS
...................
Solving for the WCS in a image requires using community software. There are a few possibilities, some of which also require local access to an astrometric catalog. The options include: 

 * The IRAF `MSCRED package <https://iraf.net/irafdocs/astrometry/astrom.html>`_ (requires catalog)
 * The `astrometry.net <http://astrometry.net>`_ software (downloading & installing software may be necessary)
 * Using `Aladin <http://aladin.u-strasbg.fr>`_ to determine WCS, and transferring the keywords to the header manually
 * Using the `USNO image and catalog service <http://www.usno.navy.mil/USNO/astrometry/optical-IR-prod/icas/fchpix>`_

Refined Spectroscopic WCS
.........................
It is possible to refine the zero-point of the wavelength calibration if night-sky emission lines are present in your spectrogram, using the **rv** package. 
See IFU :ref:`ifu-rv-correction` for details. 

Advanced Longslit WCS
.....................
It is possible to create a linear WCS that will describe the RA and Dec along the slit, as well as wavelengths in the dispersion direction, by introducing a degenerate third image axis, as described by Calabretta & Greisen (`2002, A&A, 395, 1077 <http://ui.adsabs.harvard.edu/abs/2002A%26A...395.1077C>`_; Sect. 7.4.3). 

