.. _about:

================
About This Guide
================

This *Cookbook* was created to help users reduce and calibrate spectra obtained with the GMOS spectrographs. 
Note the version number in the top banner of your web browser. 
Users may find the description of GMOS data products in the :ref:`getting-started` chapter very helpful. 

Authors
-------
This *Cookbook* was originally authored by Dick Shaw, and draws extensively upon original papers and presentations about data reduction from Rodrigo Carrasco, Kathleen Labrie, Richard McDermid, Kathy Roth, and James Turner. 
In the interest of clarity, the incorporated material is not quoted; rather, citations to prior work appear throughout this *Cookbook*, a reference list is given in :ref:`literature-ref`, and prior data reduction tutorials are listed in :ref:`online-resources`. 

The *Cookbook* has been extensively revised/updated and it is curated by the `US NGO staff <https://noirlab.edu/science/programs/csdc/usngo/about>`_ (Vinicius Placco, Brian Merino, and Letizia Stanghellini).

Citations to this *Cookbook* should read: 

   US National Gemini Office 2022, *GMOS Data Reduction Cookbook* (Version 2.0; Tucson: NSF's National Optical-Infrared Astronomy Research Laboratory), available online at:

   `<https://noirlab.edu/science/programs/csdc/usngo/gmos-cookbook/>`_

Users should also cite the article (indexed by ADS) published on `NOIRLab's The Mirror <https://ui.adsabs.harvard.edu/abs/2022Mirro...3...13M/>`_, which contains a concise description of the changes made in the new version of the cookbook.

Previous citation (deprecated):

   Shaw, Richard A. 2016, *GMOS Data Reduction Cookbook* (Version 1.2; Tucson: National Optical Astronomy Observatory), available online at: 

   `<https://noirlab.edu/science/programs/csdc/usngo/gmos-cookbook/>`_

Typographical Conventions
-------------------------
Technical documentation is often a struggle to follow. 
This document features some typographical conventions to help the reader understand the content, and to help distinguish explanatory text from dialog with the computer. 
Limitations of `reStructured text <http://www.sphinx-doc.org/en/stable/rest.html>`_ prohibit elaborate textual markup, however. 

Notes, Cautions, and Warnings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::

   **Notes** describe technical or scientific points or caveats that likely have a bearing on the scientific viability of the outcome, or call attention to features of the reduction software that may not be obvious, but are important to understand. They appear in a text block like this, usually with a distinct background color (depending upon the settings used for the document build software). 

.. warning::

   **Cautions** and **warnings** describe technical or scientific matters that are likely to seriously compromise the science products being created, or software issues that may result in silent corruption of data. They appear in a colored text block like this, usually with a pink background. 

Technical Terminology
^^^^^^^^^^^^^^^^^^^^^
The descriptions in this *Cookbook* include many technical terms. The first time a technical term, such as :term:`WCS`, is used in a Chapter it is linked with a definition in the :ref:`glossary`.

Software Packages
^^^^^^^^^^^^^^^^^
Names of software packages appear in **bold** font. 
Scripts that were built for this *Cookbook* appear in ``fixed-space`` font and are downloadable via a link. 
Names of third-party packages are often linked to the web site from which they can be downloaded (in which case they cannot appear in **bold font**). 
See the chapter on :ref:`resources` for information about the software packages that are required to make use of the processing scripts. 

.. note::
   Scripts developed for this *Cookbook* are meant to be illustrative and useful. However they do not include error checking, and are not likely to be robust against unexpected input. They are intended serve as a guide for users to develop their own personal processing scripts. 

Code Blocks and Literals
^^^^^^^^^^^^^^^^^^^^^^^^
When describing a process for using software, the text that should be typed by the user appears in a code block, unless it is prepended by a hash:

.. code-block:: bash

   # Type the line below: 
   echo 'Hello, world!'

This text appears in ``monospaced`` font and usually with syntax highlighting (using `Pygments <http://pygments.org>`_) that is appropriate for the context. 
Text describing a literal command-line, names of arguments, directory and file names, etc. are also set in ``monospaced`` font.

Colophon
--------
This Cookbook was written using `Sphinx <http://sphinx-doc.org>`_, which uses `reStructuredText <http://docutils.sourceforge.net/rst.html>`_ as the markup language (see `Sphinx Documentation <http://sphinx-doc.org/contents.html>`_). 
Most structured content (headings, lists, tables, warnings, source code blocks, etc.) was implemented with native **Sphinx** markup. 
**Sphinx** has significant limitations, however, so some content required custom tools. 
The following describes the technologies used to implements some features:  

* Some tables were created with **Microsoft Excel** and rendered as a figure (see, e.g. :ref:`image-workflow`).
* Figures were created in a variety of ways: 

  * screen shots with the OSX **grab** utility (e.g., :ref:`flux-cal`)
  * **python** scripts (e.g., :ref:`gmos-cuar-lamp`)
  * graphic illustrations with `Adobe Illustrator <http://www.adobe.com/products/illustrator.html>`_ (e.g., :ref:`gmos-focalplane`)

The source files used to create these illustrations are available separately. 

Updating This Document
^^^^^^^^^^^^^^^^^^^^^^

**Version 2.0**

Starting with V2.0, the source and configuration files for the *Cookbook* have been transferred to the `NOIRLab/CSDC GitLab <https://gitlab.com/nsf-noirlab>`_. A CI/CD pipeline was developed to handle all the building and deployment tasks. Any changes committed to the main branch of the *Cookbook* repository will be automatically incorporated into the public webpages. Please report any issues to usngo@noirlab.edu.

**Previous updating instructions**

If the source or configuration files (found under the ``/source_drc`` or ``/common`` subdirectories) are altered, the document HTML files should be rebuilt with: 

.. code-block:: bash

   sphinx-build -b html source_drc GMOS_drc

which will update the ``.html`` files in the ``/GMOS_drc`` subdirectory. Then make a tar of the ``/GMOS_drc`` directory contents, copy it to the deployment directory, and unpack. 

A PDF document may be generated (e.g., in the subdirectory ``tex``) by specifying a ``latex`` target: 

.. code-block:: bash

   sphinx-build -b latex source_drc tex_drc
   cd tex_drc
   pdflatex GMOSCookbook

The quality of the rendering is not very good, however, and the **LaTeX** processing does not complete without errors. Improving it would at least require fixing the relevant **LaTeX** style files. 

